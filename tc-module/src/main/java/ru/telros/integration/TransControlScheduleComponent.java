package ru.telros.integration;

import oracle.jdbc.driver.OracleDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.telros.integration.domain.CarInfo;
import ru.telros.integration.service.CarInfoMapper;
import ru.telros.integration.service.TransControlService;

import java.sql.*;

@Component
public class TransControlScheduleComponent {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransControlScheduleComponent.class);

	@Autowired
	private TransControlService transControlService;

	private void checkEntities(String query) {
		try {
			DriverManager.registerDriver(new OracleDriver());
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.58.24:1521:tran","BRIGADE", "brig!1$3Q");
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			CarInfoMapper carInfoMapper = new CarInfoMapper();
			while (rs.next()) {
				CarInfo carInfo = carInfoMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(transControlService.saveInfo(carInfo).debugInfo());
			}
			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkAllEntities() {
		LOGGER.info("Requesting All Car Info");
		//ZonedDateTime start = ZonedDateTime.now().toLocalDate().atStartOfDay(ZoneId.systemDefault()).withZoneSameLocal(ZoneOffset.UTC);
		//String s = start.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
		//SELECT * FROM V_CARINFO1 vc WHERE VC.GMT BETWEEN CAST(SYS_EXTRACT_UTC(systimestamp) AS DATE) - INTERVAL '15' MINUTE AND CAST(SYS_EXTRACT_UTC(systimestamp) AS DATE) ORDER BY VC.GMT DESC;
		String query = "SELECT * FROM V_CARINFO1 ORDER BY GMT";
		//String query = "SELECT * FROM V_CARINFO1 WHERE GMT > TO_DATE('" + s + "', 'DD.MM.YYYY HH24:MI:SS') ORDER BY GMT";
		checkEntities(query);
		//transControlService.deleteDuplicates();
		LOGGER.info("Finished all processes");
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkRegionEntities() {
		LOGGER.info("Requesting Region Car Info");
		String s = "'в015ае178', 'в213ас178', 'в235ас178', 'в468ем178', 'в006ае178', 'а823ра198', 'в596ау178', " +
				"'в014ае178', 'в931ао178', 'в233ас178', 'в426ав178', 'в469ем178', 'в656он178', 'в965хт98'";
		String query = "SELECT * FROM V_CARINFO1 WHERE FNUMBERAUTO IN (" + s + ") ORDER BY GMT";
		checkEntities(query);
		LOGGER.info("Finished region processes");
	}
}
