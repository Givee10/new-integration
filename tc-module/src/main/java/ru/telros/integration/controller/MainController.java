package ru.telros.integration.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.telros.integration.domain.CarInfo;
import ru.telros.integration.service.TransControlService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class MainController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

	@Autowired
	private TransControlService service;

	@RequestMapping(value = "positions", method = RequestMethod.GET)
	public @ResponseBody String getAllCarInfo() {
		final StringBuffer sb = new StringBuffer("");
		List<CarInfo> allPositions = service.findAllPositions();
		allPositions.forEach(carInfo -> {
			sb.append(carInfo.debugInfo());
			LOGGER.debug(carInfo.debugInfo());
		});
		return sb.toString();
	}

	@RequestMapping(value = "positions/{number}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getCarInfo(@PathVariable("number") String number) {
		CarInfo carInfo = service.findPositionByNumber(number);
		if (carInfo != null) {
			LOGGER.debug(carInfo.debugInfo());
			return ResponseEntity.ok(carInfo.debugInfo());
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "track/{number}", method = RequestMethod.GET)
	public @ResponseBody String getCarTrack(@PathVariable("number") String number) {
		final StringBuffer sb = new StringBuffer("");
		List<CarInfo> carInfoList = service.findTrackByNumber(number);
		carInfoList.forEach(carInfo -> {
			sb.append(carInfo.debugInfo());
			LOGGER.debug(carInfo.debugInfo());
		});
		return sb.toString();
	}
}
