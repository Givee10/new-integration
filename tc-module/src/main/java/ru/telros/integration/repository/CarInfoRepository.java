package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.CarInfo;

import java.util.Date;
import java.util.List;

@Repository
public interface CarInfoRepository extends JpaRepository<CarInfo, Long> {
	@Query(value = "SELECT o FROM CarInfo o WHERE (o.NUMBERAUTO = ?1 AND o.FNUMBERAUTO = ?2 AND o.GMT = ?3)")
	CarInfo findByParameters(String NUMBERAUTO, String FNUMBERAUTO, Date GMT);

	@Query(value = "SELECT o FROM CarInfo o WHERE (o.FNUMBERAUTO = ?1 AND o.GMT IN (SELECT MAX(o.GMT) FROM CarInfo o WHERE o.FNUMBERAUTO = ?1))")
	CarInfo findCarPosition(String number);

	@Query(value = "SELECT o FROM CarInfo o WHERE (o.FNUMBERAUTO = ?1)")
	List<CarInfo> findCarTrack(String number);

	@Query(value = "DELETE FROM V_CARINFO1 WHERE V_CARINFO1.ID > (SELECT MIN(V.ID) FROM V_CARINFO1 v " +
			"WHERE V.GMT = V_CARINFO1.GMT AND V.XCOODINATAUTO = V_CARINFO1.XCOODINATAUTO AND V.YCOORDINATAUTO = V_CARINFO1.YCOORDINATAUTO)", nativeQuery = true)
	void deleteDuplicates();
}
