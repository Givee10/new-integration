package ru.telros.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.authorizeRequests()
				.anyRequest().fullyAuthenticated()
				.and()
				.formLogin();
	}

	@Bean
	public ActiveDirectoryLdapAuthenticationProvider activeDirectoryLdapAuthenticationProvider() {
	    String domain = "vodokanal.spb.ru";
	    String uri = "ldap://192.168.99.208/ " +
                "ldap://192.168.16.11/ " +
                "ldap://192.168.58.224/ " +
                "ldap://192.168.61.196/ " +
                "ldap://192.168.99.244/ " +
                "ldap://192.168.132.3/ " +
                "ldap://192.168.31.5/ " +
                "ldap://192.168.13.125/ " +
                "ldap://192.168.73.7/ " +
                "ldap://192.168.34.192/ " +
                "ldap://192.168.39.11/ " +
                "ldap://192.168.0.210/ " +
                "ldap://192.168.28.220/ " +
                "ldap://192.168.11.4/ " +
                "ldap://192.168.11.8/ " +
                "ldap://192.168.46.199/ " +
                "ldap://192.168.30.14/ " +
                "ldap://192.168.83.3/";
        return new ActiveDirectoryLdapAuthenticationProvider(domain, uri);
	}

	@Bean
	public TaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.setPoolSize(5);
		return threadPoolTaskScheduler;
	}
}
