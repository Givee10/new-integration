package ru.telros.integration.service;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.domain.CarInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarInfoMapper implements RowMapper<CarInfo> {
	@Override
	public CarInfo mapRow(ResultSet resultSet, int i) throws SQLException {
		CarInfo carInfo = new CarInfo();
		carInfo.NUMBERAUTO = resultSet.getString("FNUMBERAUTO");
		carInfo.FNUMBERAUTO = resultSet.getString("FNUMBERAUTO");
		carInfo.MODELAUTO = resultSet.getString("MODELAUTO");
		carInfo.CONDITIONAUTO = resultSet.getString("CONDITIONAUTO");
		carInfo.XCOODINATAUTO = resultSet.getFloat("XCOODINATAUTO");
		carInfo.YCOORDINATAUTO = resultSet.getFloat("YCOORDINATAUTO");
		carInfo.GMT = resultSet.getTimestamp("GMT");
		return carInfo;
	}
}
