package ru.telros.integration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.telros.integration.domain.CarInfo;
import ru.telros.integration.repository.CarInfoRepository;

import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TransControlService {
	private final CarInfoRepository carInfoRepository;

	@Autowired
	public TransControlService(CarInfoRepository carInfoRepository) {
		this.carInfoRepository = carInfoRepository;
	}

	public void deleteDuplicates() {
		carInfoRepository.deleteDuplicates();
	}

	public CarInfo saveInfo(CarInfo entity) {
		CarInfo old = carInfoRepository.findByParameters(entity.NUMBERAUTO, entity.FNUMBERAUTO, entity.GMT);
		if (old != null) entity.ID = old.ID;
		return carInfoRepository.save(entity);
	}

	public List<CarInfo> findAllPositions() {
		return carInfoRepository.findAll();
	}

	public CarInfo findPositionByNumber(String number) {
		return carInfoRepository.findCarPosition(number);
	}

	public List<CarInfo> findTrackByNumber(String number) {
		return carInfoRepository.findCarTrack(number);
	}
}
