package ru.telros.integration.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "V_CARINFO1")
@SequenceGenerator(name = "idGenerator", sequenceName = "V_CARINFO1_SEQ", allocationSize = 1)
public class CarInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenerator")
	@Column(name = "ID")
	public Long ID;

	@Column(name = "NUMBERAUTO")
	public String NUMBERAUTO;

	@Column(name = "GMT")
	public Date GMT;

	@Column(name = "FNUMBERAUTO")
	public String FNUMBERAUTO;

	@Column(name = "MODELAUTO")
	public String MODELAUTO;

	@Column(name = "XCOODINATAUTO")
	public Float XCOODINATAUTO;

	@Column(name = "YCOORDINATAUTO")
	public Float YCOORDINATAUTO;

	@Column(name = "CONDITIONAUTO")
	public String CONDITIONAUTO;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("CarInfo{");
		sb.append("ID=").append(ID);
		sb.append(", NUMBERAUTO='").append(NUMBERAUTO).append('\'');
		sb.append(", GMT=").append(GMT);
		sb.append(", FNUMBERAUTO='").append(FNUMBERAUTO).append('\'');
		sb.append(", MODELAUTO='").append(MODELAUTO).append('\'');
		sb.append(", XCOODINATAUTO=").append(XCOODINATAUTO);
		sb.append(", YCOORDINATAUTO=").append(YCOORDINATAUTO);
		sb.append(", CONDITIONAUTO='").append(CONDITIONAUTO).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
