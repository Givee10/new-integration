package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TYPE_WORK_ELEMENT")
@SequenceGenerator(name = "idGenerator", sequenceName = "TYPE_WORK_ELEMENT_SEQ", allocationSize = 1)
public class TypeWorkElement extends AbstractEntity {
	@Column(name = "OPER_CODE")
	public String OPER_CODE;

	@Column(name = "EO_TYPE")
	public String EO_TYPE;

	@Column(name = "UPDATE_ALLOWED_PARAM1")
	public String UPDATE_ALLOWED_PARAM1;

	@Column(name = "ADDITIONAL_EO")
	public String ADDITIONAL_EO;

	@Column(name = "METRICS_CODE")
	public String METRICS_CODE;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TypeWorkElement that = (TypeWorkElement) o;

		if (OPER_CODE != null ? !OPER_CODE.equals(that.OPER_CODE) : that.OPER_CODE != null) return false;
		if (EO_TYPE != null ? !EO_TYPE.equals(that.EO_TYPE) : that.EO_TYPE != null) return false;
		return METRICS_CODE != null ? METRICS_CODE.equals(that.METRICS_CODE) : that.METRICS_CODE == null;
	}

	@Override
	public int hashCode() {
		int result = OPER_CODE != null ? OPER_CODE.hashCode() : 0;
		result = 31 * result + (EO_TYPE != null ? EO_TYPE.hashCode() : 0);
		result = 31 * result + (METRICS_CODE != null ? METRICS_CODE.hashCode() : 0);
		return result;
	}
}
