package ru.telros.integration.domain;

import javax.persistence.*;

@Entity
@Table(name = "MESSAGE_IMPORT_PLAN")
@SequenceGenerator(name = "idGenerator", sequenceName = "MESSAGE_IMPORT_PLAN_SEQ", allocationSize = 1)
public class MessageImportPlan {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenerator")
	@Column(name = "ID")
	public Long ID;

	@Column(name = "ORDER_SET_ID")
	public Long ORDER_SET_ID;

	@Column(name = "DEPARTMENT_ID")
	public Long DEPARTMENT_ID;

	@Column(name = "BRIGADE_PLAN_ID")
	public Long BRIGADE_PLAN_ID;

	@Column(name = "BRIGADE_ID")
	public Long BRIGADE_ID;
}
