package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "STANDART")
@SequenceGenerator(name = "idGenerator", sequenceName = "STANDART_SEQ", allocationSize = 1)
public class Standart extends AbstractEntity {
	@Column(name = "INVENTORY_ITEM_ID")
	public String INVENTORY_ITEM_ID;

	@Column(name = "OPER")
	public String OPER;

	@Column(name = "DESCRIPTION")
	public String DESCRIPTION;

	@Column(name = "OPERATION_SEQUENCE_ID")
	public String OPERATION_SEQUENCE_ID;

	@Column(name = "OPER_CODE")
	public String OPER_CODE;

	@Column(name = "OPERATION_DESCRIPTION")
	public String OPERATION_DESCRIPTION;

	@Column(name = "METRICS_CODE")
	public String METRICS_CODE;

	@Column(name = "DEFAULT_METRIC_VALUE")
	public String DEFAULT_METRIC_VALUE;

	@Column(name = "EO_MAT_TYPE")
	public String EO_MAT_TYPE;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Standart standart = (Standart) o;

		return OPERATION_SEQUENCE_ID != null ? OPERATION_SEQUENCE_ID.equals(standart.OPERATION_SEQUENCE_ID) : standart.OPERATION_SEQUENCE_ID == null;
	}

	@Override
	public int hashCode() {
		return OPERATION_SEQUENCE_ID != null ? OPERATION_SEQUENCE_ID.hashCode() : 0;
	}
}
