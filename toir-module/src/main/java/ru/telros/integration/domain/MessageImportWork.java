package ru.telros.integration.domain;

import javax.persistence.*;

@Entity
@Table(name = "MESSAGE_IMPORT_WORK")
@SequenceGenerator(name = "idGenerator", sequenceName = "MESSAGE_IMPORT_WORK_SEQ", allocationSize = 1)
public class MessageImportWork {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenerator")
	@Column(name = "ID")
	public Long ID;

	@Column(name = "WIP_ENTITY_ID")
	public Long WIP_ENTITY_ID;

	@Column(name = "DEPARTMENT_ID")
	public Long DEPARTMENT_ID;

	@Column(name = "WORK_ORDER_ID")
	public Long WORK_ORDER_ID;

	@Column(name = "BRIGADE_ID")
	public Long BRIGADE_ID;
}
