package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DEVIATION_REASON")
@SequenceGenerator(name = "idGenerator", sequenceName = "DEVIATION_REASON_SEQ", allocationSize = 1)
public class DeviationReason extends AbstractEntity {
	@Column(name = "FLEX_VALUE_ID")
	public String FLEX_VALUE_ID;

	@Column(name = "FLEX_VALUE")
	public String FLEX_VALUE;

	@Column(name = "FLEX_VALUE_MEANING")
	public String FLEX_VALUE_MEANING;

	@Column(name = "DESCRIPTION")
	public String DESCRIPTION;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DeviationReason that = (DeviationReason) o;

		return FLEX_VALUE_ID != null ? FLEX_VALUE_ID.equals(that.FLEX_VALUE_ID) : that.FLEX_VALUE_ID == null;
	}

	@Override
	public int hashCode() {
		return FLEX_VALUE_ID != null ? FLEX_VALUE_ID.hashCode() : 0;
	}
}
