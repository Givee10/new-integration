package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ADDRESS")
@SequenceGenerator(name = "idGenerator", sequenceName = "ADDRESS_SEQ", allocationSize = 1)
public class Address extends AbstractEntity {
	@Column(name = "ADDR_ID")
	public String ADDR_ID;

	@Column(name = "PRF_ID")
	public String PRF_ID;

	@Column(name = "REG_NAME")
	public String REG_NAME;

	@Column(name = "ADDR_CODE")
	public String ADDR_CODE;

	@Column(name = "ADDR_CADASTR_NMB")
	public String ADDR_CADASTR_NMB;

	@Column(name = "ADDR_WHOLE_NMB")
	public String ADDR_WHOLE_NMB;

	@Column(name = "ADDR_BUILD")
	public String ADDR_BUILD;

	@Column(name = "ADDR_LETTER")
	public String ADDR_LETTER;

	@Column(name = "ADDR_PART_NMB")
	public String ADDR_PART_NMB;

	@Column(name = "ADDR_PRINT")
	public String ADDR_PRINT;

	@Column(name = "ADDR_NAME")
	public String ADDR_NAME;

	@Column(name = "ADDR_FLOOR")
	public String ADDR_FLOOR;

	@Column(name = "ADDR_TYPE_NAME")
	public String ADDR_TYPE_NAME;

	@Column(name = "ADDR_KVARTAL_CODE")
	public String ADDR_KVARTAL_CODE;

	@Column(name = "ADD_NMB")
	public String ADD_NMB;

	@Column(name = "ADDR_KVARTAL")
	public String ADDR_KVARTAL;

	@Column(name = "ADDR_FS_IND")
	public String ADDR_FS_IND;

	@Column(name = "ADDR_KZR_ID")
	public String ADDR_KZR_ID;

	@Column(name = "START_DATE_ACTIVE")
	public String START_DATE_ACTIVE;

	@Column(name = "END_DATE_ACTIVE")
	public String END_DATE_ACTIVE;

	@Column(name = "ENABLED_FLAG")
	public String ENABLED_FLAG;
}
