package ru.telros.integration.domain;

public class OrderTask {
	public Long XXBR_SET_ID;
	public Long XXBR_WIP_ENTITY_ID;
	public Long WIP_ENTITY_RAW_ID;
	public String START_TIME;
	public String END_TIME;
	public String FACT_TIME;
	public String COMMENTS;
	public String LAST_UPDATE_DATE;
	public String LAST_UPDATED_BY;
	public String CREATION_DATE;
	public String CREATED_BY;
	public String LAST_UPDATE_LOGIN;
	public String M_FACT_START;
	public String M_FACT_END;
	public String M_FACT_TIME;
	public String VALIDATION_TEXT;
	public String ACTIVITY_NAME;
	public String DOC_SOURCE;
	public String REQUEST_NUMBER;
	public Long PRF_ID;
	public Long ADDR_ID;
	public String ADDR_COMMENT;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("OrderTask{");
		sb.append("XXBR_SET_ID=").append(XXBR_SET_ID);
		sb.append(", XXBR_WIP_ENTITY_ID=").append(XXBR_WIP_ENTITY_ID);
		sb.append(", WIP_ENTITY_RAW_ID=").append(WIP_ENTITY_RAW_ID);
		sb.append(", START_TIME='").append(START_TIME).append('\'');
		sb.append(", END_TIME='").append(END_TIME).append('\'');
		sb.append(", FACT_TIME='").append(FACT_TIME).append('\'');
		sb.append(", COMMENTS='").append(COMMENTS).append('\'');
		sb.append(", LAST_UPDATE_DATE='").append(LAST_UPDATE_DATE).append('\'');
		sb.append(", LAST_UPDATED_BY='").append(LAST_UPDATED_BY).append('\'');
		sb.append(", CREATION_DATE='").append(CREATION_DATE).append('\'');
		sb.append(", CREATED_BY='").append(CREATED_BY).append('\'');
		sb.append(", LAST_UPDATE_LOGIN='").append(LAST_UPDATE_LOGIN).append('\'');
		sb.append(", M_FACT_START='").append(M_FACT_START).append('\'');
		sb.append(", M_FACT_END='").append(M_FACT_END).append('\'');
		sb.append(", M_FACT_TIME='").append(M_FACT_TIME).append('\'');
		sb.append(", VALIDATION_TEXT='").append(VALIDATION_TEXT).append('\'');
		sb.append(", ACTIVITY_NAME='").append(ACTIVITY_NAME).append('\'');
		sb.append(", DOC_SOURCE='").append(DOC_SOURCE).append('\'');
		sb.append(", REQUEST_NUMBER='").append(REQUEST_NUMBER).append('\'');
		sb.append(", PRF_ID=").append(PRF_ID);
		sb.append(", ADDR_ID=").append(ADDR_ID);
		sb.append(", ADDR_COMMENT='").append(ADDR_COMMENT).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
