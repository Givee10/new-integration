package ru.telros.integration.domain;

public class WONormDet {
	public Long XXBR_WIP_ENTITY_ID;
	public Long OPERATION_ITEM_ID;
	public Long ACTION_SEQ_ID;
	public Long RESOURCE_ID;
	public Long EO_ID;
	public String EO_TYPE;
	public String CALC_NORM;
	public String METRICS_CODE;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WONormDet{");
		sb.append("XXBR_WIP_ENTITY_ID=").append(XXBR_WIP_ENTITY_ID);
		sb.append(", OPERATION_ITEM_ID=").append(OPERATION_ITEM_ID);
		sb.append(", ACTION_SEQ_ID=").append(ACTION_SEQ_ID);
		sb.append(", RESOURCE_ID=").append(RESOURCE_ID);
		sb.append(", EO_ID=").append(EO_ID);
		sb.append(", EO_TYPE='").append(EO_TYPE).append('\'');
		sb.append(", CALC_NORM='").append(CALC_NORM).append('\'');
		sb.append(", METRICS_CODE='").append(METRICS_CODE).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
