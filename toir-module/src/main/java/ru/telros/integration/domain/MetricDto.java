package ru.telros.integration.domain;

public class MetricDto {
	private Long id;
	private String wipEntityId;
	private String metricsCode;
	private String quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getMetricsCode() {
		return metricsCode;
	}

	public void setMetricsCode(String metricsCode) {
		this.metricsCode = metricsCode;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
