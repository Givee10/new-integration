package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SPEC_TECHNIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "SPEC_TECHNIC_SEQ", allocationSize = 1)
public class SpecTechnic extends AbstractEntity {
	@Column(name = "CODE_ID")
	public String CODE_ID;

	@Column(name = "TYPE_ID")
	public String TYPE_ID;

	@Column(name = "TYPE_DESCRIPTION")
	public String TYPE_DESCRIPTION;

	@Column(name = "GOS_NUMBER")
	public String GOS_NUMBER;

	@Column(name = "MARKA")
	public String MARKA;

	@Column(name = "ORG_TYPE")
	public String ORG_TYPE;

	@Column(name = "TECH_CODE")
	public String TECH_CODE;

	@Column(name = "DATE_TO")
	public String DATE_TO;

	@Column(name = "ENABLED_FLAG")
	public String ENABLED_FLAG;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpecTechnic that = (SpecTechnic) o;

		return CODE_ID != null ? CODE_ID.equals(that.CODE_ID) : that.CODE_ID == null;
	}

	@Override
	public int hashCode() {
		return CODE_ID != null ? CODE_ID.hashCode() : 0;
	}
}
