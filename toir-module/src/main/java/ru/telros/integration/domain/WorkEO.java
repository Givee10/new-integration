package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_EO")
@SequenceGenerator(name = "idGenerator", sequenceName = "WORK_EO_SEQ", allocationSize = 1)
public class WorkEO extends AbstractEntity {
	@Column(name = "WIP_ENTITY_ID")
	public String WIP_ENTITY_ID;

	@Column(name = "ELEMENT_ID")
	public String ELEMENT_ID;

	@Column(name = "EO_TYPE")
	public String EO_TYPE;

	@Column(name = "EO_TYPE_DESCR")
	public String EO_TYPE_DESCR;

	@Column(name = "ADDR")
	public String ADDR;

	@Column(name = "P1")
	public String P1;

	@Column(name = "PROMPT_PARAM1")
	public String PROMPT_PARAM1;

	@Column(name = "P2")
	public String P2;

	@Column(name = "PROMPT_PARAM2")
	public String PROMPT_PARAM2;

	@Column(name = "P3")
	public String P3;

	@Column(name = "PROMPT_PARAM3")
	public String PROMPT_PARAM3;

	@Column(name = "P4")
	public String P4;

	@Column(name = "PROMPT_PARAM4")
	public String PROMPT_PARAM4;

	@Column(name = "P5")
	public String P5;

	@Column(name = "PROMPT_PARAM5")
	public String PROMPT_PARAM5;

	@Column(name = "P6")
	public String P6;

	@Column(name = "PROMPT_PARAM6")
	public String PROMPT_PARAM6;

	@Column(name = "P7")
	public String P7;

	@Column(name = "PROMPT_PARAM7")
	public String PROMPT_PARAM7;

	@Column(name = "P8")
	public String P8;

	@Column(name = "PROMPT_PARAM8")
	public String PROMPT_PARAM8;

	@Column(name = "P9")
	public String P9;

	@Column(name = "PROMPT_PARAM9")
	public String PROMPT_PARAM9;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WorkEO workEO = (WorkEO) o;

		if (WIP_ENTITY_ID != null ? !WIP_ENTITY_ID.equals(workEO.WIP_ENTITY_ID) : workEO.WIP_ENTITY_ID != null)
			return false;
		return EO_TYPE != null ? EO_TYPE.equals(workEO.EO_TYPE) : workEO.EO_TYPE == null;
	}

	@Override
	public int hashCode() {
		int result = WIP_ENTITY_ID != null ? WIP_ENTITY_ID.hashCode() : 0;
		result = 31 * result + (EO_TYPE != null ? EO_TYPE.hashCode() : 0);
		return result;
	}

	@Column(name = "OPER_TYPE")
	public String OPER_TYPE;
}
