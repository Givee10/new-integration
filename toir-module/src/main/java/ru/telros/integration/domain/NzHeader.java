package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "NZ_HEADER")
@SequenceGenerator(name = "idGenerator", sequenceName = "NZ_HEADER_SEQ", allocationSize = 1)
public class NzHeader extends AbstractEntity {
	@Column(name = "ORDER_SET_ID")
	public String ORDER_SET_ID;

	@Column(name = "ORGANIZATION_ID")
	public String ORGANIZATION_ID;

	@Column(name = "ORDER_SET_DATE")
	public String ORDER_SET_DATE;

	@Column(name = "DEPARTMENT_ID")
	public String DEPARTMENT_ID;

	@Column(name = "CREATION_DATE")
	public String CREATION_DATE;

	@Column(name = "AFFIRMED")
	public String AFFIRMED;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NzHeader nzHeader = (NzHeader) o;

		return ORDER_SET_ID != null ? ORDER_SET_ID.equals(nzHeader.ORDER_SET_ID) : nzHeader.ORDER_SET_ID == null;
	}

	@Override
	public int hashCode() {
		return ORDER_SET_ID != null ? ORDER_SET_ID.hashCode() : 0;
	}
}
