package ru.telros.integration.domain;

import java.util.ArrayList;
import java.util.List;

public class TOIRCollectionDto {
	private List<EODto> eoDtoList = new ArrayList<>();
	private List<MetricDto> metricDtoList = new ArrayList<>();
	private List<OperationDto> operationDtoList = new ArrayList<>();
	private String inventoryItemId;

	public List<EODto> getEoDtoList() {
		return eoDtoList;
	}

	public void setEoDtoList(List<EODto> eoDtoList) {
		this.eoDtoList = eoDtoList;
	}

	public List<MetricDto> getMetricDtoList() {
		return metricDtoList;
	}

	public void setMetricDtoList(List<MetricDto> metricDtoList) {
		this.metricDtoList = metricDtoList;
	}

	public List<OperationDto> getOperationDtoList() {
		return operationDtoList;
	}

	public void setOperationDtoList(List<OperationDto> operationDtoList) {
		this.operationDtoList = operationDtoList;
	}

	public String getInventoryItemId() {
		return inventoryItemId;
	}

	public void setInventoryItemId(String inventoryItemId) {
		this.inventoryItemId = inventoryItemId;
	}
}
