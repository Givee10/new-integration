package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "METRIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "METRIC_SEQ", allocationSize = 1)
public class Metric extends AbstractEntity {
	@Column(name = "INVENTORY_ITEM_ID")
	public String INVENTORY_ITEM_ID;

	@Column(name = "OPER")
	public String OPER;

	@Column(name = "METRICS_CODE")
	public String METRICS_CODE;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Metric metric = (Metric) o;

		if (INVENTORY_ITEM_ID != null ? !INVENTORY_ITEM_ID.equals(metric.INVENTORY_ITEM_ID) : metric.INVENTORY_ITEM_ID != null)
			return false;
		return METRICS_CODE != null ? METRICS_CODE.equals(metric.METRICS_CODE) : metric.METRICS_CODE == null;
	}

	@Override
	public int hashCode() {
		int result = INVENTORY_ITEM_ID != null ? INVENTORY_ITEM_ID.hashCode() : 0;
		result = 31 * result + (METRICS_CODE != null ? METRICS_CODE.hashCode() : 0);
		return result;
	}
}
