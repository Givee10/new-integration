package ru.telros.integration.domain;

public class EODto {
	private Long id;
	private String wipEntityId;
	private String eoType;
	private String eoDescription;
	private String eoLength;
	private String eoDiametr;
	private String eoMatType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getEoType() {
		return eoType;
	}

	public void setEoType(String eoType) {
		this.eoType = eoType;
	}

	public String getEoDescription() {
		return eoDescription;
	}

	public void setEoDescription(String eoDescription) {
		this.eoDescription = eoDescription;
	}

	public String getEoLength() {
		return eoLength;
	}

	public void setEoLength(String eoLength) {
		this.eoLength = eoLength;
	}

	public String getEoDiametr() {
		return eoDiametr;
	}

	public void setEoDiametr(String eoDiametr) {
		this.eoDiametr = eoDiametr;
	}

	public String getEoMatType() {
		return eoMatType;
	}

	public void setEoMatType(String eoMatType) {
		this.eoMatType = eoMatType;
	}
}
