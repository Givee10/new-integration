package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_DET_NORM")
@SequenceGenerator(name = "idGenerator", sequenceName = "WORK_DET_NORM_SEQ", allocationSize = 1)
public class WorkDetNorm extends AbstractEntity {
	@Column(name = "WIP_ENTITY_ID")
	public String WIP_ENTITY_ID;

	@Column(name = "ACTION_SEQ_ID")
	public String ACTION_SEQ_ID;

	@Column(name = "OPERATION_DESCRIPTION")
	public String OPERATION_DESCRIPTION;

	@Column(name = "RESOURCE_CODE")
	public String RESOURCE_CODE;

	@Column(name = "CALC_NORM")
	public String CALC_NORM;

	@Column(name = "METRICS_CODE")
	public String METRICS_CODE;

	@Column(name = "EO_ID")
	public String EO_ID;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WorkDetNorm that = (WorkDetNorm) o;

		if (WIP_ENTITY_ID != null ? !WIP_ENTITY_ID.equals(that.WIP_ENTITY_ID) : that.WIP_ENTITY_ID != null)
			return false;
		return ACTION_SEQ_ID != null ? ACTION_SEQ_ID.equals(that.ACTION_SEQ_ID) : that.ACTION_SEQ_ID == null;
	}

	@Override
	public int hashCode() {
		int result = WIP_ENTITY_ID != null ? WIP_ENTITY_ID.hashCode() : 0;
		result = 31 * result + (ACTION_SEQ_ID != null ? ACTION_SEQ_ID.hashCode() : 0);
		return result;
	}
}
