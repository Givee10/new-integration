package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_MAP")
@SequenceGenerator(name = "idGenerator", sequenceName = "WORK_MAP_SEQ", allocationSize = 1)
public class WorkMap extends AbstractEntity {
	@Column(name = "WIP_ENTITY_ID")
	public String WIP_ENTITY_ID;

	@Column(name = "OPERATION_SEQ_NUM")
	public String OPERATION_SEQ_NUM;

	@Column(name = "OPERATION", length = 4000)
	public String OPERATION;

	@Column(name = "IS_SIMPLE_WORK")
	public String IS_SIMPLE_WORK;

	@Column(name = "METRICS_CODE")
	public String METRICS_CODE;

	@Column(name = "ACTIVE_FLAG")
	public String ACTIVE_FLAG;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WorkMap workMap = (WorkMap) o;

		if (WIP_ENTITY_ID != null ? !WIP_ENTITY_ID.equals(workMap.WIP_ENTITY_ID) : workMap.WIP_ENTITY_ID != null)
			return false;
		return OPERATION_SEQ_NUM != null ? OPERATION_SEQ_NUM.equals(workMap.OPERATION_SEQ_NUM) : workMap.OPERATION_SEQ_NUM == null;
	}

	@Override
	public int hashCode() {
		int result = WIP_ENTITY_ID != null ? WIP_ENTITY_ID.hashCode() : 0;
		result = 31 * result + (OPERATION_SEQ_NUM != null ? OPERATION_SEQ_NUM.hashCode() : 0);
		return result;
	}
}
