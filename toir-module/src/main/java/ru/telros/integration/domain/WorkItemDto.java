package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.telros.integration.DatesUtil;

import java.time.ZonedDateTime;

public class WorkItemDto {
	private Long id;
	private String guid;
	private String code;
	private String description;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDate;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDatePlan;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDate;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDatePlan;
	private String status;
	private Integer order;
	private Double duration;
	private Double durationCalc;
	@JsonProperty
	private Boolean isPhotoRequire;
	@JsonProperty
	private Boolean isCallRequire;
	@JsonProperty
	private Boolean isActRequire;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(ZonedDateTime finishDate) {
		this.finishDate = finishDate;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Double getDuration() {
		return duration;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}

	public Double getDurationCalc() {
		return durationCalc;
	}

	public void setDurationCalc(Double durationCalc) {
		this.durationCalc = durationCalc;
	}

	@JsonIgnore
	public Boolean getPhotoRequire() {
		return isPhotoRequire;
	}

	public void setPhotoRequire(Boolean photoRequire) {
		isPhotoRequire = photoRequire;
	}

	@JsonIgnore
	public Boolean getCallRequire() {
		return isCallRequire;
	}

	public void setCallRequire(Boolean callRequire) {
		isCallRequire = callRequire;
	}

	@JsonIgnore
	public Boolean getActRequire() {
		return isActRequire;
	}

	public void setActRequire(Boolean actRequire) {
		isActRequire = actRequire;
	}

	@Override
	public String toString() {
		return description;
	}
}
