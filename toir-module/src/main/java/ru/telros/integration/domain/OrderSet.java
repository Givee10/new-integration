package ru.telros.integration.domain;

public class OrderSet {
	public Long XXBR_SET_ID;
	public Long ORDER_SET_RAW_ID;
	public Long ORGANIZATION_ID;
	public String ORGANIZATION_NAME;
	public String ORDER_SET_DATE;
	public Long DEPARTMENT_ID;
	public String DEPARTMENT_NAME;
	public String LAST_UPDATE_DATE;
	public String LAST_UPDATED_BY;
	public String CREATION_DATE;
	public String CREATED_BY;
	public String START_TIME;
	public String END_TIME;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("OrderSet{");
		sb.append("XXBR_SET_ID=").append(XXBR_SET_ID);
		sb.append(", ORDER_SET_RAW_ID=").append(ORDER_SET_RAW_ID);
		sb.append(", ORGANIZATION_ID=").append(ORGANIZATION_ID);
		sb.append(", ORGANIZATION_NAME='").append(ORGANIZATION_NAME).append('\'');
		sb.append(", ORDER_SET_DATE='").append(ORDER_SET_DATE).append('\'');
		sb.append(", DEPARTMENT_ID=").append(DEPARTMENT_ID);
		sb.append(", DEPARTMENT_NAME='").append(DEPARTMENT_NAME).append('\'');
		sb.append(", LAST_UPDATE_DATE='").append(LAST_UPDATE_DATE).append('\'');
		sb.append(", LAST_UPDATED_BY='").append(LAST_UPDATED_BY).append('\'');
		sb.append(", CREATION_DATE='").append(CREATION_DATE).append('\'');
		sb.append(", CREATED_BY='").append(CREATED_BY).append('\'');
		sb.append(", START_TIME='").append(START_TIME).append('\'');
		sb.append(", END_TIME='").append(END_TIME).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
