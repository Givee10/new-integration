package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OrderRes {
	public Long XXBR_SET_ID;
	public Long XXBR_WIP_ENTITY_ID;
	public String EMPLOYEER_CODE;
	public String START_TIME;
	public String END_TIME;
	public String FACT_TIME;
	public Long PERSON_ID;
	//@JsonIgnore
	public String TYPE;
	@JsonIgnore
	public String NUMBER;
	@JsonIgnore
	public String CODE;
	public Long SMM_ID;
	public Long CODE_ID;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("OrderRes{");
		sb.append("XXBR_SET_ID=").append(XXBR_SET_ID);
		sb.append(", XXBR_WIP_ENTITY_ID=").append(XXBR_WIP_ENTITY_ID);
		sb.append(", EMPLOYEER_CODE='").append(EMPLOYEER_CODE).append('\'');
		sb.append(", START_TIME='").append(START_TIME).append('\'');
		sb.append(", END_TIME='").append(END_TIME).append('\'');
		sb.append(", FACT_TIME='").append(FACT_TIME).append('\'');
		sb.append(", PERSON_ID=").append(PERSON_ID);
		sb.append(", TYPE='").append(TYPE).append('\'');
		sb.append(", NUMBER='").append(NUMBER).append('\'');
		sb.append(", CODE='").append(CODE).append('\'');
		sb.append(", SMM_ID=").append(SMM_ID);
		sb.append(", CODE_ID=").append(CODE_ID);
		sb.append('}');
		return sb.toString();
	}
}
