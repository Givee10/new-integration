package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SMALL_TECHNIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "SMALL_TECHNIC_SEQ", allocationSize = 1)
public class SmallTechnic extends AbstractEntity {
	@Column(name = "CODE_ID")
	public String CODE_ID;

	@Column(name = "TECH_DESCRIPTION")
	public String TECH_DESCRIPTION;

	@Column(name = "ORG_TYPE")
	public String ORG_TYPE;

	@Column(name = "TECH_CODE")
	public String TECH_CODE;

	@Column(name = "ENABLED_FLAG")
	public String ENABLED_FLAG;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SmallTechnic that = (SmallTechnic) o;

		return CODE_ID != null ? CODE_ID.equals(that.CODE_ID) : that.CODE_ID == null;
	}

	@Override
	public int hashCode() {
		return CODE_ID != null ? CODE_ID.hashCode() : 0;
	}
}
