package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TYPE_ELEMENT")
@SequenceGenerator(name = "idGenerator", sequenceName = "TYPE_ELEMENT_SEQ", allocationSize = 1)
public class TypeElement extends AbstractEntity {
	@Column(name = "EO_TYPE")
	public String EO_TYPE;

	@Column(name = "EO_TYPE_DESCR")
	public String EO_TYPE_DESCR;

	@Column(name = "SHORT_DESCR")
	public String SHORT_DESCR;

	@Column(name = "PRIMARY_UOM")
	public String PRIMARY_UOM;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TypeElement that = (TypeElement) o;

		return EO_TYPE != null ? EO_TYPE.equals(that.EO_TYPE) : that.EO_TYPE == null;
	}

	@Override
	public int hashCode() {
		return EO_TYPE != null ? EO_TYPE.hashCode() : 0;
	}
}
