package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BRIGADE_STRUCTURE")
@SequenceGenerator(name = "idGenerator", sequenceName = "BRIGADE_STRUCTURE_SEQ", allocationSize = 1)
public class BrigadeStructure extends AbstractEntity {
	@Column(name = "DEPARTMENT_ID")
	public String DEPARTMENT_ID;

	@Column(name = "DEPARTMENT_CODE")
	public String DEPARTMENT_CODE;

	@Column(name = "DESCRIPTION")
	public String DESCRIPTION;

	@Column(name = "RESOURCE_CODE")
	public String RESOURCE_CODE;

	@Column(name = "IS_BRIGADIER")
	public String IS_BRIGADIER;

	@Column(name = "PERSON_ID")
	public String PERSON_ID;

	@Column(name = "EMPLOYEE_NUMBER")
	public String EMPLOYEE_NUMBER;

	@Column(name = "FULL_NAME")
	public String FULL_NAME;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BrigadeStructure that = (BrigadeStructure) o;

		if (DEPARTMENT_ID != null ? !DEPARTMENT_ID.equals(that.DEPARTMENT_ID) : that.DEPARTMENT_ID != null)
			return false;
		if (RESOURCE_CODE != null ? !RESOURCE_CODE.equals(that.RESOURCE_CODE) : that.RESOURCE_CODE != null)
			return false;
		return PERSON_ID != null ? PERSON_ID.equals(that.PERSON_ID) : that.PERSON_ID == null;
	}

	@Override
	public int hashCode() {
		int result = DEPARTMENT_ID != null ? DEPARTMENT_ID.hashCode() : 0;
		result = 31 * result + (RESOURCE_CODE != null ? RESOURCE_CODE.hashCode() : 0);
		result = 31 * result + (PERSON_ID != null ? PERSON_ID.hashCode() : 0);
		return result;
	}
}
