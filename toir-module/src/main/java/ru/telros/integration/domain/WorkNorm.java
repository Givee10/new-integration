package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_NORM")
@SequenceGenerator(name = "idGenerator", sequenceName = "WORK_NORM_SEQ", allocationSize = 1)
public class WorkNorm extends AbstractEntity {
	@Column(name = "WIP_ENTITY_ID")
	public String WIP_ENTITY_ID;

	@Column(name = "RESOURCE_CODE")
	public String RESOURCE_CODE;

	@Column(name = "S")
	public String S;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WorkNorm workNorm = (WorkNorm) o;

		if (WIP_ENTITY_ID != null ? !WIP_ENTITY_ID.equals(workNorm.WIP_ENTITY_ID) : workNorm.WIP_ENTITY_ID != null)
			return false;
		return RESOURCE_CODE != null ? RESOURCE_CODE.equals(workNorm.RESOURCE_CODE) : workNorm.RESOURCE_CODE == null;
	}

	@Override
	public int hashCode() {
		int result = WIP_ENTITY_ID != null ? WIP_ENTITY_ID.hashCode() : 0;
		result = 31 * result + (RESOURCE_CODE != null ? RESOURCE_CODE.hashCode() : 0);
		return result;
	}
}
