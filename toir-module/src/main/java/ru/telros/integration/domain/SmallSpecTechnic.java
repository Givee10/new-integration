package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SMALL_SPEC_TECHNIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "SMALL_SPEC_TECHNIC_SEQ", allocationSize = 1)
public class SmallSpecTechnic extends AbstractEntity {
	@Column(name = "SMM_ID")
	public String SMM_ID;

	@Column(name = "CODE_ID")
	public String CODE_ID;

	@Column(name = "TECH_CODE")
	public String TECH_CODE;

	@Column(name = "INV_NUMBER")
	public String INV_NUMBER;

	@Column(name = "INV_DESCRIPTION", length = 2000)
	public String INV_DESCRIPTION;

	@Column(name = "MOL")
	public String MOL;

	@Column(name = "ORGANIZATION_ID")
	public String ORGANIZATION_ID;

	@Column(name = "ENABLED_FLAG")
	public String ENABLED_FLAG;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SmallSpecTechnic that = (SmallSpecTechnic) o;

		return SMM_ID != null ? SMM_ID.equals(that.SMM_ID) : that.SMM_ID == null;
	}

	@Override
	public int hashCode() {
		return SMM_ID != null ? SMM_ID.hashCode() : 0;
	}
}
