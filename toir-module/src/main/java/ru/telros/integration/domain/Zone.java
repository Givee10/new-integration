package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ZONE")
@SequenceGenerator(name = "idGenerator", sequenceName = "ZONE_SEQ", allocationSize = 1)
public class Zone extends AbstractEntity {
	@Column(name = "ORGANIZATION_ID")
	public String ORGANIZATION_ID;

	@Column(name = "ORGANIZATION_CODE")
	public String ORGANIZATION_CODE;

	@Column(name = "ORGANIZATION_NAME")
	public String ORGANIZATION_NAME;

	@Column(name = "ORGANIZATION_ID_PARENT")
	public String ORGANIZATION_ID_PARENT;

	@Column(name = "D_PARENT_NAME")
	public String D_PARENT_NAME;

	@Column(name = "DISABLE_DATE")
	public String DISABLE_DATE;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Zone zone = (Zone) o;

		return ORGANIZATION_ID != null ? ORGANIZATION_ID.equals(zone.ORGANIZATION_ID) : zone.ORGANIZATION_ID == null;
	}

	@Override
	public int hashCode() {
		return ORGANIZATION_ID != null ? ORGANIZATION_ID.hashCode() : 0;
	}
}
