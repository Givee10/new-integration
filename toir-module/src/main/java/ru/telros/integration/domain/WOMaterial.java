package ru.telros.integration.domain;

public class WOMaterial {
	public Long XXBR_SET_ID;
	public Long XXBR_WIP_ENTITY_ID;
	public String XXBR_MATERIAL_NAME;
	public String XXBR_MATERIAL_UOM;
	public String XXBR_QUANTITY;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WOMaterial{");
		sb.append("XXBR_SET_ID=").append(XXBR_SET_ID);
		sb.append(", XXBR_WIP_ENTITY_ID=").append(XXBR_WIP_ENTITY_ID);
		sb.append(", XXBR_MATERIAL_NAME='").append(XXBR_MATERIAL_NAME).append('\'');
		sb.append(", XXBR_MATERIAL_UOM='").append(XXBR_MATERIAL_UOM).append('\'');
		sb.append(", XXBR_QUANTITY='").append(XXBR_QUANTITY).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
