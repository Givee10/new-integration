package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeWorkItemDto {
	private Long id;
	private String guid, code, description;
	private Double norm;
	@JsonProperty
	private Boolean isPhotoRequire, isCallRequire, isActRequire;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getNorm() {
		return norm;
	}

	public void setNorm(Double norm) {
		this.norm = norm;
	}

	@JsonIgnore
	public Boolean getPhotoRequire() {
		return isPhotoRequire;
	}

	public void setPhotoRequire(Boolean photoRequire) {
		isPhotoRequire = photoRequire;
	}

	@JsonIgnore
	public Boolean getCallRequire() {
		return isCallRequire;
	}

	public void setCallRequire(Boolean callRequire) {
		isCallRequire = callRequire;
	}

	@JsonIgnore
	public Boolean getActRequire() {
		return isActRequire;
	}

	public void setActRequire(Boolean actRequire) {
		isActRequire = actRequire;
	}

	@Override
	public String toString() {
		return description;
	}
}
