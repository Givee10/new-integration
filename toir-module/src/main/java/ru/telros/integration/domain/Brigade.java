package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BRIGADE")
@SequenceGenerator(name = "idGenerator", sequenceName = "BRIGADE_SEQ", allocationSize = 1)
public class Brigade extends AbstractEntity {
	@Column(name = "DEPARTMENT_ID")
	public String DEPARTMENT_ID;

	@Column(name = "DEPARTMENT_CODE")
	public String DEPARTMENT_CODE;

	@Column(name = "DESCRIPTION")
	public String DESCRIPTION;

	@Column(name = "ORGANIZATION_ID")
	public String ORGANIZATION_ID;

	@Column(name = "ORGANIZATION_CODE")
	public String ORGANIZATION_CODE;

	@Column(name = "ORGANIZATION_NAME")
	public String ORGANIZATION_NAME;

	@Column(name = "DISABLE_DATE")
	public String DISABLE_DATE;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Brigade brigade = (Brigade) o;

		return DEPARTMENT_ID != null ? DEPARTMENT_ID.equals(brigade.DEPARTMENT_ID) : brigade.DEPARTMENT_ID == null;
	}

	@Override
	public int hashCode() {
		return DEPARTMENT_ID != null ? DEPARTMENT_ID.hashCode() : 0;
	}
}
