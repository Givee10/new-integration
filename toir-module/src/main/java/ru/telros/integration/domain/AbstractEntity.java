package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class AbstractEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenerator")
	@Column(name = "ID")
	public Long ID;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHANGE_DATE", nullable = false)
	public Date CHANGE_DATE;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false)
	public Date CREATE_DATE;

	@Column(name = "IS_ACTIVE")
	public Boolean IS_ACTIVE = true;
}
