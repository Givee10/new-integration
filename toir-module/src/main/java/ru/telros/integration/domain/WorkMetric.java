package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_METRIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "WORK_METRIC_SEQ", allocationSize = 1)
public class WorkMetric extends AbstractEntity {
	@Column(name = "WIP_ENTITY_ID")
	public String WIP_ENTITY_ID;

	@Column(name = "METRICS_CODE")
	public String METRICS_CODE;

	@Column(name = "M_QUANTITY")
	public String M_QUANTITY;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WorkMetric that = (WorkMetric) o;

		if (WIP_ENTITY_ID != null ? !WIP_ENTITY_ID.equals(that.WIP_ENTITY_ID) : that.WIP_ENTITY_ID != null)
			return false;
		return METRICS_CODE != null ? METRICS_CODE.equals(that.METRICS_CODE) : that.METRICS_CODE == null;
	}

	@Override
	public int hashCode() {
		int result = WIP_ENTITY_ID != null ? WIP_ENTITY_ID.hashCode() : 0;
		result = 31 * result + (METRICS_CODE != null ? METRICS_CODE.hashCode() : 0);
		return result;
	}
}
