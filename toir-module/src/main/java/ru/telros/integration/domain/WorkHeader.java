package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_HEADER")
@SequenceGenerator(name = "idGenerator", sequenceName = "WORK_HEADER_SEQ", allocationSize = 1)
public class WorkHeader extends AbstractEntity {
	@Column(name = "WIP_ENTITY_ID")
	public String WIP_ENTITY_ID;

	@Column(name = "ORGANIZATION_ID")
	public String ORGANIZATION_ID;

	@Column(name = "PER_ORG_ID")
	public String PER_ORG_ID;

	@Column(name = "DOC_SOURCE")
	public String DOC_SOURCE;

	@Column(name = "DOC_NUMBER")
	public String DOC_NUMBER;

	@Column(name = "INVENTORY_ITEM_ID")
	public String INVENTORY_ITEM_ID;

	@Column(name = "PRF_ID", length = 4000)
	public String PRF_ID;

	@Column(name = "ADDR_ID", length = 4000)
	public String ADDR_ID;

	@Column(name = "ADDR_COMMENT", length = 4000)
	public String ADDR_COMMENT;

	@Column(name = "WA_ADDRESS", length = 4000)
	public String WA_ADDRESS;

	@Column(name = "SCHEDULED_START_DATE")
	public String SCHEDULED_START_DATE;

	@Column(name = "GENERAL_FACT")
	public String GENERAL_FACT;

	@Column(name = "MAN_HOURS")
	public String MAN_HOURS;

	@Column(name = "DEPARTMENT_ID")
	public String DEPARTMENT_ID;

	@Column(name = "ORDER_SET_ID")
	public String ORDER_SET_ID;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WorkHeader that = (WorkHeader) o;

		return WIP_ENTITY_ID != null ? WIP_ENTITY_ID.equals(that.WIP_ENTITY_ID) : that.WIP_ENTITY_ID == null;
	}

	@Override
	public int hashCode() {
		return WIP_ENTITY_ID != null ? WIP_ENTITY_ID.hashCode() : 0;
	}
}
