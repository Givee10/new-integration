package ru.telros.integration.domain;

public class WorkHeadDto {
	protected String wipEntityId;
	protected String inventoryItemId;
	protected String scheduledStartDate;

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getInventoryItemId() {
		return inventoryItemId;
	}

	public void setInventoryItemId(String inventoryItemId) {
		this.inventoryItemId = inventoryItemId;
	}

	public String getScheduledStartDate() {
		return scheduledStartDate;
	}

	public void setScheduledStartDate(String scheduledStartDate) {
		this.scheduledStartDate = scheduledStartDate;
	}
}
