package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TECHNIC_TYPE")
@SequenceGenerator(name = "idGenerator", sequenceName = "TECHNIC_TYPE_SEQ", allocationSize = 1)
public class TechnicType extends AbstractEntity {
	@Column(name = "TYPE_ID")
	public String TYPE_ID;

	@Column(name = "TYPE_NAME")
	public String TYPE_NAME;

	@Column(name = "TYPE_DESCRIPTION")
	public String TYPE_DESCRIPTION;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TechnicType that = (TechnicType) o;

		return TYPE_ID != null ? TYPE_ID.equals(that.TYPE_ID) : that.TYPE_ID == null;
	}

	@Override
	public int hashCode() {
		return TYPE_ID != null ? TYPE_ID.hashCode() : 0;
	}
}
