package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "RESOURCES")
@SequenceGenerator(name = "idGenerator", sequenceName = "RESOURCES_SEQ", allocationSize = 1)
public class Resource extends AbstractEntity {
	@Column(name = "RESOURCE_ID")
	public String RESOURCE_ID;

	@Column(name = "RESOURCE_CODE")
	public String RESOURCE_CODE;

	@Column(name = "DESCRIPTION")
	public String DESCRIPTION;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Resource resource = (Resource) o;

		return RESOURCE_ID != null ? RESOURCE_ID.equals(resource.RESOURCE_ID) : resource.RESOURCE_ID == null;
	}

	@Override
	public int hashCode() {
		return RESOURCE_ID != null ? RESOURCE_ID.hashCode() : 0;
	}
}
