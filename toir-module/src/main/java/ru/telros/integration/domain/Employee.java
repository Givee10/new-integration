package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
@SequenceGenerator(name = "idGenerator", sequenceName = "EMPLOYEE_SEQ", allocationSize = 1)
public class Employee extends AbstractEntity {
	@Column(name = "PERSON_ID")
	public String PERSON_ID;

	@Column(name = "EMPLOYEE_NUMBER")
	public String EMPLOYEE_NUMBER;

	@Column(name = "LAST_NAME")
	public String LAST_NAME;

	@Column(name = "MIDDLE_NAMES")
	public String MIDDLE_NAMES;

	@Column(name = "FIRST_NAME")
	public String FIRST_NAME;

	@Column(name = "FULL_NAME")
	public String FULL_NAME;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Employee employee = (Employee) o;

		return PERSON_ID != null ? PERSON_ID.equals(employee.PERSON_ID) : employee.PERSON_ID == null;
	}

	@Override
	public int hashCode() {
		return PERSON_ID != null ? PERSON_ID.hashCode() : 0;
	}
}
