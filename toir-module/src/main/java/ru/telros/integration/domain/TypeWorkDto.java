package ru.telros.integration.domain;

public class TypeWorkDto {
	private Long id;
	private String guid, code, description, numberToir;
	private Double norm;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getNorm() {
		return norm;
	}

	public void setNorm(Double norm) {
		this.norm = norm;
	}

	public String getNumberToir() {
		return numberToir;
	}

	public void setNumberToir(String numberToir) {
		this.numberToir = numberToir;
	}

	@Override
	public String toString() {
		return description;
	}
}
