package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EmployeeDto {
	private Long id, idUser;
	private String lastName, firstName, middleName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	@JsonIgnore
	public String getFullName() {
		return lastName + " " + firstName + " " + middleName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		EmployeeDto that = (EmployeeDto) o;

		if (!id.equals(that.id)) return false;
		if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
		if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
		return middleName != null ? middleName.equals(that.middleName) : that.middleName == null;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return getFullName();
	}
}
