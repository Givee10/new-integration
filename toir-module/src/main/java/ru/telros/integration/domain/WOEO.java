package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WOEO {
	public Long XXBR_WIP_ENTITY_ID;
	public Long XXBR_ELEMENT_ID;
	public Long ELEMENT_RAW_ID;
	public String EO_TYPE;
	@JsonIgnore
	public String EO_TYPE_DESCR;
	public String EO_LENGTH;
	@JsonIgnore
	public String EO_LENGTH_UNIT;
	public String EO_DIAMETR;
	public String EO_MAT_TYPE;
	public String ADDR_STREET_ID;
	public String ADDR_BUILD_ID;
	public String ADDR_COMMENT;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WOEO{");
		sb.append("XXBR_WIP_ENTITY_ID=").append(XXBR_WIP_ENTITY_ID);
		sb.append(", XXBR_ELEMENT_ID=").append(XXBR_ELEMENT_ID);
		sb.append(", ELEMENT_RAW_ID=").append(ELEMENT_RAW_ID);
		sb.append(", EO_TYPE='").append(EO_TYPE).append('\'');
		sb.append(", EO_TYPE_DESCR='").append(EO_TYPE_DESCR).append('\'');
		sb.append(", EO_LENGTH='").append(EO_LENGTH).append('\'');
		sb.append(", EO_LENGTH_UNIT='").append(EO_LENGTH_UNIT).append('\'');
		sb.append(", EO_DIAMETR='").append(EO_DIAMETR).append('\'');
		sb.append(", EO_MAT_TYPE='").append(EO_MAT_TYPE).append('\'');
		sb.append(", ADDR_STREET_ID='").append(ADDR_STREET_ID).append('\'');
		sb.append(", ADDR_BUILD_ID='").append(ADDR_BUILD_ID).append('\'');
		sb.append(", ADDR_COMMENT='").append(ADDR_COMMENT).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
