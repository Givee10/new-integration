package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WORouting {
	public Long XXBR_WIP_ENTITY_ID;
	public Long ACTION_SEQ_ID;
	public String ACTIVE_FLAG;
	public Long OPERATION_ITEM_ID;
	public Long OPERATION_SEQ_NUM;
	public String OPERATION_DESCRIPTION;
	@JsonIgnore
	public String OPERATION_CODE;
	@JsonIgnore
	public String OPER;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WORouting{");
		sb.append("XXBR_WIP_ENTITY_ID=").append(XXBR_WIP_ENTITY_ID);
		sb.append(", ACTION_SEQ_ID=").append(ACTION_SEQ_ID);
		sb.append(", ACTIVE_FLAG='").append(ACTIVE_FLAG).append('\'');
		sb.append(", OPERATION_ITEM_ID=").append(OPERATION_ITEM_ID);
		sb.append(", OPERATION_SEQ_NUM=").append(OPERATION_SEQ_NUM);
		sb.append(", OPERATION_DESCRIPTION='").append(OPERATION_DESCRIPTION).append('\'');
		sb.append(", OPERATION_CODE='").append(OPERATION_CODE).append('\'');
		sb.append(", OPER='").append(OPER).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
