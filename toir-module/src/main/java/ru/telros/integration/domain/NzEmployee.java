package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "NZ_EMPLOYEE")
@SequenceGenerator(name = "idGenerator", sequenceName = "NZ_EMPLOYEE_SEQ", allocationSize = 1)
public class NzEmployee extends AbstractEntity {
	@Column(name = "ORDER_SET_ID")
	public String ORDER_SET_ID;

	@Column(name = "EMPLOYEER_CODE")
	public String EMPLOYEER_CODE;

	@Column(name = "PERSON_ID")
	public String PERSON_ID;

	@Column(name = "PLAN_TIME")
	public String PLAN_TIME;

	@Column(name = "ABSENCE_COMMENT")
	public String ABSENCE_COMMENT;

	@Column(name = "IS_BRIGADIER")
	public String IS_BRIGADIER;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NzEmployee that = (NzEmployee) o;

		if (ORDER_SET_ID != null ? !ORDER_SET_ID.equals(that.ORDER_SET_ID) : that.ORDER_SET_ID != null) return false;
		return PERSON_ID != null ? PERSON_ID.equals(that.PERSON_ID) : that.PERSON_ID == null;
	}

	@Override
	public int hashCode() {
		int result = ORDER_SET_ID != null ? ORDER_SET_ID.hashCode() : 0;
		result = 31 * result + (PERSON_ID != null ? PERSON_ID.hashCode() : 0);
		return result;
	}
}
