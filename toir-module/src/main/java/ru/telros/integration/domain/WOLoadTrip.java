package ru.telros.integration.domain;

public class WOLoadTrip {
	public String TYPE_WORK;
	public Long ORGANIZATION_ID;
	public Long XXBR_SET_ID;
	public String START_TIME;
	public String END_TIME;
	public String FACT_TIME;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WOLoadTrip{");
		sb.append("TYPE_WORK='").append(TYPE_WORK).append('\'');
		sb.append(", ORGANIZATION_ID=").append(ORGANIZATION_ID);
		sb.append(", XXBR_SET_ID=").append(XXBR_SET_ID);
		sb.append(", START_TIME='").append(START_TIME).append('\'');
		sb.append(", END_TIME='").append(END_TIME).append('\'');
		sb.append(", FACT_TIME='").append(FACT_TIME).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
