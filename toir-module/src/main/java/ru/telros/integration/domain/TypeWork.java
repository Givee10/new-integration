package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TYPE_WORK")
@SequenceGenerator(name = "idGenerator", sequenceName = "TYPE_WORK_SEQ", allocationSize = 1)
public class TypeWork extends AbstractEntity {
	@Column(name = "ACTIVITY_ASSOCIATION_ID")
	public String ACTIVITY_ASSOCIATION_ID;

	@Column(name = "ASSET_ACTIVITY_ID")
	public String ASSET_ACTIVITY_ID;

	@Column(name = "ORGANIZATION_ID")
	public String ORGANIZATION_ID;

	@Column(name = "ORGANIZATION_CODE")
	public String ORGANIZATION_CODE;

	@Column(name = "ACTIVITY")
	public String ACTIVITY;

	@Column(name = "ACTIVITY_DESCRIPTION")
	public String ACTIVITY_DESCRIPTION;

	@Column(name = "START_DATE_ACTIVE")
	public String START_DATE_ACTIVE;

	@Column(name = "END_DATE_ACTIVE")
	public String END_DATE_ACTIVE;

	@Column(name = "ATTRIBUTE2")
	public String ATTRIBUTE2;

	@Column(name = "AKT_NUM")
	public String AKT_NUM;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TypeWork typeWork = (TypeWork) o;

		return ACTIVITY_ASSOCIATION_ID != null ? ACTIVITY_ASSOCIATION_ID.equals(typeWork.ACTIVITY_ASSOCIATION_ID) : typeWork.ACTIVITY_ASSOCIATION_ID == null;
	}

	@Override
	public int hashCode() {
		return ACTIVITY_ASSOCIATION_ID != null ? ACTIVITY_ASSOCIATION_ID.hashCode() : 0;
	}
}
