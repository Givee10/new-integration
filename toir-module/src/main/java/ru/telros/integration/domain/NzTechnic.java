package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "NZ_TECHNIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "NZ_TECHNIC_SEQ", allocationSize = 1)
public class NzTechnic extends AbstractEntity {
	@Column(name = "ORDER_SET_ID")
	public String ORDER_SET_ID;

	@Column(name = "CODE_ID")
	public String CODE_ID;

	@Column(name = "PLAN_TIME")
	public String PLAN_TIME;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NzTechnic nzTechnic = (NzTechnic) o;

		if (ORDER_SET_ID != null ? !ORDER_SET_ID.equals(nzTechnic.ORDER_SET_ID) : nzTechnic.ORDER_SET_ID != null)
			return false;
		return CODE_ID != null ? CODE_ID.equals(nzTechnic.CODE_ID) : nzTechnic.CODE_ID == null;
	}

	@Override
	public int hashCode() {
		int result = ORDER_SET_ID != null ? ORDER_SET_ID.hashCode() : 0;
		result = 31 * result + (CODE_ID != null ? CODE_ID.hashCode() : 0);
		return result;
	}
}
