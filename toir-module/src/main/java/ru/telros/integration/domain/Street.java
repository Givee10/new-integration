package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "STREET")
@SequenceGenerator(name = "idGenerator", sequenceName = "STREET_SEQ", allocationSize = 1)
public class Street extends AbstractEntity {
	@Column(name = "PRF_ID")
	public String PRF_ID;

	@Column(name = "VT_ID")
	public String VT_ID;

	@Column(name = "VT_NAME", length = 2000)
	public String VT_NAME;

	@Column(name = "GEONIM_ID")
	public String GEONIM_ID;

	@Column(name = "GEONIM_PRINT_NAME", length = 2000)
	public String GEONIM_PRINT_NAME;

	@Column(name = "TPNM_ID")
	public String TPNM_ID;

	@Column(name = "TPNM_NAME", length = 2000)
	public String TPNM_NAME;

	@Column(name = "PRF_NAME", length = 2000)
	public String PRF_NAME;

	@Column(name = "PRF_CODE")
	public String PRF_CODE;

	@Column(name = "PRF_POST_INDEX")
	public String PRF_POST_INDEX;

	@Column(name = "PRF_PRINT_NAME", length = 2000)
	public String PRF_PRINT_NAME;

	@Column(name = "PRF_ACTIVATE_DATE")
	public String PRF_ACTIVATE_DATE;

	@Column(name = "PRF_DEACTIVATE_DATE")
	public String PRF_DEACTIVATE_DATE;

	@Column(name = "PRF_STATUS")
	public String PRF_STATUS;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Street street = (Street) o;

		return PRF_ID != null ? PRF_ID.equals(street.PRF_ID) : street.PRF_ID == null;
	}

	@Override
	public int hashCode() {
		return PRF_ID != null ? PRF_ID.hashCode() : 0;
	}
}
