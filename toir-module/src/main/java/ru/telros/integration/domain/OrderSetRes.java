package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OrderSetRes {
	public Long XXBR_SET_ID;
	public String EMPLOYEER_CODE;
	public String PLAN_TIME;
	public String FACT_TIME;
	public Long PERSON_ID;
	public String COMMENTS;
	@JsonIgnore
	public String ABSENCE_REASON;
	public String ABSENCE_COMMENT;
	public String ARRIVED;
	public String DEPARTED;
	public String IS_BRIGADIER;
	public String TIME_COMMENTS;
	public String ST_TIME;
	public String EN_TIME;
	@JsonIgnore
	public String TYPE;
	@JsonIgnore
	public String NUMBER;
	@JsonIgnore
	public String CODE;
	public Long SMM_ID;
	public Long CODE_ID;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("OrderSetRes{");
		sb.append("XXBR_SET_ID=").append(XXBR_SET_ID);
		sb.append(", EMPLOYEER_CODE='").append(EMPLOYEER_CODE).append('\'');
		sb.append(", PLAN_TIME='").append(PLAN_TIME).append('\'');
		sb.append(", FACT_TIME='").append(FACT_TIME).append('\'');
		sb.append(", PERSON_ID=").append(PERSON_ID);
		sb.append(", COMMENTS='").append(COMMENTS).append('\'');
		sb.append(", ABSENCE_REASON='").append(ABSENCE_REASON).append('\'');
		sb.append(", ABSENCE_COMMENT='").append(ABSENCE_COMMENT).append('\'');
		sb.append(", ARRIVED='").append(ARRIVED).append('\'');
		sb.append(", DEPARTED='").append(DEPARTED).append('\'');
		sb.append(", IS_BRIGADIER='").append(IS_BRIGADIER).append('\'');
		sb.append(", TIME_COMMENTS='").append(TIME_COMMENTS).append('\'');
		sb.append(", ST_TIME='").append(ST_TIME).append('\'');
		sb.append(", EN_TIME='").append(EN_TIME).append('\'');
		sb.append(", TYPE='").append(TYPE).append('\'');
		sb.append(", NUMBER='").append(NUMBER).append('\'');
		sb.append(", CODE='").append(CODE).append('\'');
		sb.append(", SMM_ID=").append(SMM_ID);
		sb.append(", CODE_ID=").append(CODE_ID);
		sb.append('}');
		return sb.toString();
	}
}
