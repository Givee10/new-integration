package ru.telros.integration.domain;

public class EmployeeBrigadePlanDto {
	private Long id;
	private Long idBrigadePlan;
	private Long idEmployee;
	private String lastName;
	private String firstName;
	private String middleName;
	private Boolean isManager;
	private Boolean isBrigadier;
	private String phone;
	private String post;
	private String absenceReason;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdBrigadePlan() {
		return idBrigadePlan;
	}

	public void setIdBrigadePlan(Long idBrigadePlan) {
		this.idBrigadePlan = idBrigadePlan;
	}

	public Long getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Long idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Boolean getManager() {
		return isManager;
	}

	public void setManager(Boolean manager) {
		isManager = manager;
	}

	public Boolean getBrigadier() {
		return isBrigadier;
	}

	public void setBrigadier(Boolean brigadier) {
		isBrigadier = brigadier;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getAbsenceReason() {
		return absenceReason;
	}

	public void setAbsenceReason(String absenceReason) {
		this.absenceReason = absenceReason;
	}
}
