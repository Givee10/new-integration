package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "NZ_SMALL_TECHNIC")
@SequenceGenerator(name = "idGenerator", sequenceName = "NZ_SMALL_TECHNIC_SEQ", allocationSize = 1)
public class NzSmallTechnic extends AbstractEntity {
	@Column(name = "ORDER_SET_ID")
	public String ORDER_SET_ID;

	@Column(name = "CODE_ID")
	public String CODE_ID;

	@Column(name = "SMM_ID")
	public String SMM_ID;

	@Column(name = "PLAN_TIME")
	public String PLAN_TIME;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NzSmallTechnic that = (NzSmallTechnic) o;

		if (ORDER_SET_ID != null ? !ORDER_SET_ID.equals(that.ORDER_SET_ID) : that.ORDER_SET_ID != null) return false;
		return SMM_ID != null ? SMM_ID.equals(that.SMM_ID) : that.SMM_ID == null;
	}

	@Override
	public int hashCode() {
		int result = ORDER_SET_ID != null ? ORDER_SET_ID.hashCode() : 0;
		result = 31 * result + (SMM_ID != null ? SMM_ID.hashCode() : 0);
		return result;
	}
}
