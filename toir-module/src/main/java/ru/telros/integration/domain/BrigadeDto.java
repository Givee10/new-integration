package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.telros.integration.DatesUtil;

import java.time.ZonedDateTime;

public class BrigadeDto {
	private Long id;
	private String description;
	private String name;
	private String location;
	private String phone;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime beginWork, endWork, lastNotifyDate;
	@JsonProperty
	private Boolean isMavr;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public ZonedDateTime getBeginWork() {
		return beginWork;
	}

	public void setBeginWork(ZonedDateTime beginWork) {
		this.beginWork = beginWork;
	}

	public ZonedDateTime getEndWork() {
		return endWork;
	}

	public void setEndWork(ZonedDateTime endWork) {
		this.endWork = endWork;
	}

	public ZonedDateTime getLastNotifyDate() {
		return lastNotifyDate;
	}

	public void setLastNotifyDate(ZonedDateTime lastNotifyDate) {
		this.lastNotifyDate = lastNotifyDate;
	}

	@JsonIgnore
	public Boolean getMavr() {
		return isMavr;
	}

	public void setMavr(Boolean mavr) {
		isMavr = mavr;
	}

	@Override
	public String toString() {
		return name;
	}
}
