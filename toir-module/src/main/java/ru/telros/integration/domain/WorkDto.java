package ru.telros.integration.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.integration.DatesUtil;

import java.time.ZonedDateTime;

public class WorkDto {
	private Long id;
	private String guid, number, description;
	private String location, status, stage, urgency, impact;
	private Number prior;
	private BrigadeDto brigade;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime registrationDate, startDate, startDatePlan, finishDate, finishDatePlan, closeDate;
	private String code, closeCode, organization;
	private Long idTypeWork, idBrigade, idPlan;
	private Boolean trafficOff, consumerOff;
	private String source, stausToir, address, updatedAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public Number getPrior() {
		return prior;
	}

	public void setPrior(Number prior) {
		this.prior = prior;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(ZonedDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(ZonedDateTime finishDate) {
		this.finishDate = finishDate;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public ZonedDateTime getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(ZonedDateTime closeDate) {
		this.closeDate = closeDate;
	}

	public String getCloseCode() {
		return closeCode;
	}

	public void setCloseCode(String closeCode) {
		this.closeCode = closeCode;
	}

	public Long getIdTypeWork() {
		return idTypeWork;
	}

	public void setIdTypeWork(Long idTypeWork) {
		this.idTypeWork = idTypeWork;
	}

	public Long getIdBrigade() {
		return idBrigade;
	}

	public void setIdBrigade(Long idBrigade) {
		this.idBrigade = idBrigade;
	}

	public Long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}

	public BrigadeDto getBrigade() {
		return brigade;
	}

	public void setBrigade(BrigadeDto brigade) {
		this.brigade = brigade;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getTrafficOff() {
		return trafficOff;
	}

	public void setTrafficOff(Boolean trafficOff) {
		this.trafficOff = trafficOff;
	}

	public Boolean getConsumerOff() {
		return consumerOff;
	}

	public void setConsumerOff(Boolean consumerOff) {
		this.consumerOff = consumerOff;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStausToir() {
		return stausToir;
	}

	public void setStausToir(String stausToir) {
		this.stausToir = stausToir;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUpdatedAddress() {
		return updatedAddress;
	}

	public void setUpdatedAddress(String updatedAddress) {
		this.updatedAddress = updatedAddress;
	}

	@Override
	public String toString() {
		return number;
	}
}
