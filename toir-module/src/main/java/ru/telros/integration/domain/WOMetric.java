package ru.telros.integration.domain;

public class WOMetric {
	public String XXBR_WIP_ENTITY_ID;
	public String METRICS_CODE;
	public String M_QUANTITY;

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WOMetric{");
		sb.append("XXBR_WIP_ENTITY_ID='").append(XXBR_WIP_ENTITY_ID).append('\'');
		sb.append(", METRICS_CODE='").append(METRICS_CODE).append('\'');
		sb.append(", M_QUANTITY='").append(M_QUANTITY).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
