package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SOURCES")
@SequenceGenerator(name = "idGenerator", sequenceName = "SOURCES_SEQ", allocationSize = 1)
public class Source extends AbstractEntity {
	@Column(name = "FLEX_VALUE_ID")
	public String FLEX_VALUE_ID;

	@Column(name = "FLEX_VALUE")
	public String FLEX_VALUE;

	@Column(name = "DESCRIPTION")
	public String DESCRIPTION;

	@Column(name = "KIND_ACTIVITY")
	public String KIND_ACTIVITY;

	@Column(name = "FLG_ACTUAL")
	public String FLG_ACTUAL;

	@Column(name = "FLG_FOR_MANUAL_INPUT")
	public String FLG_FOR_MANUAL_INPUT;

	@Column(name = "ENABLED_FLAG")
	public String ENABLED_FLAG;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Source source = (Source) o;

		return FLEX_VALUE_ID != null ? FLEX_VALUE_ID.equals(source.FLEX_VALUE_ID) : source.FLEX_VALUE_ID == null;
	}

	@Override
	public int hashCode() {
		return FLEX_VALUE_ID != null ? FLEX_VALUE_ID.hashCode() : 0;
	}
}
