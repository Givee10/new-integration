package ru.telros.integration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.telros.integration.domain.*;
import ru.telros.integration.repository.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class WorkService {
	private final AbsenceReasonRepository absenceReasonRepository;
	private final AddressRepository addressRepository;
	private final BrigadeRepository brigadeRepository;
	private final BrigadeStructureRepository brigadeStructureRepository;
	private final DeviationReasonRepository deviationReasonRepository;
	private final EmployeeRepository employeeRepository;
	private final MetricRepository metricRepository;
	private final MessageImportPlanRepository importPlanRepository;
	private final MessageImportWorkRepository importWorkRepository;
	private final NzEmployeeRepository nzEmployeeRepository;
	private final NzHeaderRepository nzHeaderRepository;
	private final NzSmallTechnicRepository nzSmallTechnicRepository;
	private final NzTechnicRepository nzTechnicRepository;
	private final ResourceRepository resourceRepository;
	private final SmallSpecTechnicRepository smallSpecTechnicRepository;
	private final SmallTechnicRepository smallTechnicRepository;
	private final SourceRepository sourceRepository;
	private final SpecTechnicRepository specTechnicRepository;
	private final StandartRepository standartRepository;
	private final StreetRepository streetRepository;
	private final TechnicTypeRepository technicTypeRepository;
	private final TypeElementRepository typeElementRepository;
	private final TypeWorkElementRepository typeWorkElementRepository;
	private final TypeWorkRepository typeWorkRepository;
	private final WorkDetNormRepository workDetNormRepository;
	private final WorkEORepository workEORepository;
	private final WorkHeaderRepository workHeaderRepository;
	private final WorkMapRepository workMapRepository;
	private final WorkMetricRepository workMetricRepository;
	private final WorkNormRepository workNormRepository;
	private final ZoneRepository zoneRepository;

	@Autowired
	public WorkService(AbsenceReasonRepository absenceReasonRepository, AddressRepository addressRepository, BrigadeRepository brigadeRepository,
					   BrigadeStructureRepository brigadeStructureRepository, DeviationReasonRepository deviationReasonRepository,
					   EmployeeRepository employeeRepository, MetricRepository metricRepository, MessageImportPlanRepository importPlanRepository,
					   MessageImportWorkRepository importWorkRepository, NzEmployeeRepository nzEmployeeRepository, NzHeaderRepository nzHeaderRepository,
					   NzSmallTechnicRepository nzSmallTechnicRepository, NzTechnicRepository nzTechnicRepository,
					   ResourceRepository resourceRepository, SmallSpecTechnicRepository smallSpecTechnicRepository,
					   SmallTechnicRepository smallTechnicRepository, SourceRepository sourceRepository,
					   SpecTechnicRepository specTechnicRepository, StandartRepository standartRepository,
					   StreetRepository streetRepository, TechnicTypeRepository technicTypeRepository,
					   TypeElementRepository typeElementRepository, TypeWorkElementRepository typeWorkElementRepository,
					   TypeWorkRepository typeWorkRepository, WorkDetNormRepository workDetNormRepository,
					   WorkEORepository workEORepository, WorkHeaderRepository workHeaderRepository,
					   WorkMapRepository workMapRepository, WorkMetricRepository workMetricRepository,
					   WorkNormRepository workNormRepository, ZoneRepository zoneRepository) {
		this.absenceReasonRepository = absenceReasonRepository;
		this.addressRepository = addressRepository;
		this.brigadeRepository = brigadeRepository;
		this.brigadeStructureRepository = brigadeStructureRepository;
		this.deviationReasonRepository = deviationReasonRepository;
		this.employeeRepository = employeeRepository;
		this.metricRepository = metricRepository;
		this.importPlanRepository = importPlanRepository;
		this.importWorkRepository = importWorkRepository;
		this.nzEmployeeRepository = nzEmployeeRepository;
		this.nzHeaderRepository = nzHeaderRepository;
		this.nzSmallTechnicRepository = nzSmallTechnicRepository;
		this.nzTechnicRepository = nzTechnicRepository;
		this.resourceRepository = resourceRepository;
		this.smallSpecTechnicRepository = smallSpecTechnicRepository;
		this.smallTechnicRepository = smallTechnicRepository;
		this.sourceRepository = sourceRepository;
		this.specTechnicRepository = specTechnicRepository;
		this.standartRepository = standartRepository;
		this.streetRepository = streetRepository;
		this.technicTypeRepository = technicTypeRepository;
		this.typeElementRepository = typeElementRepository;
		this.typeWorkElementRepository = typeWorkElementRepository;
		this.typeWorkRepository = typeWorkRepository;
		this.workDetNormRepository = workDetNormRepository;
		this.workEORepository = workEORepository;
		this.workHeaderRepository = workHeaderRepository;
		this.workMapRepository = workMapRepository;
		this.workMetricRepository = workMetricRepository;
		this.workNormRepository = workNormRepository;
		this.zoneRepository = zoneRepository;
	}

	private <T extends AbstractEntity> T updateEntity(T oldEntity, T newEntity) {
		Date date = new Date();
		if (oldEntity != null) {
			newEntity.ID = oldEntity.ID;
			newEntity.CREATE_DATE = oldEntity.CREATE_DATE;
			newEntity.CHANGE_DATE = date;
		} else {
			newEntity.CREATE_DATE = date;
			newEntity.CHANGE_DATE = date;
		}
		newEntity.IS_ACTIVE = true;
		return newEntity;
	}

	public AbsenceReason saveEntity(AbsenceReason entity) {
		AbsenceReason oldEntity = absenceReasonRepository.findByParameters(entity.FLEX_VALUE_ID);
		return absenceReasonRepository.save(updateEntity(oldEntity, entity));
	}

	public Brigade saveEntity(Brigade entity) {
		Brigade oldEntity = brigadeRepository.findByParameters(entity.DEPARTMENT_ID);
		return brigadeRepository.save(updateEntity(oldEntity, entity));
	}

	public BrigadeStructure saveEntity(BrigadeStructure entity) {
		BrigadeStructure oldEntity = brigadeStructureRepository.findByParameters(entity.DEPARTMENT_ID, entity.PERSON_ID, entity.RESOURCE_CODE);
		return brigadeStructureRepository.save(updateEntity(oldEntity, entity));
	}

	public DeviationReason saveEntity(DeviationReason entity) {
		DeviationReason oldEntity = deviationReasonRepository.findByParameters(entity.FLEX_VALUE_ID);
		return deviationReasonRepository.save(updateEntity(oldEntity, entity));
	}

	public Employee saveEntity(Employee entity) {
		Employee oldEntity = employeeRepository.findByParameters(entity.PERSON_ID);
		return employeeRepository.save(updateEntity(oldEntity, entity));
	}

	public Metric saveEntity(Metric entity) {
		Metric oldEntity = metricRepository.findByParameters(entity.INVENTORY_ITEM_ID, entity.METRICS_CODE);
		return metricRepository.save(updateEntity(oldEntity, entity));
	}

	public Resource saveEntity(Resource entity) {
		Resource oldEntity = resourceRepository.findByParameters(entity.RESOURCE_ID);
		return resourceRepository.save(updateEntity(oldEntity, entity));
	}

	public SmallSpecTechnic saveEntity(SmallSpecTechnic entity) {
		SmallSpecTechnic oldEntity = smallSpecTechnicRepository.findByParameters(entity.SMM_ID);
		return smallSpecTechnicRepository.save(updateEntity(oldEntity, entity));
	}

	public SmallTechnic saveEntity(SmallTechnic entity) {
		SmallTechnic oldEntity = smallTechnicRepository.findByParameters(entity.CODE_ID);
		return smallTechnicRepository.save(updateEntity(oldEntity, entity));
	}

	public Source saveEntity(Source entity) {
		Source oldEntity = sourceRepository.findByParameters(entity.FLEX_VALUE_ID);
		return sourceRepository.save(updateEntity(oldEntity, entity));
	}

	public SpecTechnic saveEntity(SpecTechnic entity) {
		SpecTechnic oldEntity = specTechnicRepository.findByParameters(entity.CODE_ID);
		return specTechnicRepository.save(updateEntity(oldEntity, entity));
	}

	public Standart saveEntity(Standart entity) {
		Standart oldEntity = standartRepository.findByParameters(entity.OPERATION_SEQUENCE_ID);
		return standartRepository.save(updateEntity(oldEntity, entity));
	}

	public Street saveEntity(Street entity) {
		Street oldEntity = streetRepository.findByParameters(entity.PRF_ID);
		return streetRepository.save(updateEntity(oldEntity, entity));
	}

	public TechnicType saveEntity(TechnicType entity) {
		TechnicType oldEntity = technicTypeRepository.findByParameters(entity.TYPE_ID);
		return technicTypeRepository.save(updateEntity(oldEntity, entity));
	}

	public TypeElement saveEntity(TypeElement entity) {
		TypeElement oldEntity = typeElementRepository.findByParameters(entity.EO_TYPE);
		return typeElementRepository.save(updateEntity(oldEntity, entity));
	}

	public TypeWork saveEntity(TypeWork entity) {
		TypeWork oldEntity = typeWorkRepository.findByParameters(entity.ACTIVITY_ASSOCIATION_ID);
		return typeWorkRepository.save(updateEntity(oldEntity, entity));
	}

	public TypeWorkElement saveEntity(TypeWorkElement entity) {
		TypeWorkElement oldEntity = typeWorkElementRepository.findByParameters(entity.EO_TYPE, entity.METRICS_CODE, entity.OPER_CODE);
		return typeWorkElementRepository.save(updateEntity(oldEntity, entity));
	}

	public Zone saveEntity(Zone entity) {
		Zone oldEntity = zoneRepository.findByParameters(entity.ORGANIZATION_ID);
		return zoneRepository.save(updateEntity(oldEntity, entity));
	}

	public NzEmployee saveEntity(NzEmployee entity) {
		NzEmployee oldEntity = nzEmployeeRepository.findByParameters(entity.ORDER_SET_ID, entity.PERSON_ID);
		return nzEmployeeRepository.save(updateEntity(oldEntity, entity));
	}

	public NzHeader saveEntity(NzHeader entity) {
		NzHeader oldEntity = nzHeaderRepository.findByParameters(entity.ORDER_SET_ID);
		return nzHeaderRepository.save(updateEntity(oldEntity, entity));
	}

	public NzSmallTechnic saveEntity(NzSmallTechnic entity) {
		NzSmallTechnic oldEntity = nzSmallTechnicRepository.findByParameters(entity.ORDER_SET_ID, entity.SMM_ID);
		return nzSmallTechnicRepository.save(updateEntity(oldEntity, entity));
	}

	public NzTechnic saveEntity(NzTechnic entity) {
		NzTechnic oldEntity = nzTechnicRepository.findByParameters(entity.CODE_ID, entity.ORDER_SET_ID);
		return nzTechnicRepository.save(updateEntity(oldEntity, entity));
	}

	public WorkDetNorm saveEntity(WorkDetNorm entity) {
		WorkDetNorm oldEntity = workDetNormRepository.findByParameters(entity.ACTION_SEQ_ID, entity.WIP_ENTITY_ID);
		return workDetNormRepository.save(updateEntity(oldEntity, entity));
	}

	public WorkEO saveEntity(WorkEO entity) {
		WorkEO oldEntity = workEORepository.findByParameters(entity.EO_TYPE, entity.WIP_ENTITY_ID);
		return workEORepository.save(updateEntity(oldEntity, entity));
	}

	public WorkHeader saveEntity(WorkHeader entity) {
		WorkHeader oldEntity = workHeaderRepository.findByParameters(entity.WIP_ENTITY_ID);
		return workHeaderRepository.save(updateEntity(oldEntity, entity));
	}

	public WorkMap saveEntity(WorkMap entity) {
		WorkMap oldEntity = workMapRepository.findByParameters(entity.WIP_ENTITY_ID, entity.OPERATION_SEQ_NUM);
		return workMapRepository.save(updateEntity(oldEntity, entity));
	}

	public WorkMetric saveEntity(WorkMetric entity) {
		WorkMetric oldEntity = workMetricRepository.findByParameters(entity.METRICS_CODE, entity.WIP_ENTITY_ID);
		return workMetricRepository.save(updateEntity(oldEntity, entity));
	}

	public WorkNorm saveEntity(WorkNorm entity) {
		WorkNorm oldEntity = workNormRepository.findByParameters(entity.RESOURCE_CODE, entity.WIP_ENTITY_ID);
		return workNormRepository.save(updateEntity(oldEntity, entity));
	}

	public MessageImportPlan saveEntity(MessageImportPlan entity) {
		MessageImportPlan oldEntity = importPlanRepository.findByParameters(entity.ORDER_SET_ID);
		if (oldEntity != null) entity.ID = oldEntity.ID;
		return importPlanRepository.save(entity);
	}

	public MessageImportWork saveEntity(MessageImportWork entity) {
		MessageImportWork oldEntity = importWorkRepository.findByParameters(entity.WIP_ENTITY_ID);
		if (oldEntity != null) entity.ID = oldEntity.ID;
		return importWorkRepository.save(entity);
	}

	public MessageImportPlan findImport(Long orderSetId) {
		return importPlanRepository.findByParameters(orderSetId);
	}

	public MessageImportPlan findImportFromPlan(Long brigade_plan_id) {
		return importPlanRepository.findByPlan(brigade_plan_id);
	}

	public List<MessageImportPlan> findImportPlans() {
		return importPlanRepository.findAll();
	}

	public MessageImportWork findImportWork(Long wip_entity_id) {
		return importWorkRepository.findByParameters(wip_entity_id);
	}

	public MessageImportWork findImportWorkFromId(Long work_order_id) {
		return importWorkRepository.findByWork(work_order_id);
	}

	public AbsenceReason findAbsenceReasonByName(String name) {
		return absenceReasonRepository.findByName(name);
	}

	public Brigade findBrigadeByDepID(String department_id) {
		return brigadeRepository.findByParameters(department_id);
	}

	public Brigade findBrigadeByName(String name) {
		return brigadeRepository.findByName(name);
	}

	public Employee findEmployeeByCode(String person_id) {
		return employeeRepository.findByParameters(person_id);
	}

	public TechnicType findTechnicTypeByTypeId(String type_id) {
		return technicTypeRepository.findByParameters(type_id);
	}

	public SpecTechnic findTechnicByCode(String code_id) {
		return specTechnicRepository.findByParameters(code_id);
	}

	public SmallTechnic findSmallTechnicByCode(String code_id) {
		return smallTechnicRepository.findByParameters(code_id);
	}

	public SmallSpecTechnic findSmallSpecTechnicByCode(String smm_id) {
		return smallSpecTechnicRepository.findByParameters(smm_id);
	}

	public SpecTechnic findTechnicByTechCode(String tech_code) {
		return specTechnicRepository.findByTechCode(tech_code);
	}

	public SmallSpecTechnic findSmallSpecTechnicByNumberAndCode(String inv_number, String tech_code) {
		return smallSpecTechnicRepository.findByNumberAndCode(inv_number, tech_code);
	}

	public Standart findStandartByName(String operation_description, String code, String oper) {
		return standartRepository.findByName(operation_description, code, oper);
	}

	public TypeElement findTypeElementByParams(String EO_TYPE_DESCR, String PRIMARY_UOM) {
		return typeElementRepository.findByDescAndMetric(EO_TYPE_DESCR, PRIMARY_UOM);
	}

	public TypeWork findTypeWorkByParams(String asset_activity_id, String organization_id) {
		return typeWorkRepository.findByNumberAndOrganization(asset_activity_id, organization_id);
	}

	public List<WorkHeader> findNzWorkHeaders(String ORDER_SET_ID) {
		return workHeaderRepository.findByOrderSetId(ORDER_SET_ID);
	}

	public List<NzHeader> findAllNzHeaders() {
		return nzHeaderRepository.findAll();
	}

	public List<NzEmployee> findEmployeesByOrderSetId(String orderSetId) {
		return nzEmployeeRepository.findByOrderSetId(orderSetId);
	}

	public List<NzTechnic> findEquipmentByOrderSetId(String orderSetId) {
		return nzTechnicRepository.findByOrderSetId(orderSetId);
	}

	public List<NzSmallTechnic> findMechanizationByOrderSetId(String orderSetId) {
		return nzSmallTechnicRepository.findByOrderSetId(orderSetId);
	}

	public List<EODto> findEODtoList(String oper_code) {
		List<TypeElement> elementList = typeElementRepository.findTypeElementList(oper_code);
		return createEODtoList(elementList);
	}

	private List<EODto> createEODtoList(List<TypeElement> elementList) {
		List<EODto> result = new ArrayList<>();
		elementList.forEach(element -> {
			EODto eoDto = new EODto();
			eoDto.setId(element.ID);
			eoDto.setEoType(element.EO_TYPE);
			eoDto.setEoDescription(element.EO_TYPE_DESCR);
			result.add(eoDto);
		});
		return result;
	}

	public List<MetricDto> findMetricDtoList(String oper_code) {
		List<Metric> metricList = metricRepository.findMetricList(oper_code);
		return createMetricDtoList(metricList);
	}

	private List<MetricDto> createMetricDtoList(List<Metric> metricList) {
		List<MetricDto> result = new ArrayList<>();
		metricList.forEach(metric -> {
			MetricDto metricDto = new MetricDto();
			metricDto.setId(metric.ID);
			metricDto.setMetricsCode(metric.METRICS_CODE);
			result.add(metricDto);
		});
		return result;
	}

	public List<OperationDto> findOperationDtoList(String oper_code) {
		List<Standart> standartList = standartRepository.findStandartList(oper_code);
		return createOperationDtoList(standartList);
	}

	private List<OperationDto> createOperationDtoList(List<Standart> standartList) {
		List<OperationDto> result = new ArrayList<>();
		standartList.forEach(standart -> {
			OperationDto operationDto = new OperationDto();
			operationDto.setId(standart.ID);
			operationDto.setOperationCode(standart.OPER_CODE);
			operationDto.setOperationName(standart.OPERATION_DESCRIPTION);
			operationDto.setOperationSequenceId(standart.OPERATION_SEQUENCE_ID);
			result.add(operationDto);
		});
		return result;
	}

	public String findInventoryItemId(String oper_code) {
		TypeWork typeWork = typeWorkRepository.findByOperCode(oper_code);
		return typeWork.ASSET_ACTIVITY_ID;
	}

	public void deleteOldAbsenceReasons(List<AbsenceReason> newEntityList) {
		List<AbsenceReason> oldEntityList = absenceReasonRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (AbsenceReason entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			absenceReasonRepository.save(entity);
		}
	}

	public void deleteOldAddresses(List<Address> newEntityList) {
		List<Address> oldEntityList = addressRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Address entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			addressRepository.save(entity);
		}
	}

	public void deleteOldBrigades(List<Brigade> newEntityList) {
		List<Brigade> oldEntityList = brigadeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Brigade entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			brigadeRepository.save(entity);
		}
	}

	public void deleteOldBrigadeStructures(List<BrigadeStructure> newEntityList) {
		List<BrigadeStructure> oldEntityList = brigadeStructureRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (BrigadeStructure entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			brigadeStructureRepository.save(entity);
		}
	}

	public void deleteOldDeviationReasons(List<DeviationReason> newEntityList) {
		List<DeviationReason> oldEntityList = deviationReasonRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (DeviationReason entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			deviationReasonRepository.save(entity);
		}
	}

	public void deleteOldEmployees(List<Employee> newEntityList) {
		List<Employee> oldEntityList = employeeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Employee entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			employeeRepository.save(entity);
		}
	}

	public void deleteOldMetrics(List<Metric> newEntityList) {
		List<Metric> oldEntityList = metricRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Metric entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			metricRepository.save(entity);
		}
	}

	public void deleteOldNzEmployees(List<NzEmployee> newEntityList) {
		List<NzEmployee> oldEntityList = nzEmployeeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (NzEmployee entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			nzEmployeeRepository.save(entity);
		}
	}

	public void deleteOldNzHeaders(List<NzHeader> newEntityList) {
		List<NzHeader> oldEntityList = nzHeaderRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (NzHeader entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			nzHeaderRepository.save(entity);
		}
	}

	public void deleteOldNzSmallTechnics(List<NzSmallTechnic> newEntityList) {
		List<NzSmallTechnic> oldEntityList = nzSmallTechnicRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (NzSmallTechnic entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			nzSmallTechnicRepository.save(entity);
		}
	}

	public void deleteOldNzTechnics(List<NzTechnic> newEntityList) {
		List<NzTechnic> oldEntityList = nzTechnicRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (NzTechnic entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			nzTechnicRepository.save(entity);
		}
	}

	public void deleteOldResources(List<Resource> newEntityList) {
		List<Resource> oldEntityList = resourceRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Resource entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			resourceRepository.save(entity);
		}
	}

	public void deleteOldSmallSpecTechnics(List<SmallSpecTechnic> newEntityList) {
		List<SmallSpecTechnic> oldEntityList = smallSpecTechnicRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (SmallSpecTechnic entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			smallSpecTechnicRepository.save(entity);
		}
	}

	public void deleteOldSmallTechnics(List<SmallTechnic> newEntityList) {
		List<SmallTechnic> oldEntityList = smallTechnicRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (SmallTechnic entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			smallTechnicRepository.save(entity);
		}
	}

	public void deleteOldSources(List<Source> newEntityList) {
		List<Source> oldEntityList = sourceRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Source entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			sourceRepository.save(entity);
		}
	}
	public void deleteOldSpecTechnics(List<SpecTechnic> newEntityList) {
		List<SpecTechnic> oldEntityList = specTechnicRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (SpecTechnic entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			specTechnicRepository.save(entity);
		}
	}

	public void deleteOldStandarts(List<Standart> newEntityList) {
		List<Standart> oldEntityList = standartRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Standart entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			standartRepository.save(entity);
		}
	}

	public void deleteOldStreets(List<Street> newEntityList) {
		List<Street> oldEntityList = streetRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Street entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			streetRepository.save(entity);
		}
	}

	public void deleteOldTechnicTypes(List<TechnicType> newEntityList) {
		List<TechnicType> oldEntityList = technicTypeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (TechnicType entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			technicTypeRepository.save(entity);
		}
	}

	public void deleteOldTypeElements(List<TypeElement> newEntityList) {
		List<TypeElement> oldEntityList = typeElementRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (TypeElement entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			typeElementRepository.save(entity);
		}
	}

	public void deleteOldTypeWorkElements(List<TypeWorkElement> newEntityList) {
		List<TypeWorkElement> oldEntityList = typeWorkElementRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (TypeWorkElement entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			typeWorkElementRepository.save(entity);
		}
	}

	public void deleteOldTypeWorks(List<TypeWork> newEntityList) {
		List<TypeWork> oldEntityList = typeWorkRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (TypeWork entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			typeWorkRepository.save(entity);
		}
	}

	public void deleteOldWorkDetNorms(List<WorkDetNorm> newEntityList) {
		List<WorkDetNorm> oldEntityList = workDetNormRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (WorkDetNorm entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			workDetNormRepository.save(entity);
		}
	}

	public void deleteOldWorkEOs(List<WorkEO> newEntityList) {
		List<WorkEO> oldEntityList = workEORepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (WorkEO entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			workEORepository.save(entity);
		}
	}

	public void deleteOldWorkHeaders(List<WorkHeader> newEntityList) {
		List<WorkHeader> oldEntityList = workHeaderRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (WorkHeader entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			workHeaderRepository.save(entity);
		}
	}

	public void deleteOldWorkMaps(List<WorkMap> newEntityList) {
		List<WorkMap> oldEntityList = workMapRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (WorkMap entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			workMapRepository.save(entity);
		}
	}

	public void deleteOldWorkMetrics(List<WorkMetric> newEntityList) {
		List<WorkMetric> oldEntityList = workMetricRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (WorkMetric entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			workMetricRepository.save(entity);
		}
	}

	public void deleteOldWorkNorms(List<WorkNorm> newEntityList) {
		List<WorkNorm> oldEntityList = workNormRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (WorkNorm entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			workNormRepository.save(entity);
		}
	}

	public void deleteOldZones(List<Zone> newEntityList) {
		List<Zone> oldEntityList = zoneRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Zone entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			zoneRepository.save(entity);
		}
	}
}
