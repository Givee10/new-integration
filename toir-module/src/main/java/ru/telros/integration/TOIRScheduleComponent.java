package ru.telros.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.telros.integration.controller.CustomRestTemplate;
import ru.telros.integration.controller.OrderController;
import ru.telros.integration.domain.*;
import ru.telros.integration.elements.AbstractResponse;
import ru.telros.integration.elements.toirnz.ToirClient;
import ru.telros.integration.elements.toirsprav.ToirRefClient;
import ru.telros.integration.service.WorkService;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static ru.telros.integration.DatesUtil.NORMAL_DATE_FORMAT;
import static ru.telros.integration.controller.TOIRController.logResponse;

@Component
public class TOIRScheduleComponent {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOIRScheduleComponent.class);

	@Autowired
	private CustomRestTemplate template;
	@Autowired
	private OrderController controller;
	@Autowired
	private ToirClient toirClient;
	@Autowired
	private ToirRefClient toirRefClient;
	@Autowired
	private WorkService workService;
	@Autowired
	private ObjectMapper objectMapper;

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNzEmployees() {
		List<NzEmployee> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getNzEmployees();
			logResponse = logResponse(response);
			NzEmployee[] nzEmployees = objectMapper.readValue(logResponse, NzEmployee[].class);
			for (NzEmployee nzEmployee : nzEmployees) {
				NzEmployee savedEntity = workService.saveEntity(nzEmployee);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldNzEmployees(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNzHeaders() {
		List<NzHeader> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getNzHeaders();
			logResponse = logResponse(response);
			NzHeader[] nzHeaders = objectMapper.readValue(logResponse, NzHeader[].class);
			for (NzHeader nzHeader : nzHeaders) {
				NzHeader savedEntity = workService.saveEntity(nzHeader);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldNzHeaders(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNzSmallTechnics() {
		List<NzSmallTechnic> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getNzSmallTechnics();
			logResponse = logResponse(response);
			NzSmallTechnic[] nzSmallTechnics = objectMapper.readValue(logResponse, NzSmallTechnic[].class);
			for (NzSmallTechnic nzSmallTechnic : nzSmallTechnics) {
				NzSmallTechnic savedEntity = workService.saveEntity(nzSmallTechnic);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldNzSmallTechnics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNzTechnics() {
		List<NzTechnic> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getNzTechnics();
			logResponse = logResponse(response);
			NzTechnic[] nzTechnics = objectMapper.readValue(logResponse, NzTechnic[].class);
			for (NzTechnic nzTechnic : nzTechnics) {
				NzTechnic savedEntity = workService.saveEntity(nzTechnic);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldNzTechnics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkWorkDetNorms() {
		List<WorkDetNorm> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getWorkDetNorms();
			logResponse = logResponse(response);
			WorkDetNorm[] workDetNorms = objectMapper.readValue(logResponse, WorkDetNorm[].class);
			for (WorkDetNorm workDetNorm : workDetNorms) {
				WorkDetNorm savedEntity = workService.saveEntity(workDetNorm);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldWorkDetNorms(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkWorkEos() {
		List<WorkEO> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getWorkEos();
			logResponse = logResponse(response);
			WorkEO[] workEOS = objectMapper.readValue(logResponse, WorkEO[].class);
			for (WorkEO workEO : workEOS) {
				WorkEO savedEntity = workService.saveEntity(workEO);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldWorkEOs(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkWorkHeaders() {
		List<WorkHeader> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getWorkHeaders();
			logResponse = logResponse(response);
			WorkHeader[] workHeaders = objectMapper.readValue(logResponse, WorkHeader[].class);
			for (WorkHeader workHeader : workHeaders) {
				WorkHeader savedEntity = workService.saveEntity(workHeader);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldWorkHeaders(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkWorkMaps() {
		List<WorkMap> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getWorkMaps();
			logResponse = logResponse(response);
			WorkMap[] workMaps = objectMapper.readValue(logResponse, WorkMap[].class);
			for (WorkMap workMap : workMaps) {
				WorkMap savedEntity = workService.saveEntity(workMap);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldWorkMaps(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkWorkMetrics() {
		List<WorkMetric> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getWorkMetrics();
			logResponse = logResponse(response);
			WorkMetric[] workMetrics = objectMapper.readValue(logResponse, WorkMetric[].class);
			for (WorkMetric workMetric : workMetrics) {
				WorkMetric savedEntity = workService.saveEntity(workMetric);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldWorkMetrics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkWorkNorms() {
		List<WorkNorm> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirClient.getWorkNorms();
			logResponse = logResponse(response);
			WorkNorm[] workNorms = objectMapper.readValue(logResponse, WorkNorm[].class);
			for (WorkNorm workNorm : workNorms) {
				WorkNorm savedEntity = workService.saveEntity(workNorm);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldWorkNorms(newEntityList);
	}


	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkAbsenceReasons() {
		List<AbsenceReason> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getAbsenceReasons();
			logResponse = logResponse(response);
			AbsenceReason[] absenceReasons = objectMapper.readValue(logResponse, AbsenceReason[].class);
			for (AbsenceReason absenceReason : absenceReasons) {
				AbsenceReason savedEntity = workService.saveEntity(absenceReason);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldAbsenceReasons(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkBrigades() {
		List<Brigade> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getBrigades();
			logResponse = logResponse(response);
			Brigade[] brigades = objectMapper.readValue(logResponse, Brigade[].class);
			for (Brigade brigade : brigades) {
				Brigade savedEntity = workService.saveEntity(brigade);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldBrigades(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkStructureBrigades() {
		List<BrigadeStructure> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getStructureBrigades();
			logResponse = logResponse(response);
			BrigadeStructure[] brigadeStructures = objectMapper.readValue(logResponse, BrigadeStructure[].class);
			for (BrigadeStructure structure : brigadeStructures) {
				BrigadeStructure savedEntity = workService.saveEntity(structure);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldBrigadeStructures(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkDeviationReasons() {
		List<DeviationReason> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getDeviationReasons();
			logResponse = logResponse(response);
			DeviationReason[] deviationReasons = objectMapper.readValue(logResponse, DeviationReason[].class);
			for (DeviationReason deviationReason : deviationReasons) {
				DeviationReason savedEntity = workService.saveEntity(deviationReason);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldDeviationReasons(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkEmployees() {
		List<Employee> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getEmployees();
			logResponse = logResponse(response);
			Employee[] employees = objectMapper.readValue(logResponse, Employee[].class);
			for (Employee employee : employees) {
				Employee savedEntity = workService.saveEntity(employee);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldEmployees(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkMetrics() {
		List<Metric> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getMetrics();
			logResponse = logResponse(response);
			Metric[] metrics = objectMapper.readValue(logResponse, Metric[].class);
			for (Metric metric : metrics) {
				Metric savedEntity = workService.saveEntity(metric);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldMetrics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkResources() {
		List<Resource> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getResources();
			logResponse = logResponse(response);
			Resource[] resources = objectMapper.readValue(logResponse, Resource[].class);
			for (Resource resource : resources) {
				Resource savedEntity = workService.saveEntity(resource);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldResources(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkSmallSpecTechnicTypes() {
		List<SmallSpecTechnic> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getSmallSpecTechnicTypes();
			logResponse = logResponse(response);
			SmallSpecTechnic[] smallSpecTechnics = objectMapper.readValue(logResponse, SmallSpecTechnic[].class);
			for (SmallSpecTechnic smallSpecTechnic : smallSpecTechnics) {
				SmallSpecTechnic savedEntity = workService.saveEntity(smallSpecTechnic);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldSmallSpecTechnics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkSmallTechnicTypes() {
		List<SmallTechnic> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getSmallTechnicTypes();
			logResponse = logResponse(response);
			SmallTechnic[] smallTechnics = objectMapper.readValue(logResponse, SmallTechnic[].class);
			for (SmallTechnic smallTechnic : smallTechnics) {
				SmallTechnic savedEntity = workService.saveEntity(smallTechnic);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldSmallTechnics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkSources() {
		List<Source> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getSources();
			logResponse = logResponse(response);
			Source[] sources = objectMapper.readValue(logResponse, Source[].class);
			for (Source source : sources) {
				Source savedEntity = workService.saveEntity(source);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldSources(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkSpecTechnicTypes() {
		List<SpecTechnic> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getSpecTechnicTypes();
			logResponse = logResponse(response);
			SpecTechnic[] specTechnics = objectMapper.readValue(logResponse, SpecTechnic[].class);
			for (SpecTechnic specTechnic : specTechnics) {
				SpecTechnic savedEntity = workService.saveEntity(specTechnic);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldSpecTechnics(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkStandarts() {
		List<Standart> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getStandarts();
			logResponse = logResponse(response);
			Standart[] standarts = objectMapper.readValue(logResponse, Standart[].class);
			for (Standart standart : standarts) {
				Standart savedEntity = workService.saveEntity(standart);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldStandarts(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkStreets() {
		List<Street> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getStreets();
			logResponse = logResponse(response);
			Street[] streets = objectMapper.readValue(logResponse, Street[].class);
			for (Street street : streets) {
				Street savedEntity = workService.saveEntity(street);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldStreets(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkTechnicTypes() {
		List<TechnicType> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getTechnicTypes();
			logResponse = logResponse(response);
			TechnicType[] technicTypes = objectMapper.readValue(logResponse, TechnicType[].class);
			for (TechnicType technicType : technicTypes) {
				TechnicType savedEntity = workService.saveEntity(technicType);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldTechnicTypes(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkTypeElements() {
		List<TypeElement> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getTypeElements();
			logResponse = logResponse(response);
			TypeElement[] typeElements = objectMapper.readValue(logResponse, TypeElement[].class);
			for (TypeElement typeElement : typeElements) {
				TypeElement savedEntity = workService.saveEntity(typeElement);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldTypeElements(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkTypesWorks() {
		List<TypeWork> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getTypesWorks();
			logResponse = logResponse(response);
			TypeWork[] typeWorks = objectMapper.readValue(logResponse, TypeWork[].class);
			for (TypeWork typeWork : typeWorks) {
				TypeWork savedEntity = workService.saveEntity(typeWork);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldTypeWorks(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkTypeElementsWorks() {
		List<TypeWorkElement> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getTypeElementsWorks();
			logResponse = logResponse(response);
			TypeWorkElement[] typeWorkElements = objectMapper.readValue(logResponse, TypeWorkElement[].class);
			for (TypeWorkElement typeWorkElement : typeWorkElements) {
				TypeWorkElement savedEntity = workService.saveEntity(typeWorkElement);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldTypeWorkElements(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkZones() {
		List<Zone> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = toirRefClient.getZones();
			logResponse = logResponse(response);
			Zone[] zones = objectMapper.readValue(logResponse, Zone[].class);
			for (Zone zone : zones) {
				Zone savedEntity = workService.saveEntity(zone);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(zone);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		workService.deleteOldZones(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void importNzPlans() {
		LOGGER.info("Importing new brigade plans");
		List<BrigadeDto> brigadeList = template.getAllBrigades();
		List<NzHeader> nzHeaders = workService.findAllNzHeaders();
		for (NzHeader nzHeader : nzHeaders) {
			MessageImportPlan importPlan = workService.findImport(Long.valueOf(nzHeader.ORDER_SET_ID));
			if (importPlan == null) {
				BrigadeDto brigadeDto = findBrigadeByToirId(brigadeList, nzHeader.DEPARTMENT_ID);
				if (brigadeDto != null) {
					importNewPlan(brigadeDto, nzHeader);
				}
			}
		}
		LOGGER.info("Importing new brigade works");
		List<MessageImportPlan> importPlans = workService.findImportPlans();
		for (MessageImportPlan messageImportPlan : importPlans) {
			List<WorkHeader> nzWorkHeaders = workService.findNzWorkHeaders(messageImportPlan.ORDER_SET_ID.toString());
			for (WorkHeader workHeader : nzWorkHeaders) {
				MessageImportWork importWork = workService.findImportWork(Long.valueOf(workHeader.WIP_ENTITY_ID));
				if (importWork == null) {
					BrigadeDto brigadeDto = findBrigadeByToirId(brigadeList, workHeader.DEPARTMENT_ID);
					if (brigadeDto != null) {
						importNewWork(brigadeDto, workHeader, messageImportPlan.BRIGADE_PLAN_ID);
					}
				}
			}
		}
	}

	private void importNewPlan(BrigadeDto brigade, NzHeader nzHeader) {
		ZonedDateTime createDate = stringToDate(nzHeader.CREATION_DATE);
		ZonedDateTime parse = stringToDate(nzHeader.ORDER_SET_DATE);
		List<BrigadePlanDto> brigadePlans = template.getBrigadePlans(brigade.getId(), parse, DatesUtil.setHourInDate(parse, 24));
		if (brigadePlans.isEmpty()) {
			BrigadePlanDto brigadePlanDto = new BrigadePlanDto();
			if (isDayTime(createDate)) {
				brigadePlanDto.setBeginDate(getMorningTime(parse));
				brigadePlanDto.setEndDate(getEveningTime(parse));
			} else {
				brigadePlanDto.setBeginDate(getEveningTime(parse));
				brigadePlanDto.setEndDate(getMorningTime(parse).plusDays(1));
			}
			brigadePlanDto.setMavr(false);
			brigadePlanDto.setIdBrigade(brigade.getId());
			BrigadePlanDto savedBrigadePlan = template.saveBrigadePlan(brigadePlanDto);
			if (savedBrigadePlan != null) {
				LOGGER.debug(StringUtil.writeValueAsString(savedBrigadePlan));
				MessageImportPlan messageImportPlan = new MessageImportPlan();
				messageImportPlan.BRIGADE_PLAN_ID = savedBrigadePlan.getId();
				messageImportPlan.BRIGADE_ID = savedBrigadePlan.getIdBrigade();
				messageImportPlan.ORDER_SET_ID = Long.valueOf(nzHeader.ORDER_SET_ID);
				messageImportPlan.DEPARTMENT_ID = Long.valueOf(nzHeader.DEPARTMENT_ID);
				MessageImportPlan savedEntity = workService.saveEntity(messageImportPlan);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				List<NzEmployee> nzEmployees = workService.findEmployeesByOrderSetId(nzHeader.ORDER_SET_ID);
				List<EmployeeBrigadePlanDto> employeeList = new ArrayList<>();
				for (NzEmployee nzEmployee : nzEmployees) {
					Employee employee = workService.findEmployeeByCode(nzEmployee.PERSON_ID);
					Long personId = Long.valueOf(employee.PERSON_ID);
					EmployeeBrigadePlanDto employeeDto = new EmployeeBrigadePlanDto();
					employeeDto.setLastName(employee.LAST_NAME);
					employeeDto.setFirstName(employee.FIRST_NAME);
					employeeDto.setMiddleName(employee.MIDDLE_NAMES);
					employeeDto.setAbsenceReason(nzEmployee.ABSENCE_COMMENT);
					employeeDto.setIdBrigadePlan(savedBrigadePlan.getId());
					employeeDto.setBrigadier("1".equalsIgnoreCase(nzEmployee.IS_BRIGADIER));
					employeeDto.setIdEmployee(personId);
					employeeList.add(employeeDto);
				}
				template.savePlanEmployees(savedBrigadePlan, employeeList);
				List<NzTechnic> nzTechnics = workService.findEquipmentByOrderSetId(nzHeader.ORDER_SET_ID);
				List<EquipmentBrigadePlanDto> equipmentList = new ArrayList<>();
				for (NzTechnic nzTechnic : nzTechnics) {
					//SpecTechnic technic = workService.findTechnicByCode(nzTechnic.CODE_ID);
					//TechnicType technicType = workService.findTechnicTypeByTypeId(technic.TYPE_ID);
					EquipmentDto equipment = controller.getEquipmentById(nzTechnic.CODE_ID);
					EquipmentBrigadePlanDto equipmentDto = new EquipmentBrigadePlanDto();
					equipmentDto.setGuid(equipment.getGuid());
					equipmentDto.setType(equipment.getType());
					equipmentDto.setModel(equipment.getModel());
					equipmentDto.setNumber(equipment.getNumber());
					equipmentDto.setCode(equipment.getCode());
					equipmentDto.setDescription(equipment.getDescription());
					equipmentDto.setUnit(equipment.getUnit());
					equipmentDto.setIdBrigadePlan(savedBrigadePlan.getId());
					equipmentList.add(equipmentDto);
				}
				template.savePlanEquipments(savedBrigadePlan, equipmentList);
				List<NzSmallTechnic> nzSmallTechnics = workService.findMechanizationByOrderSetId(nzHeader.ORDER_SET_ID);
				List<MechanizationBrigadePlanDto> mechanizationList = new ArrayList<>();
				for (NzSmallTechnic nzSmallTechnic : nzSmallTechnics) {
					//SmallTechnic smallTechnic = workService.findSmallTechnicByCode(nzSmallTechnic.CODE_ID);
					//SmallSpecTechnic smallSpecTechnic = workService.findSmallSpecTechnicByCode(nzSmallTechnic.SMM_ID);
					MechanizationDto mechanization = controller.getMechanizationById(nzSmallTechnic.SMM_ID);
					MechanizationBrigadePlanDto mechanizationDto = new MechanizationBrigadePlanDto();
					mechanizationDto.setGuid(mechanization.getGuid());
					mechanizationDto.setType(mechanization.getType());
					mechanizationDto.setModel(mechanization.getModel());
					mechanizationDto.setNumber(mechanization.getInventoryNumber());
					mechanizationDto.setCode(mechanization.getCode());
					mechanizationDto.setDescription(mechanization.getDescription());
					mechanizationDto.setUnit(mechanization.getUnit());
					mechanizationDto.setIdBrigadePlan(savedBrigadePlan.getId());
					mechanizationList.add(mechanizationDto);
				}
				template.savePlanMechanization(savedBrigadePlan, mechanizationList);
			}
		} else {
			for (BrigadePlanDto brigadePlanDto : brigadePlans) {
				MessageImportPlan messageImportPlan = new MessageImportPlan();
				messageImportPlan.BRIGADE_PLAN_ID = brigadePlanDto.getId();
				messageImportPlan.BRIGADE_ID = brigadePlanDto.getIdBrigade();
				messageImportPlan.ORDER_SET_ID = Long.valueOf(nzHeader.ORDER_SET_ID);
				messageImportPlan.DEPARTMENT_ID = Long.valueOf(nzHeader.DEPARTMENT_ID);
				MessageImportPlan savedEntity = workService.saveEntity(messageImportPlan);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
			}
		}
	}

	private void importNewWork(BrigadeDto brigade, WorkHeader workHeader, Long brigadePlanId) {
		TypeWork typeWork = workService.findTypeWorkByParams(workHeader.INVENTORY_ITEM_ID, workHeader.ORGANIZATION_ID);
		if (typeWork != null) {
			TypeWorkDto typeWorkDto = controller.getTypeWorkByCode(typeWork.ACTIVITY);
			if (typeWorkDto != null) {
				Double norm = typeWorkDto.getNorm();
				ZonedDateTime parse = stringToDate(workHeader.SCHEDULED_START_DATE);
				ZonedDateTime startDatePlan = parse.plusHours(9);
				ZonedDateTime finishDatePlan = startDatePlan.plusMinutes((long) (60 * norm));

				WorkDto workDto = new WorkDto();
				workDto.setStatus("Назначена");
				workDto.setUrgency("Обычная");
				workDto.setOrganization("РВ Юго-Западный");
				workDto.setRegistrationDate(ZonedDateTime.now());
				workDto.setDescription(typeWork.ACTIVITY_DESCRIPTION);
				workDto.setAddress(workHeader.WA_ADDRESS);
				workDto.setUpdatedAddress(workHeader.ADDR_COMMENT);
				//workDto.setSource(workHeader.DOC_SOURCE);
				workDto.setSource("Иное");
				workDto.setBrigade(brigade);
				workDto.setIdBrigade(brigade.getId());
				workDto.setIdPlan(brigadePlanId);
				workDto.setIdTypeWork(typeWorkDto.getId());
				workDto.setStartDatePlan(startDatePlan);
				workDto.setFinishDatePlan(finishDatePlan);
				WorkDto savedWork = template.saveWork(workDto);
				if (savedWork != null) {
					MessageImportWork messageImportWork = new MessageImportWork();
					messageImportWork.WIP_ENTITY_ID = Long.valueOf(workHeader.WIP_ENTITY_ID);
					messageImportWork.DEPARTMENT_ID = Long.valueOf(workHeader.DEPARTMENT_ID);
					messageImportWork.WORK_ORDER_ID = savedWork.getId();
					messageImportWork.BRIGADE_ID = brigade.getId();
					//messageImportWork.BRIGADE_ID = savedWork.getIdBrigade();
					MessageImportWork savedEntity = workService.saveEntity(messageImportWork);
					LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
					List<TypeWorkItemDto> typeWorkItems = template.getTypeWorkItems(typeWorkDto.getId());
					//typeWorkItems.sort(Comparator.comparing(TypeWorkItemDto::getId));
					List<WorkItemDto> workItems = createWorkItemStages(typeWorkItems, startDatePlan);
					template.saveWorkItems(savedWork.getId(), workItems);
				}
			}
		}
	}

	private ZonedDateTime stringToDate(String dateToConvert) {
		if (dateToConvert == null) return null;
		//DateTimeFormatter df = new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("dd-MMM-yy").toFormatter(Locale.ENGLISH);
		LocalDateTime parse = LocalDateTime.parse(dateToConvert, DateTimeFormatter.ofPattern(NORMAL_DATE_FORMAT));
		return ZonedDateTime.of(parse, ZoneId.systemDefault());
	}

	private boolean isDayTime(ZonedDateTime dateToConvert) {
		ZonedDateTime start = getMorningTime(dateToConvert);
		ZonedDateTime finish = getEveningTime(dateToConvert);
		return dateToConvert.isAfter(start) && dateToConvert.isBefore(finish);
	}

	private ZonedDateTime getMorningTime(ZonedDateTime dateToConvert) {
		return dateToConvert.withHour(8).withMinute(30).withSecond(0).withNano(0);
	}

	private ZonedDateTime getEveningTime(ZonedDateTime dateToConvert) {
		return dateToConvert.withHour(20).withMinute(30).withSecond(0).withNano(0);
	}

	private BrigadeDto findBrigadeByToirId(List<BrigadeDto> brigades, String DEPARTMENT_ID) {
		Brigade toirBrigade = workService.findBrigadeByDepID(DEPARTMENT_ID);
		if (toirBrigade != null) {
			return findBrigade(brigades, toirBrigade.DESCRIPTION);
		}
		return null;
	}

	private BrigadeDto findBrigade(List<BrigadeDto> brigades, String name) {
		return brigades.stream().filter(brigadeDto -> brigadeDto.getName().equals(name)).findFirst().orElse(null);
	}

	public static List<WorkItemDto> createWorkItemStages(List<TypeWorkItemDto> stages, ZonedDateTime startDatePlan) {
		List<WorkItemDto> result = new ArrayList<>();
		for (TypeWorkItemDto item : stages) {
			// инициализация данных этапа
			WorkItemDto stage = new WorkItemDto();
			stage.setCode(item.getCode());
			stage.setDescription(item.getDescription()); // название
			stage.setStartDatePlan(startDatePlan);
			startDatePlan = startDatePlan.plusMinutes((long) (60 * item.getNorm()));
			stage.setFinishDatePlan(startDatePlan);
			stage.setCallRequire(item.getCallRequire());
			stage.setActRequire(item.getActRequire());
			stage.setPhotoRequire(item.getPhotoRequire());
			stage.setDuration(item.getNorm());
			result.add(stage);
		}
		return result;
	}
}
