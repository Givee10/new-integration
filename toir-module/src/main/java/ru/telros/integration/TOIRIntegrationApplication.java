package ru.telros.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TOIRIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(TOIRIntegrationApplication.class, args);
	}

}
