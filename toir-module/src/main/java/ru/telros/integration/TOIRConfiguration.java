package ru.telros.integration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import ru.telros.integration.elements.toircalc.ToirCalcClient;
import ru.telros.integration.elements.toirnz.ToirClient;
import ru.telros.integration.elements.toirsprav.ToirRefClient;

@Configuration
public class TOIRConfiguration {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in pom.xml
        marshaller.setContextPath("ru.telros.integration.elements");
		return marshaller;
	}

    @Bean
    public ToirRefClient workRefClient(Jaxb2Marshaller marshaller) {
        ToirRefClient client = new ToirRefClient();
        client.setDefaultUri("http://172.16.152.134/xxbr/toirsprav.php?wsdl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public ToirClient workMainClient(Jaxb2Marshaller marshaller) {
        ToirClient client = new ToirClient();
        client.setDefaultUri("http://172.16.152.134/xxbr/toirnz.php?wsdl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

	@Bean
	public ToirCalcClient workCalcClient(Jaxb2Marshaller marshaller) {
		ToirCalcClient client = new ToirCalcClient();
		client.setDefaultUri("http://172.16.152.134/xxbr/toircalc.php?wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public TaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.setPoolSize(10);
		return threadPoolTaskScheduler;
	}

	@Bean
	public ObjectMapper buildObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		// for Jackson version 2.X
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//To parse LocalDateTime
		objectMapper.registerModule(new JavaTimeModule());
		return objectMapper;
	}
}
