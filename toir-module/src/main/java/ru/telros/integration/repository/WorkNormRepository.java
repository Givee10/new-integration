package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.WorkNorm;

import java.util.List;

@Repository
public interface WorkNormRepository extends JpaRepository<WorkNorm, Long> {
	@Query(value = "SELECT o FROM WorkNorm o WHERE (o.RESOURCE_CODE = ?1) AND (o.WIP_ENTITY_ID = ?2)")
	WorkNorm findByParameters(String resource_code, String wip_entity_id);

	@Query(value = "SELECT * FROM WORK_NORM WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<WorkNorm> findActive();
}
