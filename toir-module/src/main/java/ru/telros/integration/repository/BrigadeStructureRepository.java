package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.BrigadeStructure;

import java.util.List;

@Repository
public interface BrigadeStructureRepository extends JpaRepository<BrigadeStructure, Long> {
	@Query(value = "SELECT o FROM BrigadeStructure o WHERE (o.DEPARTMENT_ID = ?1) AND (o.PERSON_ID = ?2) AND (o.RESOURCE_CODE = ?3)")
	BrigadeStructure findByParameters(String department_id, String person_id, String resource_code);

	@Query(value = "SELECT * FROM BRIGADE_STRUCTURE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<BrigadeStructure> findActive();
}
