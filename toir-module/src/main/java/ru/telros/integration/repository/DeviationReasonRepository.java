package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.DeviationReason;

import java.util.List;

@Repository
public interface DeviationReasonRepository extends JpaRepository<DeviationReason, Long> {
	@Query(value = "SELECT o FROM DeviationReason o WHERE (o.FLEX_VALUE_ID = ?1)")
	DeviationReason findByParameters(String flex_value_id);

	@Query(value = "SELECT * FROM DEVIATION_REASON WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<DeviationReason> findActive();
}
