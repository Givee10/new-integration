package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Resource;

import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long> {
	@Query(value = "SELECT o FROM Resource o WHERE (o.RESOURCE_ID = ?1)")
	Resource findByParameters(String resource_id);

	@Query(value = "SELECT * FROM RESOURCES WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Resource> findActive();
}
