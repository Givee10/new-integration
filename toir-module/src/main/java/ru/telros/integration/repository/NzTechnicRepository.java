package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.NzTechnic;

import java.util.List;

@Repository
public interface NzTechnicRepository extends JpaRepository<NzTechnic, Long> {
	@Query(value = "SELECT o FROM NzTechnic o WHERE (o.CODE_ID = ?1) AND (o.ORDER_SET_ID = ?2)")
	NzTechnic findByParameters(String code_id, String order_set_id);

	@Query(value = "SELECT o FROM NzTechnic o WHERE (o.ORDER_SET_ID = ?1)")
	List<NzTechnic> findByOrderSetId(String order_set_id);

	@Query(value = "SELECT * FROM NZ_TECHNIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<NzTechnic> findActive();
}
