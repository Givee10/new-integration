package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.NzHeader;

import java.util.List;

@Repository
public interface NzHeaderRepository extends JpaRepository<NzHeader, Long> {
	@Query(value = "SELECT o FROM NzHeader o WHERE (o.ORDER_SET_ID = ?1)")
	NzHeader findByParameters(String order_set_id);

	@Query(value = "SELECT * FROM NZ_HEADER WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<NzHeader> findActive();
}
