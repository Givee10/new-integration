package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.TypeWorkElement;

import java.util.List;

@Repository
public interface TypeWorkElementRepository extends JpaRepository<TypeWorkElement, Long> {
	@Query(value = "SELECT o FROM TypeWorkElement o WHERE (o.EO_TYPE = ?1) AND (o.METRICS_CODE = ?2) AND (o.OPER_CODE = ?3)")
	TypeWorkElement findByParameters(String eo_type, String metrics_code, String oper_code);

	@Query(value = "SELECT * FROM TYPE_WORK_ELEMENT WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<TypeWorkElement> findActive();
}
