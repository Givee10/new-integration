package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.NzSmallTechnic;

import java.util.List;

@Repository
public interface NzSmallTechnicRepository extends JpaRepository<NzSmallTechnic, Long> {
	@Query(value = "SELECT o FROM NzSmallTechnic o WHERE (o.ORDER_SET_ID = ?1) AND (o.SMM_ID = ?2)")
	NzSmallTechnic findByParameters(String order_set_id, String smm_id);

	@Query(value = "SELECT o FROM NzSmallTechnic o WHERE (o.ORDER_SET_ID = ?1)")
	List<NzSmallTechnic> findByOrderSetId(String order_set_id);

	@Query(value = "SELECT * FROM NZ_SMALL_TECHNIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<NzSmallTechnic> findActive();
}
