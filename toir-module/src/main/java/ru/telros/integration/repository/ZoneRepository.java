package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Zone;

import java.util.List;

@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {
	@Query(value = "SELECT o FROM Zone o WHERE (o.ORGANIZATION_ID = ?1)")
	Zone findByParameters(String organization_id);

	@Query(value = "SELECT * FROM ZONE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Zone> findActive();
}
