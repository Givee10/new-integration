package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.SmallTechnic;

import java.util.List;

@Repository
public interface SmallTechnicRepository extends JpaRepository<SmallTechnic, Long> {
	@Query(value = "SELECT o FROM SmallTechnic o WHERE (o.CODE_ID = ?1)")
	SmallTechnic findByParameters(String code_id);

	@Query(value = "SELECT * FROM SMALL_TECHNIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<SmallTechnic> findActive();
}
