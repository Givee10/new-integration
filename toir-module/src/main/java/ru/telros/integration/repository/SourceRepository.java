package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Source;

import java.util.List;

@Repository
public interface SourceRepository extends JpaRepository<Source, Long> {
	@Query(value = "SELECT o FROM Source o WHERE (o.FLEX_VALUE_ID = ?1)")
	Source findByParameters(String flex_value_id);

	@Query(value = "SELECT * FROM SOURCES WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Source> findActive();
}
