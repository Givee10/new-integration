package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.WorkHeader;

import java.util.List;

@Repository
public interface WorkHeaderRepository extends JpaRepository<WorkHeader, Long> {
	@Query(value = "SELECT o FROM WorkHeader o WHERE (o.WIP_ENTITY_ID = ?1)")
	WorkHeader findByParameters(String wip_entity_id);

	@Query(value = "SELECT * FROM WORK_HEADER WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<WorkHeader> findActive();

	@Query(value = "SELECT * FROM WORK_HEADER WHERE ORDER_SET_ID = ?1", nativeQuery = true)
	List<WorkHeader> findByOrderSetId(String ORDER_SET_ID);
}
