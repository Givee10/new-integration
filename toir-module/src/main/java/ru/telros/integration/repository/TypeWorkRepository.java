package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.TypeWork;

import java.util.List;

@Repository
public interface TypeWorkRepository extends JpaRepository<TypeWork, Long> {
	@Query(value = "SELECT o FROM TypeWork o WHERE (o.ACTIVITY_ASSOCIATION_ID = ?1)")
	TypeWork findByParameters(String activity_association_id);

	@Query(value = "SELECT o FROM TypeWork o WHERE (o.ACTIVITY = ?1) AND ROWNUM = 1")
	TypeWork findByOperCode(String activity);

	@Query(value = "SELECT o FROM TypeWork o WHERE (o.ASSET_ACTIVITY_ID = ?1) AND (o.ORGANIZATION_ID = ?2) AND ROWNUM = 1")
	TypeWork findByNumberAndOrganization(String ASSET_ACTIVITY_ID, String ORGANIZATION_ID);

	@Query(value = "SELECT * FROM TYPE_WORK WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<TypeWork> findActive();
}
