package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Street;

import java.util.List;

@Repository
public interface StreetRepository extends JpaRepository<Street, Long> {
	@Query(value = "SELECT o FROM Street o WHERE o.PRF_ID = ?1")
	Street findByParameters(String prf_id);

	@Query(value = "SELECT * FROM STREET WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Street> findActive();
}
