package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Employee;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	@Query(value = "SELECT o FROM Employee o WHERE (o.PERSON_ID = ?1)")
	Employee findByParameters(String person_id);

	@Query(value = "SELECT * FROM EMPLOYEE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Employee> findActive();
}
