package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.telros.integration.domain.MessageImportPlan;

public interface MessageImportPlanRepository extends JpaRepository<MessageImportPlan, Long> {
	@Query(value = "SELECT o FROM MessageImportPlan o WHERE (o.ORDER_SET_ID = ?1)")
	MessageImportPlan findByParameters(Long order_set_id);

	@Query(value = "SELECT o FROM MessageImportPlan o WHERE (o.BRIGADE_PLAN_ID = ?1)")
	MessageImportPlan findByPlan(Long brigade_plan_id);
}
