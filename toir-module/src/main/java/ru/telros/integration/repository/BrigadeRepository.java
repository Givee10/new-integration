package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Brigade;

import java.util.List;

@Repository
public interface BrigadeRepository extends JpaRepository<Brigade, Long> {
	@Query(value = "SELECT o FROM Brigade o WHERE (o.DEPARTMENT_ID = ?1)")
	Brigade findByParameters(String department_id);

	@Query(value = "SELECT o FROM Brigade o WHERE (o.DESCRIPTION = ?1)")
	Brigade findByName(String description);

	@Query(value = "SELECT * FROM BRIGADE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Brigade> findActive();
}
