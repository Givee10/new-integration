package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.WorkMap;

import java.util.List;

@Repository
public interface WorkMapRepository extends JpaRepository<WorkMap, Long> {
	@Query(value = "SELECT o FROM WorkMap o WHERE (o.WIP_ENTITY_ID = ?1) AND (o.OPERATION_SEQ_NUM = ?2)")
	WorkMap findByParameters(String wip_entity_id, String operation_seq_num);

	@Query(value = "SELECT * FROM WORK_MAP WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<WorkMap> findActive();
}
