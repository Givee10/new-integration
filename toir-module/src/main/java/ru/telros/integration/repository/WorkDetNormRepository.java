package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.WorkDetNorm;

import java.util.List;

@Repository
public interface WorkDetNormRepository extends JpaRepository<WorkDetNorm, Long> {
	@Query(value = "SELECT o FROM WorkDetNorm o WHERE (o.ACTION_SEQ_ID = ?1) AND (o.WIP_ENTITY_ID = ?2)")
	WorkDetNorm findByParameters(String action_seq_id, String wip_entity_id);

	@Query(value = "SELECT * FROM WORK_DET_NORM WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<WorkDetNorm> findActive();
}
