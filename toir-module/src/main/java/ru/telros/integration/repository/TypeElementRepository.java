package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.TypeElement;

import java.util.List;

@Repository
public interface TypeElementRepository extends JpaRepository<TypeElement, Long> {
	@Query(value = "SELECT o FROM TypeElement o WHERE (o.EO_TYPE = ?1)")
	TypeElement findByParameters(String eo_type);

	@Query(
			value = "SELECT TTE.* FROM TOIR.TYPE_ELEMENT tte \n" +
					"LEFT JOIN TOIR.TYPE_WORK_ELEMENT ttwe ON TTWE.EO_TYPE = TTE.EO_TYPE \n" +
					"WHERE TTWE.OPER_CODE = :oper_code ORDER BY TTE.ID",
			nativeQuery = true
	)
	List<TypeElement> findTypeElementList(@Param("oper_code") String oper_code);

	@Query(value = "SELECT * FROM TYPE_ELEMENT WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<TypeElement> findActive();

	@Query(value = "SELECT * FROM TYPE_ELEMENT WHERE EO_TYPE_DESCR = ?1 AND PRIMARY_UOM = ?2", nativeQuery = true)
	TypeElement findByDescAndMetric(String EO_TYPE_DESCR, String PRIMARY_UOM);
}
