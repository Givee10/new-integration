package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.AbsenceReason;

import java.util.List;

@Repository
public interface AbsenceReasonRepository extends JpaRepository<AbsenceReason, Long> {
	@Query(value = "SELECT o FROM AbsenceReason o WHERE (o.FLEX_VALUE_ID = ?1)")
	AbsenceReason findByParameters(String FLEX_VALUE_ID);

	@Query(value = "SELECT o FROM AbsenceReason o WHERE (o.DESCRIPTION = ?1)")
	AbsenceReason findByName(String DESCRIPTION);

	@Query(value = "SELECT * FROM ABSENCE_REASON WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<AbsenceReason> findActive();
}
