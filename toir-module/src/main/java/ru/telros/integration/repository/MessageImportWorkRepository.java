package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.telros.integration.domain.MessageImportWork;

public interface MessageImportWorkRepository extends JpaRepository<MessageImportWork, Long> {
	@Query(value = "SELECT o FROM MessageImportWork o WHERE (o.WIP_ENTITY_ID = ?1)")
	MessageImportWork findByParameters(Long WIP_ENTITY_ID);

	@Query(value = "SELECT o FROM MessageImportWork o WHERE (o.WORK_ORDER_ID = ?1)")
	MessageImportWork findByWork(Long WORK_ORDER_ID);
}
