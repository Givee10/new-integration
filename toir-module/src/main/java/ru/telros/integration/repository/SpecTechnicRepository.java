package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.SpecTechnic;

import java.util.List;

@Repository
public interface SpecTechnicRepository extends JpaRepository<SpecTechnic, Long> {
	@Query(value = "SELECT o FROM SpecTechnic o WHERE (o.CODE_ID = ?1)")
	SpecTechnic findByParameters(String code_id);

	@Query(value = "SELECT o FROM SpecTechnic o WHERE (o.TECH_CODE = ?1)")
	SpecTechnic findByTechCode(String tech_code);

	@Query(value = "SELECT * FROM SPEC_TECHNIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<SpecTechnic> findActive();
}
