package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.SmallSpecTechnic;

import java.util.List;

@Repository
public interface SmallSpecTechnicRepository extends JpaRepository<SmallSpecTechnic, Long> {
	@Query(value = "SELECT o FROM SmallSpecTechnic o WHERE (o.SMM_ID = ?1)")
	SmallSpecTechnic findByParameters(String smm_id);

	@Query(value = "SELECT o FROM SmallSpecTechnic o WHERE (o.INV_NUMBER = ?1) AND (o.TECH_CODE = ?2)")
	SmallSpecTechnic findByNumberAndCode(String inv_number, String tech_code);

	@Query(value = "SELECT * FROM SMALL_SPEC_TECHNIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<SmallSpecTechnic> findActive();
}
