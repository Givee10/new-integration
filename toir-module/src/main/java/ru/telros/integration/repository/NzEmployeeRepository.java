package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.NzEmployee;

import java.util.List;

@Repository
public interface NzEmployeeRepository extends JpaRepository<NzEmployee, Long> {
	@Query(value = "SELECT o FROM NzEmployee o WHERE (o.ORDER_SET_ID = ?1) AND (o.PERSON_ID = ?2)")
	NzEmployee findByParameters(String order_set_id, String person_id);

	@Query(value = "SELECT o FROM NzEmployee o WHERE (o.ORDER_SET_ID = ?1)")
	List<NzEmployee> findByOrderSetId(String order_set_id);

	@Query(value = "SELECT * FROM NZ_EMPLOYEE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<NzEmployee> findActive();
}
