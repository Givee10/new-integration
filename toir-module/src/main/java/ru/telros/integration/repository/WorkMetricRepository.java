package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.WorkMetric;

import java.util.List;

@Repository
public interface WorkMetricRepository extends JpaRepository<WorkMetric, Long> {
	@Query(value = "SELECT o FROM WorkMetric o WHERE (o.METRICS_CODE = ?1) AND (o.WIP_ENTITY_ID = ?2)")
	WorkMetric findByParameters(String metrics_code, String wip_entity_id);

	@Query(value = "SELECT * FROM WORK_METRIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<WorkMetric> findActive();
}
