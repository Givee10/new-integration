package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.TechnicType;

import java.util.List;

@Repository
public interface TechnicTypeRepository extends JpaRepository<TechnicType, Long> {
	@Query(value = "SELECT o FROM TechnicType o WHERE (o.TYPE_ID = ?1)")
	TechnicType findByParameters(String type_id);

	@Query(value = "SELECT * FROM TECHNIC_TYPE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<TechnicType> findActive();
}
