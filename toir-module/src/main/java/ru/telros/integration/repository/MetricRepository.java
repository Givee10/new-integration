package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Metric;

import java.util.List;

@Repository
public interface MetricRepository extends JpaRepository<Metric, Long> {
	@Query(value = "SELECT o FROM Metric o WHERE (o.INVENTORY_ITEM_ID = ?1) AND (o.METRICS_CODE = ?2)")
	Metric findByParameters(String inventory_item_id, String metrics_code);

	@Query(
			value = "SELECT TM.* FROM TOIR.METRIC tm \n" +
					"LEFT JOIN TOIR.STANDART ts ON TM.METRICS_CODE = TS.METRICS_CODE AND TM.OPER = TS.OPER \n" +
					"WHERE TM.OPER = :oper_code AND TS.DEFAULT_METRIC_VALUE IS NOT NULL ORDER BY TM.ID",
			nativeQuery = true
	)
	List<Metric> findMetricList(@Param("oper_code") String oper_code);

	@Query(value = "SELECT * FROM METRIC WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Metric> findActive();
}
