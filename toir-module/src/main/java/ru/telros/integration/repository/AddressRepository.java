package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Address;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
	@Query(value = "SELECT o FROM Address o WHERE (o.ADDR_ID = ?1) AND (o.PRF_ID = ?2)")
	Address findByParameters(String addr_id, String prf_id);

	@Query(value = "SELECT * FROM ADDRESS WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Address> findActive();
}
