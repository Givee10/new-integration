package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Standart;

import java.util.List;

@Repository
public interface StandartRepository extends JpaRepository<Standart, Long> {
	@Query(value = "SELECT o FROM Standart o WHERE (o.OPERATION_SEQUENCE_ID = ?1)")
	Standart findByParameters(String operation_sequence_id);

	@Query(value = "SELECT o FROM Standart o WHERE (o.OPERATION_DESCRIPTION = ?1) AND (o.OPER_CODE = ?2) AND (o.OPER = ?3)")
	Standart findByName(String operation_description, String code, String oper);

	@Query(
			value = "SELECT TS.* FROM TOIR.STANDART ts WHERE TS.OPER = :oper_code ORDER BY TO_NUMBER(TS.OPER_CODE)",
			nativeQuery = true
	)
	List<Standart> findStandartList(@Param("oper_code") String oper_code);

	@Query(value = "SELECT * FROM STANDART WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Standart> findActive();
}
