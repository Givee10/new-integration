package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.WorkEO;

import java.util.List;

@Repository
public interface WorkEORepository extends JpaRepository<WorkEO, Long> {
	@Query(value = "SELECT o FROM WorkEO o WHERE (o.EO_TYPE = ?1) AND (o.WIP_ENTITY_ID = ?2)")
	WorkEO findByParameters(String eo_type, String wip_entity_id);

	@Query(value = "SELECT * FROM WORK_EO WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<WorkEO> findActive();
}
