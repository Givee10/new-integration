package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.TypeWorkDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TypeWorkMapper implements RowMapper<TypeWorkDto> {
	@Override
	public TypeWorkDto mapRow(ResultSet resultSet, int i) throws SQLException {
		TypeWorkDto typeWorkDto = new TypeWorkDto();
		typeWorkDto.setId(resultSet.getLong("ID"));
		typeWorkDto.setGuid(StringUtil.returnString(resultSet.getString("GUID")));
		typeWorkDto.setCode(StringUtil.returnString(resultSet.getString("CODE")));
		typeWorkDto.setDescription(StringUtil.returnString(resultSet.getString("DESCRIPTION")));
		typeWorkDto.setNorm(resultSet.getDouble("NORM"));
		typeWorkDto.setNumberToir(StringUtil.returnString(resultSet.getString("NUMBER_TOIR")));
		return typeWorkDto;
	}
}
