package ru.telros.integration.controller;

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.domain.*;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

@Component
public class CustomRestTemplate {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomRestTemplate.class);

	@Value("${test.server-ip}")
	private String serverIp;
	@Value("${test.server-port}")
	private String serverPort;
	@Value("${test.server-hl-ip}")
	private String serverHlIp;
	@Value("${test.server-hl-port}")
	private String serverHlPort;

	private static final String brigades = "/brigades";
	private static final String brigade_plans = "/brigades/%d/plans";
	private static final String brigade_plan_employees = "/brigades/%d/plans/%d/employees";
	private static final String brigade_plan_equipments = "/brigades-plans/%d/equipments";
	private static final String brigade_plan_mechanization = "/brigades-plans/%d/mechanization";
	private static final String brigade_works = "/brigades/%d/works";
	private static final String type_work_items = "/types-works/%d/items";
	private static final String works = "/works";
	private static final String work_items = "/works/%d/work-items";

	private Integer custom_skip = 0;
	private Integer custom_limit = 200;

	@Autowired
	private RestTemplateBuilder builder;

	private RestTemplate getRestTemplate() {
		return builder.basicAuthorization("admin", "admin").build();
	}

	private MultiValueMap<String, String> createMap() {
		return new LinkedMultiValueMap<>();
	}

	private MultiValueMap<String, String> getMap() {
		return getMap(null, null, custom_skip, custom_limit);
	}

	private MultiValueMap<String, String> getMap(Integer skip, Integer limit) {
		return getMap(null, null, skip, limit);
	}

	private MultiValueMap<String, String> getMap(ZonedDateTime start, ZonedDateTime end) {
		return getMap(start, end, custom_skip, custom_limit);
	}

	private MultiValueMap<String, String> getMap(ZonedDateTime start, ZonedDateTime end, Integer skip, Integer limit) {
		MultiValueMap<String, String> map = createMap();
		if (start != null) map.add("from", DatesUtil.formatToUTC(start));
		if (end != null) map.add("to", DatesUtil.formatToUTC(end));
		map.add("skip", skip.toString());
		map.add("limit", limit.toString());
		return map;
	}

	private <T> T makeEntityRequest(String url, HttpMethod method, HttpEntity<T> httpEntity, Class<T> tClass) {
		try {
			TypeToken<T> resultTypeToken = TypeToken.of(tClass);
			ParameterizedTypeReference<T> responseTypeRef = ParameterizedTypeReferenceBuilder.fromTypeToken(
					resultTypeToken.where(new TypeParameter<T>() {}, tClass));
			ResponseEntity<T> response = getRestTemplate().exchange(url, method, httpEntity, responseTypeRef);
			return response.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private <T> List<T> makeListRequest(String url, HttpMethod method, HttpEntity<List<T>> httpEntity, Class<T> tClass) {
		try {
			TypeToken<T> resultTypeToken = TypeToken.of(tClass);
			ParameterizedTypeReference<List<T>> responseTypeRef = ParameterizedTypeReferenceBuilder.fromTypeToken(
					new TypeToken<List<T>>() {}.where(new TypeParameter<T>() {}, resultTypeToken));
			ResponseEntity<List<T>> response = getRestTemplate().exchange(url, method, httpEntity, responseTypeRef);
			return response.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private static void logExceptions(String url, RestClientException e) {
		if (e instanceof RestClientResponseException) {
			LOGGER.error("Request {} returns with error {}", url, ((RestClientResponseException)e).getResponseBodyAsString());
		}
		if (e instanceof ResourceAccessException) {
			LOGGER.error(e.getMessage());
		}
	}

	private String getBaseUrl(String path) {
		return String.format("http://%s:%s/api/v1/%s", serverIp, serverPort, path);
	}

	public List<BrigadeDto> getAllBrigades() {
		String url = getBaseUrl(brigades);
		url = UriComponentsBuilder.fromUriString(url).queryParams(getMap()).build().toUriString();
		return makeListRequest(url, HttpMethod.GET, null, BrigadeDto.class);
	}

	public List<BrigadePlanDto> getBrigadePlans(Long id, ZonedDateTime start, ZonedDateTime finish) {
		MultiValueMap<String, String> map = getMap(start, finish);
		String url = String.format(getBaseUrl(brigade_plans), id);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return makeListRequest(url, HttpMethod.GET, null, BrigadePlanDto.class);
	}

	public BrigadePlanDto saveBrigadePlan(BrigadePlanDto brigadePlanDto) {
		String url = String.format(getBaseUrl(brigade_plans), brigadePlanDto.getIdBrigade());
		return makeEntityRequest(url, HttpMethod.POST, new HttpEntity<>(brigadePlanDto), BrigadePlanDto.class);
	}

	public List<EmployeeBrigadePlanDto> savePlanEmployees(BrigadePlanDto brigadePlanDto, List<EmployeeBrigadePlanDto> employeeList) {
		String url = String.format(getBaseUrl(brigade_plan_employees), brigadePlanDto.getIdBrigade(), brigadePlanDto.getId());
		return makeListRequest(url, HttpMethod.POST, new HttpEntity<>(employeeList), EmployeeBrigadePlanDto.class);
	}

	public List<EquipmentBrigadePlanDto> savePlanEquipments(BrigadePlanDto brigadePlanDto, List<EquipmentBrigadePlanDto> equipmentList) {
		String url = String.format(getBaseUrl(brigade_plan_equipments), brigadePlanDto.getId());
		return makeListRequest(url, HttpMethod.POST, new HttpEntity<>(equipmentList), EquipmentBrigadePlanDto.class);
	}

	public List<MechanizationBrigadePlanDto> savePlanMechanization(BrigadePlanDto brigadePlanDto, List<MechanizationBrigadePlanDto> mechanizationList) {
		String url = String.format(getBaseUrl(brigade_plan_mechanization), brigadePlanDto.getId());
		return makeListRequest(url, HttpMethod.POST, new HttpEntity<>(mechanizationList), MechanizationBrigadePlanDto.class);
	}

	public List<WorkDto> getBrigadeWorks(Long id, ZonedDateTime start, ZonedDateTime finish) {
		MultiValueMap<String, String> map = getMap(start, finish);
		String url = String.format(getBaseUrl(brigade_works), id);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return makeListRequest(url, HttpMethod.GET, null, WorkDto.class);
	}

	public List<TypeWorkItemDto> getTypeWorkItems(Long typeWorkId) {
		String url = String.format(getBaseUrl(type_work_items), typeWorkId);
		return makeListRequest(url, HttpMethod.GET, null, TypeWorkItemDto.class);
	}

	public WorkDto saveWork(WorkDto workDto) {
		String url = getBaseUrl(works);
		return makeEntityRequest(url, HttpMethod.POST, new HttpEntity<>(workDto), WorkDto.class);
	}

	public List<WorkItemDto> saveWorkItems(Long workId, List<WorkItemDto> items) {
		String url = String.format(getBaseUrl(work_items), workId);
		return makeListRequest(url, HttpMethod.POST, new HttpEntity<>(items), WorkItemDto.class);
	}

	public Long getAddressId(String number) {
		String url = String.format("http://%s:%s/api/v1/incidents/address?number=%s", serverHlIp, serverHlPort, number);
		try {
			ResponseEntity<String> entity = getRestTemplate().getForEntity(url, String.class);
			return Long.parseLong(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}
}
