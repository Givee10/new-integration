package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.OrderSet;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderSetRowMapper implements RowMapper<OrderSet> {
	@Override
	public OrderSet mapRow(ResultSet resultSet, int i) throws SQLException {
		OrderSet orderSet = new OrderSet();
		orderSet.XXBR_SET_ID = resultSet.getLong("XXBR_SET_ID");
		orderSet.ORDER_SET_RAW_ID = resultSet.getLong("ORDER_SET_RAW_ID");
		orderSet.ORGANIZATION_ID = resultSet.getLong("ORGANIZATION_ID");
		orderSet.ORGANIZATION_NAME = StringUtil.returnString(resultSet.getString("ORGANIZATION_NAME"));
		orderSet.ORDER_SET_DATE = DatesUtil.timestampToString(resultSet.getTimestamp("ORDER_SET_DATE"));
		orderSet.DEPARTMENT_ID = resultSet.getLong("DEPARTMENT_ID");
		orderSet.DEPARTMENT_NAME = StringUtil.returnString(resultSet.getString("DEPARTMENT_NAME"));
		orderSet.LAST_UPDATE_DATE = DatesUtil.timestampToString(resultSet.getTimestamp("LAST_UPDATE_DATE"));
		orderSet.LAST_UPDATED_BY = StringUtil.returnString(resultSet.getString("LAST_UPDATED_BY"));
		orderSet.CREATION_DATE = DatesUtil.timestampToString(resultSet.getTimestamp("CREATION_DATE"));
		orderSet.CREATED_BY = StringUtil.returnString(resultSet.getString("CREATED_BY"));
		orderSet.START_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("START_TIME"));
		orderSet.END_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("END_TIME"));
		return orderSet;
	}
}
