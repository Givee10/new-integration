package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.WORouting;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WORoutingRowMapper implements RowMapper<WORouting> {
	@Override
	public WORouting mapRow(ResultSet resultSet, int i) throws SQLException {
		WORouting woRouting = new WORouting();
		woRouting.XXBR_WIP_ENTITY_ID = resultSet.getLong("XXBR_WIP_ENTITY_ID");
		woRouting.ACTION_SEQ_ID = resultSet.getLong("ACTION_SEQ_ID");
		woRouting.ACTIVE_FLAG = StringUtil.returnString(resultSet.getString("ACTIVE_FLAG"));
		woRouting.OPERATION_ITEM_ID = resultSet.getLong("OPERATION_ITEM_ID");
		woRouting.OPERATION_SEQ_NUM = resultSet.getLong("OPERATION_SEQ_NUM");
		woRouting.OPERATION_DESCRIPTION = StringUtil.returnString(resultSet.getString("OPERATION_DESCRIPTION"));
		woRouting.OPERATION_CODE = StringUtil.returnString(resultSet.getString("OPERATION_CODE"));
		woRouting.OPER = StringUtil.returnString(resultSet.getString("OPER"));
		return woRouting;
	}
}
