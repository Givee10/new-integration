package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.WONormDet;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WONormDetRowMapper implements RowMapper<WONormDet> {
	@Override
	public WONormDet mapRow(ResultSet resultSet, int i) throws SQLException {
		WONormDet woNormDet = new WONormDet();
		woNormDet.XXBR_WIP_ENTITY_ID = resultSet.getLong("XXBR_WIP_ENTITY_ID");
		woNormDet.OPERATION_ITEM_ID = resultSet.getLong("OPERATION_ITEM_ID");
		woNormDet.ACTION_SEQ_ID = resultSet.getLong("ACTION_SEQ_ID");
		woNormDet.RESOURCE_ID = resultSet.getLong("RESOURCE_ID");
		woNormDet.EO_ID = resultSet.getLong("EO_ID");
		woNormDet.EO_TYPE = StringUtil.returnString(resultSet.getString("EO_TYPE"));
		woNormDet.CALC_NORM = StringUtil.returnString(resultSet.getString("CALC_NORM"));
		woNormDet.METRICS_CODE = StringUtil.returnString(resultSet.getString("METRICS_CODE"));
		return woNormDet;
	}
}
