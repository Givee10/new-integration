package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.OrderTask;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderTaskRowMapper implements RowMapper<OrderTask> {
	@Override
	public OrderTask mapRow(ResultSet resultSet, int i) throws SQLException {
		OrderTask orderTask = new OrderTask();
		orderTask.XXBR_SET_ID = resultSet.getLong("XXBR_SET_ID");
		orderTask.XXBR_WIP_ENTITY_ID = resultSet.getLong("XXBR_WIP_ENTITY_ID");
		orderTask.WIP_ENTITY_RAW_ID = resultSet.getLong("WIP_ENTITY_RAW_ID");
		orderTask.START_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("START_TIME"));
		orderTask.END_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("END_TIME"));
		orderTask.FACT_TIME = StringUtil.returnString(resultSet.getString("FACT_TIME"));
		orderTask.COMMENTS = StringUtil.returnString(resultSet.getString("COMMENTS"));
		orderTask.LAST_UPDATE_DATE = DatesUtil.timestampToString(resultSet.getTimestamp("LAST_UPDATE_DATE"));
		orderTask.LAST_UPDATED_BY = StringUtil.returnString(resultSet.getString("LAST_UPDATED_BY"));
		orderTask.CREATION_DATE = DatesUtil.timestampToString(resultSet.getTimestamp("CREATION_DATE"));
		orderTask.CREATED_BY = StringUtil.returnString(resultSet.getString("CREATED_BY"));
		orderTask.LAST_UPDATE_LOGIN = StringUtil.returnString(resultSet.getString("LAST_UPDATE_LOGIN"));
		orderTask.M_FACT_START = DatesUtil.timestampToString(resultSet.getTimestamp("M_FACT_START"));
		orderTask.M_FACT_END = DatesUtil.timestampToString(resultSet.getTimestamp("M_FACT_END"));
		orderTask.M_FACT_TIME = StringUtil.returnString(resultSet.getString("M_FACT_TIME"));
		orderTask.VALIDATION_TEXT = StringUtil.returnString(resultSet.getString("VALIDATION_TEXT"));
		orderTask.ACTIVITY_NAME = StringUtil.returnString(resultSet.getString("ACTIVITY_NAME"));
		orderTask.DOC_SOURCE = StringUtil.returnString(resultSet.getString("DOC_SOURCE"));
		orderTask.REQUEST_NUMBER = StringUtil.returnString(resultSet.getString("REQUEST_NUMBER"));
		orderTask.PRF_ID = resultSet.getLong("PRF_ID");
		orderTask.ADDR_ID = resultSet.getLong("ADDR_ID");
		orderTask.ADDR_COMMENT = StringUtil.returnString(resultSet.getString("ADDR_COMMENT"));
		return orderTask;
	}
}
