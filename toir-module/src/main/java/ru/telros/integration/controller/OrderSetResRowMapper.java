package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.OrderSetRes;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderSetResRowMapper implements RowMapper<OrderSetRes> {
	@Override
	public OrderSetRes mapRow(ResultSet resultSet, int i) throws SQLException {
		OrderSetRes orderSetRes = new OrderSetRes();
		orderSetRes.XXBR_SET_ID = resultSet.getLong("XXBR_SET_ID");
		orderSetRes.EMPLOYEER_CODE = StringUtil.returnString(resultSet.getString("EMPLOYEER_CODE"));
		orderSetRes.PLAN_TIME = StringUtil.returnString(resultSet.getString("PLAN_TIME"));
		orderSetRes.FACT_TIME = StringUtil.returnString(resultSet.getString("FACT_TIME"));
		orderSetRes.PERSON_ID = resultSet.getLong("PERSON_ID");
		orderSetRes.COMMENTS = StringUtil.returnString(resultSet.getString("COMMENTS"));
		orderSetRes.ABSENCE_REASON = StringUtil.returnString(resultSet.getString("ABSENCE_REASON"));
		orderSetRes.ABSENCE_COMMENT = StringUtil.returnString(resultSet.getString("ABSENCE_COMMENT"));
		orderSetRes.ARRIVED = DatesUtil.timestampToString(resultSet.getTimestamp("ARRIVED"));
		orderSetRes.DEPARTED = DatesUtil.timestampToString(resultSet.getTimestamp("DEPARTED"));
		orderSetRes.IS_BRIGADIER = StringUtil.returnString(resultSet.getString("IS_BRIGADIER"));
		orderSetRes.TIME_COMMENTS = StringUtil.returnString(resultSet.getString("TIME_COMMENTS"));
		orderSetRes.ST_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("ST_TIME"));
		orderSetRes.EN_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("EN_TIME"));
		orderSetRes.TYPE = StringUtil.returnString(resultSet.getString("TYPE"));
		orderSetRes.NUMBER = StringUtil.returnString(resultSet.getString("NUMBER"));
		orderSetRes.CODE = StringUtil.returnString(resultSet.getString("CODE"));
		orderSetRes.SMM_ID = resultSet.getLong("SMM_ID");
		orderSetRes.CODE_ID = resultSet.getLong("CODE_ID");
		return orderSetRes;
	}
}
