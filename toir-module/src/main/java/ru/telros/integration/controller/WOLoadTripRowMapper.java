package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.WOLoadTrip;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WOLoadTripRowMapper implements RowMapper<WOLoadTrip> {
	@Override
	public WOLoadTrip mapRow(ResultSet resultSet, int i) throws SQLException {
		WOLoadTrip woLoadTrip = new WOLoadTrip();
		woLoadTrip.TYPE_WORK = StringUtil.returnString(resultSet.getString("TYPE_WORK"));
		woLoadTrip.ORGANIZATION_ID = resultSet.getLong("ORGANIZATION_ID");
		woLoadTrip.XXBR_SET_ID = resultSet.getLong("XXBR_SET_ID");
		woLoadTrip.START_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("START_TIME"));
		woLoadTrip.END_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("END_TIME"));
		woLoadTrip.FACT_TIME = StringUtil.returnString(resultSet.getString("FACT_TIME"));
		return woLoadTrip;
	}
}
