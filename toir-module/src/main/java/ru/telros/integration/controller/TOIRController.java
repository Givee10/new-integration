package ru.telros.integration.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.telros.integration.domain.EODto;
import ru.telros.integration.domain.MetricDto;
import ru.telros.integration.domain.OperationDto;
import ru.telros.integration.domain.WorkHeadDto;
import ru.telros.integration.elements.AbstractResponse;
import ru.telros.integration.elements.ResponseItem;
import ru.telros.integration.elements.Result;
import ru.telros.integration.elements.ResultItem;
import ru.telros.integration.elements.toircalc.*;
import ru.telros.integration.elements.toirnz.*;
import ru.telros.integration.elements.toirsprav.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class TOIRController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOIRController.class);

	@Autowired
	private ToirClient toirClient;
	@Autowired
	private ToirRefClient toirRefClient;
	@Autowired
	private ToirCalcClient toirCalcClient;
	@Autowired
	private ObjectMapper objectMapper;

	public static String logResponse(AbstractResponse response) {
		ObjectMapper objectMapper = new ObjectMapper();
		Result result = response.getResult();
		List<ResultItem> itemList = result.getItem();
		ArrayNode arrayNode = objectMapper.createArrayNode();
		int i = 0;
		for (ResultItem item : itemList) {
			i++;
			List<ResponseItem> responseItemList = item.getItem();
			ObjectNode objectNode = objectMapper.createObjectNode();
			for (ResponseItem responseItem : responseItemList) {
				//LOGGER.debug("{} {} - Key: {}, Value: {}", response.getClass().getSimpleName(), i, responseItem.getKey(), responseItem.getValue());
				objectNode.put(responseItem.getKey(), responseItem.getValue());
			}
			arrayNode.add(objectNode);
		}
		return arrayNode.toString();
	}

	public static String logResponse(AbstractPutResponse response) {
		return response.getRetcode().getRetcode();
	}

	/*
	 * 1	getTypesWorks() - метод веб-сервиса, возвращающий виды работ
	 * 2	getEmployeers() - метод веб-сервиса, возвращающий сотрудников
	 * 3	getBrigades() - метод веб-сервиса, возвращающий бригады
	 * 4	getStructureBrigades() - метод веб-сервиса, возвращающий состав бригад
	 * 5	getSource() - метод веб-сервиса, возвращающий справочник источников работ
	 * 6	getAbsenceReasons() - метод веб-сервиса, возвращающий справочник причин отсутствия
	 * 7	getDeviationReasons() - метод веб-сервиса, возвращающий справочник причин отклонения рабочего времени сотрудника от времени бригады
	 * 8	getStreet() - метод веб-сервиса, возвращающий улицы (префиксы)
	 * 9	getAddress()  - метод веб-сервиса, возвращающий адреса (дома)
	 * 10	getTypeElements() - метод веб-сервиса, возвращающий типы элементов обслуживания
	 * 11   getStandarts() - метод веб-сервиса, возвращающий техкарты (виды работ с разбивкой на действия)
	 * 12	getResources() - метод веб-сервиса, возвращающий ресурсы (слесарь, мастер, изолировщик и т.п.).
	 * 13	getTechnicTypes() - метод веб-сервиса, возвращающий типы техники
	 * 14	getSmallTechnicTypes()- метод веб-сервиса, возвращающий типы малой механизации
	 * 15	getSpecTechnicTypes() - метод веб-сервиса, возвращающий справочник единиц техники
	 * 16	getSmallSpecTechnicTypes() - метод веб-сервиса, возвращающий справочник единиц малой механизации
	 * 17	getZones() - метод веб-сервиса, возвращающий справочник районов, ТУВ, ПУАР и зависимостей между ними
	 * 18	getMetrics() - метод веб-сервиса, возвращающий метрики (для видов работ)
	 * 19	getTypeElements_TypeWorks()- метод веб-сервиса, возвращающийсправочник зависимостей между типами элементов обслуживания и видами работ
	 */

	@GetMapping(value ="typesworks")
	public @ResponseBody String getTypesWorks() {
		TypesWorksResponse response = toirRefClient.getTypesWorks();
		return logResponse(response);
	}

	@GetMapping(value ="employees")
	public @ResponseBody String getEmployees() {
		EmployeersResponse response = toirRefClient.getEmployees();
		return logResponse(response);
	}

	@GetMapping(value ="brigades")
	public @ResponseBody String getBrigades() {
		BrigadesResponse response = toirRefClient.getBrigades();
		return logResponse(response);
	}

	@GetMapping(value ="brigades/structure")
	public @ResponseBody String getStructureBrigades() {
		StructureBrigadesResponse response = toirRefClient.getStructureBrigades();
		return logResponse(response);
	}

	@GetMapping(value ="sources")
	public @ResponseBody String getSources() {
		SourceResponse response = toirRefClient.getSources();
		return logResponse(response);
	}

	@GetMapping(value ="absencereasons")
	public @ResponseBody String getAbsenceReasons() {
		AbsenceReasonsResponse response = toirRefClient.getAbsenceReasons();
		return logResponse(response);
	}

	@GetMapping(value ="deviationreasons")
	public @ResponseBody String getDeviationReasons() {
		DeviationReasonsResponse response = toirRefClient.getDeviationReasons();
		return logResponse(response);
	}

	@GetMapping(value ="streets")
	public @ResponseBody String getStreets() {
		StreetsResponse response = toirRefClient.getStreets();
		return logResponse(response);
	}

	@GetMapping(value ="addresses")
	public @ResponseBody String getAddresses() {
		AddressResponse response = toirRefClient.getAddresses();
		return logResponse(response);
	}

	@GetMapping(value ="standarts")
	public @ResponseBody String getStandarts() {
		StandartsResponse response = toirRefClient.getStandarts();
		return logResponse(response);
	}

	@GetMapping(value ="resources")
	public @ResponseBody String getResources() {
		ResourcesResponse response = toirRefClient.getResources();
		return logResponse(response);
	}

	@GetMapping(value ="technictypes")
	public @ResponseBody String getTechnicTypes() {
		TechnicTypesResponse response = toirRefClient.getTechnicTypes();
		return logResponse(response);
	}

	@GetMapping(value ="spectechnictypes")
	public @ResponseBody String getSpecTechnicTypes() {
		SpecTechnicTypesResponse response = toirRefClient.getSpecTechnicTypes();
		return logResponse(response);
	}

	@GetMapping(value ="smalltechnictypes")
	public @ResponseBody String getSmallTechnicTypes() {
		SmallTechnicTypesResponse response = toirRefClient.getSmallTechnicTypes();
		return logResponse(response);
	}

	@GetMapping(value ="sstechnictypes")
	public @ResponseBody String getSmallSpecTechnicTypes() {
		SmallSpecTechnicTypesResponse response = toirRefClient.getSmallSpecTechnicTypes();
		return logResponse(response);
	}

	@GetMapping(value ="typeelements")
	public @ResponseBody String getTypeElements() {
		TypeElementsResponse response = toirRefClient.getTypeElements();
		return logResponse(response);
	}

	@GetMapping(value ="metrics")
	public @ResponseBody String getMetrics() {
		MetricsResponse response = toirRefClient.getMetrics();
		return logResponse(response);
	}

	@GetMapping(value ="zones")
	public @ResponseBody String getZones() {
		ZonesResponse response = toirRefClient.getZones();
		return logResponse(response);
	}

	@GetMapping(value ="typeelementsworks")
	public @ResponseBody String getTypeElementsWorks() {
		TypeElementsWorkResponse response = toirRefClient.getTypeElementsWorks();
		return logResponse(response);
	}

	/*
	 * 1.	getWorkHeaders() - метод веб-сервиса, возвращающий заголовки работ
	 * 2.	getWorkEos()- метод веб-сервиса, возвращающий элементы обслуживания
	 * 3.	getWorkMaps() - метод веб-сервиса, возвращающий карту  работ
	 * 4.	getWorkMetrics() - метод веб-сервиса, возвращающий метрики
	 * 5.	getWorkNorms()- метод веб-сервиса, возвращающий нормативы
	 * 6.	getWorkDetNorms()- метод веб-сервиса, возвращающий нормативы подробно

	 * 1.	getNzHeaders()- метод веб-сервиса, возвращающий заголовки наряд-заданий
	 * 2.	getNzEmployeers()- метод веб-сервиса, возвращающий назначенный на НЗ  персонал
	 * 3.	getNzTechnics () - метод веб-сервиса, возвращающий назначенную на НЗ технику
	 * 4.	getNzSmallTechnics () - метод веб-сервиса, возвращающий назначенную на НЗ малую механизацию
	 */

	@GetMapping(value ="works/headers")
	public @ResponseBody String getWorkHeaders() {
		WorkHeadersResponse response = toirClient.getWorkHeaders();
		return logResponse(response);
	}

	@GetMapping(value ="works/eo")
	public @ResponseBody String getWorkEos() {
		WorkEosResponse response = toirClient.getWorkEos();
		return logResponse(response);
	}

	@GetMapping(value ="works/maps")
	public @ResponseBody String getWorkMaps() {
		WorkMapsResponse response = toirClient.getWorkMaps();
		return logResponse(response);
	}

	@GetMapping(value ="works/metrics")
	public @ResponseBody String getWorkMetrics() {
		WorkMetricsResponse response = toirClient.getWorkMetrics();
		return logResponse(response);
	}

	@GetMapping(value ="works/norms")
	public @ResponseBody String getWorkNorms() {
		WorkNormsResponse response = toirClient.getWorkNorms();
		return logResponse(response);
	}

	@GetMapping(value ="works/detnorms")
	public @ResponseBody String getWorkDetNorms() {
		WorkDetNormsResponse response = toirClient.getWorkDetNorms();
		return logResponse(response);
	}

	@GetMapping(value ="nz/headers")
	public @ResponseBody String getNzHeaders() {
		NzHeadersResponse response = toirClient.getNzHeaders();
		return logResponse(response);
	}

	@GetMapping(value ="nz/employees")
	public @ResponseBody String getNzEmployees() {
		NzEmployeersResponse response = toirClient.getNzEmployees();
		return logResponse(response);
	}

	@GetMapping(value ="nz/technics")
	public @ResponseBody String getNzTechnics() {
		NzTechnicsResponse response = toirClient.getNzTechnics();
		return logResponse(response);
	}

	@GetMapping(value ="nz/smalltechnics")
	public @ResponseBody String getNzSmallTechnics() {
		NzSmallTechnicsResponse response = toirClient.getNzSmallTechnics();
		return logResponse(response);
	}

	/*
	 *
	 *
	 */
	@PostMapping(value = "eo")
	public @ResponseBody String putEO(@RequestBody EODto body) {
		PutEOResponse response = toirCalcClient.putEO(createEO(body));
		return logResponse(response);
	}

	@PostMapping(value = "metric")
	public @ResponseBody String putMetric(@RequestBody MetricDto body) {
		PutMetricResponse response = toirCalcClient.putMetric(createMetric(body));
		return logResponse(response);
	}

	@PostMapping(value = "oprc")
	public @ResponseBody String putOprc(@RequestBody OperationDto body) {
		PutOprcResponse response = toirCalcClient.putOprc(createOperation(body));
		return logResponse(response);
	}

	@PostMapping(value = "workhead")
	public @ResponseBody String putWorkHead(@RequestBody WorkHeadDto body) {
		PutWorkHeadResponse response = toirCalcClient.putWorkHead(createWorkHead(body));
		return logResponse(response);
	}

	@PostMapping(value = "workcalc/{id}")
	public @ResponseBody String putWorkCalc(@PathVariable("id") Long id) {
		PutWorkCalcResponse response = toirCalcClient.putWorkCalc(id.toString());
		return logResponse(response);
	}

	@GetMapping(value = "workcalc/{id}")
	public @ResponseBody String getCalcNorm(@PathVariable("id") Long id) {
		GetCalcNormResponse response = toirCalcClient.getCalcNorm(id.toString());
		return logResponse(response);
	}

	@GetMapping(value = "workcalc/{id}/detail")
	public @ResponseBody String getCalcNormDetail(@PathVariable("id") Long id) {
		GetCalcNormDetailResponse response = toirCalcClient.getCalcNormDetail(id.toString());
		return logResponse(response);
	}

	private EO createEO(EODto eoDto) {
		EO eo = new EO();
		eo.setpEoDiametr(eoDto.getEoDiametr());
		eo.setpEoLength(eoDto.getEoLength());
		eo.setpEoMatType(eoDto.getEoMatType());
		eo.setpEoType(eoDto.getEoType());
		eo.setpWipEntityId(eo.getpWipEntityId());
		LOGGER.info(writeValueAsString(eoDto));
		return eo;
	}

	private METRIC createMetric(MetricDto metricDto) {
		METRIC metric = new METRIC();
		metric.setpMetricsCode(metricDto.getMetricsCode());
		metric.setPmQuantity(metricDto.getQuantity());
		metric.setpWipEntityId(metricDto.getWipEntityId());
		LOGGER.info(writeValueAsString(metric));
		return metric;
	}

	private OPRC createOperation(OperationDto operationDto) {
		OPRC oprc = new OPRC();
		oprc.setpOperationSequenceId(operationDto.getOperationSequenceId());
		oprc.setpWipEntityId(operationDto.getWipEntityId());
		LOGGER.info(writeValueAsString(oprc));
		return oprc;
	}

	private WORKHEAD createWorkHead(WorkHeadDto workHeadDto) {
		WORKHEAD workhead = new WORKHEAD();
		workhead.setpInventoryItemId(workHeadDto.getInventoryItemId());
		workhead.setpScheduledStartDate(workHeadDto.getScheduledStartDate());
		workhead.setpWipEntityId(workHeadDto.getWipEntityId());
		LOGGER.info(writeValueAsString(workhead));
		return workhead;
	}

	private String writeValueAsString(Object value) {
		try {
			return objectMapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			LOGGER.error(e.getMessage());
			return "";
		}
	}
}
