package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.WOMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WOMaterialRowMapper implements RowMapper<WOMaterial> {
	@Override
	public WOMaterial mapRow(ResultSet resultSet, int i) throws SQLException {
		WOMaterial woMaterial = new WOMaterial();
		woMaterial.XXBR_SET_ID = resultSet.getLong("XXBR_SET_ID");
		woMaterial.XXBR_WIP_ENTITY_ID = resultSet.getLong("XXBR_WIP_ENTITY_ID");
		woMaterial.XXBR_MATERIAL_NAME = StringUtil.returnString(resultSet.getString("XXBR_MATERIAL_NAME"));
		woMaterial.XXBR_MATERIAL_UOM = StringUtil.returnString(resultSet.getString("XXBR_MATERIAL_UOM"));
		woMaterial.XXBR_QUANTITY = StringUtil.returnString(resultSet.getString("XXBR_QUANTITY"));
		return woMaterial;
	}
}
