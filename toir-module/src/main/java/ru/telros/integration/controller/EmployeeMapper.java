package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.EmployeeDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements RowMapper<EmployeeDto> {
	@Override
	public EmployeeDto mapRow(ResultSet resultSet, int i) throws SQLException {
		EmployeeDto employeeDto = new EmployeeDto();
		employeeDto.setId(resultSet.getLong("ID"));
		employeeDto.setIdUser(resultSet.getLong("USER_ID"));
		employeeDto.setFirstName(StringUtil.returnString(resultSet.getString("FIRSTNAME")));
		employeeDto.setMiddleName(StringUtil.returnString(resultSet.getString("MIDDLENAME")));
		employeeDto.setLastName(StringUtil.returnString(resultSet.getString("LASTNAME")));
		return employeeDto;
	}
}
