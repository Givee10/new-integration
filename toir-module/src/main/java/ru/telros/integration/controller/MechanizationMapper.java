package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.MechanizationDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MechanizationMapper implements RowMapper<MechanizationDto> {
	@Override
	public MechanizationDto mapRow(ResultSet resultSet, int i) throws SQLException {
		MechanizationDto mechanizationDto = new MechanizationDto();
		mechanizationDto.setId(resultSet.getLong("ID"));
		mechanizationDto.setGuid(StringUtil.returnString(resultSet.getString("GUID")));
		mechanizationDto.setType(StringUtil.returnString(resultSet.getString("TYPE")));
		mechanizationDto.setModel(StringUtil.returnString(resultSet.getString("MODEL")));
		mechanizationDto.setInventoryNumber(StringUtil.returnString(resultSet.getString("NUMBER")));
		mechanizationDto.setDescription(StringUtil.returnString(resultSet.getString("DESCRIPTION")));
		mechanizationDto.setCode(StringUtil.returnString(resultSet.getString("CODE")));
		mechanizationDto.setUnit(StringUtil.returnString(resultSet.getString("UNIT")));
		return mechanizationDto;
	}
}
