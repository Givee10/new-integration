package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.WOMetric;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WOMetricRowMapper implements RowMapper<WOMetric> {
	@Override
	public WOMetric mapRow(ResultSet resultSet, int i) throws SQLException {
		WOMetric woMetric = new WOMetric();
		woMetric.XXBR_WIP_ENTITY_ID = StringUtil.returnString(resultSet.getString("XXBR_WIP_ENTITY_ID"));
		woMetric.METRICS_CODE = StringUtil.returnString(resultSet.getString("METRICS_CODE"));
		woMetric.M_QUANTITY = StringUtil.returnString(resultSet.getString("M_QUANTITY"));
		return woMetric;
	}
}
