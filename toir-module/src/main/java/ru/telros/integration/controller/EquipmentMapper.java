package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.EquipmentDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EquipmentMapper implements RowMapper<EquipmentDto> {
	@Override
	public EquipmentDto mapRow(ResultSet resultSet, int i) throws SQLException {
		EquipmentDto equipmentDto = new EquipmentDto();
		equipmentDto.setId(resultSet.getLong("ID"));
		equipmentDto.setGuid(StringUtil.returnString(resultSet.getString("GUID")));
		equipmentDto.setType(StringUtil.returnString(resultSet.getString("TYPE")));
		equipmentDto.setModel(StringUtil.returnString(resultSet.getString("MODEL")));
		equipmentDto.setNumber(StringUtil.returnString(resultSet.getString("NUMBER")));
		equipmentDto.setDescription(StringUtil.returnString(resultSet.getString("DESCRIPTION")));
		equipmentDto.setCode(StringUtil.returnString(resultSet.getString("CODE")));
		equipmentDto.setUnit(StringUtil.returnString(resultSet.getString("UNIT")));
		return equipmentDto;
	}
}
