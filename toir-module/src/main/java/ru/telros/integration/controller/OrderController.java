package ru.telros.integration.controller;

import oracle.jdbc.pool.OracleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.*;
import ru.telros.integration.service.WorkService;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class OrderController {
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
	private OracleDataSource dataSource;

	@Autowired
	private WorkService workService;
	@Autowired
	private CustomRestTemplate template;

	public OrderController() {
		try {
			OracleDataSource dataSource = new OracleDataSource();
			dataSource.setUser("BRIGADES");
			dataSource.setPassword("BRIGADES");
//			dataSource.setUser("BRIGADES2");
//			dataSource.setPassword("brigades2");
			dataSource.setURL("jdbc:oracle:thin:@localhost:1521:XE");
			this.dataSource = dataSource;
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
	}

	@GetMapping(value = "collection")
	@ResponseBody
	public TOIRCollectionDto getCollection(
			@RequestParam String operCode) {
		TOIRCollectionDto collectionDto = new TOIRCollectionDto();
		List<EODto> eoDtoList = workService.findEODtoList(operCode);
		List<MetricDto> metricDtoList = workService.findMetricDtoList(operCode);
		List<OperationDto> operationDtoList = workService.findOperationDtoList(operCode);
		collectionDto.setInventoryItemId(workService.findInventoryItemId(operCode));
		collectionDto.setEoDtoList(eoDtoList);
		collectionDto.setMetricDtoList(metricDtoList);
		collectionDto.setOperationDtoList(operationDtoList);
		return collectionDto;
	}

	@GetMapping(value = "equipments/{id}")
	@ResponseBody
	public EquipmentDto getEquipmentById(@PathVariable("id") String id) {
		EquipmentDto result = null;
		try {
			EquipmentMapper rowMapper = new EquipmentMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM EQUIPMENT e WHERE E.ID = " + id);
			while (rs.next()) {
				result = rowMapper.mapRow(rs, rs.getRow());
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "mechanization/{id}")
	@ResponseBody
	public MechanizationDto getMechanizationById(@PathVariable("id") String id) {
		MechanizationDto result = null;
		try {
			MechanizationMapper rowMapper = new MechanizationMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM MECHANIZATION m WHERE M.ID = " + id);
			while (rs.next()) {
				result = rowMapper.mapRow(rs, rs.getRow());
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "type-works")
	@ResponseBody
	public TypeWorkDto getTypeWorkByCode(@RequestParam("code") String code) {
		TypeWorkDto result = null;
		try {
			TypeWorkMapper rowMapper = new TypeWorkMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM TYPE_WORK WHERE CODE = '" + code + "'");
			while (rs.next()) {
				result = rowMapper.mapRow(rs, rs.getRow());
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	private OrderSet fillOrderSet(OrderSet orderSet) {
		Brigade brigade = workService.findBrigadeByName(orderSet.DEPARTMENT_NAME);
		if (brigade != null) {
			orderSet.DEPARTMENT_ID = Long.valueOf(brigade.DEPARTMENT_ID);
			orderSet.ORGANIZATION_ID = Long.valueOf(brigade.ORGANIZATION_ID);
			orderSet.ORGANIZATION_NAME = brigade.ORGANIZATION_NAME;
		}
		MessageImportPlan importFromPlan = workService.findImportFromPlan(orderSet.XXBR_SET_ID);
		if (importFromPlan != null) {
			orderSet.ORDER_SET_RAW_ID = importFromPlan.ORDER_SET_ID;
		}
		LOGGER.info(orderSet.debugInfo());
		return orderSet;
	}

	@GetMapping(value = "orders/sets", params = {"from", "to"})
	@ResponseBody
	public List<OrderSet> getOrderSets(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<OrderSet> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			OrderSetRowMapper rowMapper = new OrderSetRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT \n" +
					"BP.ID AS XXBR_SET_ID, \n" +
					"NULL AS ORDER_SET_RAW_ID, \n" +
					"NULL AS ORGANIZATION_ID, \n" +
					"NULL AS ORGANIZATION_NAME,\n" +
					"BP.BEGIN_DATE AS ORDER_SET_DATE,\n" +
					"NULL AS DEPARTMENT_ID,\n" +
					"BR.NAME AS DEPARTMENT_NAME,\n" +
					"BP.CHANGE_DATE AS LAST_UPDATE_DATE,\n" +
					"UU.LASTNAME || ' ' || UU.FIRSTNAME || ' ' || UU.MIDDLENAME AS LAST_UPDATED_BY,\n" +
					"BP.CREATE_DATE AS CREATION_DATE,\n" +
					"AU.LASTNAME || ' ' || AU.FIRSTNAME || ' ' || AU.MIDDLENAME AS CREATED_BY,\n" +
					"BP.BEGIN_DATE AS START_TIME,\n" +
					"BP.END_DATE AS END_TIME \n" +
					"FROM BRIGADE_PLAN bp " +
					"LEFT JOIN USERS au ON AU.ID = BP.CREATE_BY " +
					"LEFT JOIN USERS uu ON UU.ID = BP.CHANGE_BY \n" +
					"LEFT JOIN BRIGADE br ON BR.ID = BP.BRIGADE_ID\n" +
					"WHERE BP.BEGIN_DATE BETWEEN " + between + " \n" +
					"ORDER BY BP.ID");
			while (rs.next()) {
				OrderSet orderSet = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillOrderSet(orderSet));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/sets/{id}")
	@ResponseBody
	public List<OrderSet> getOrderSets(@PathVariable("id") String id) {
		List<OrderSet> result = new ArrayList<>();
		try {
			OrderSetRowMapper rowMapper = new OrderSetRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT \n" +
					"BP.ID AS XXBR_SET_ID, \n" +
					"NULL AS ORDER_SET_RAW_ID, \n" +
					"NULL AS ORGANIZATION_ID, \n" +
					"NULL AS ORGANIZATION_NAME,\n" +
					"BP.BEGIN_DATE AS ORDER_SET_DATE,\n" +
					"NULL AS DEPARTMENT_ID,\n" +
					"BR.NAME AS DEPARTMENT_NAME,\n" +
					"BP.CHANGE_DATE AS LAST_UPDATE_DATE,\n" +
					"UU.LASTNAME || ' ' || UU.FIRSTNAME || ' ' || UU.MIDDLENAME AS LAST_UPDATED_BY,\n" +
					"BP.CREATE_DATE AS CREATION_DATE,\n" +
					"AU.LASTNAME || ' ' || AU.FIRSTNAME || ' ' || AU.MIDDLENAME AS CREATED_BY,\n" +
					"BP.BEGIN_DATE AS START_TIME,\n" +
					"BP.END_DATE AS END_TIME \n" +
					"FROM BRIGADE_PLAN bp " +
					"LEFT JOIN USERS au ON AU.ID = BP.CREATE_BY " +
					"LEFT JOIN USERS uu ON UU.ID = BP.CHANGE_BY \n" +
					"LEFT JOIN BRIGADE br ON BR.ID = BP.BRIGADE_ID\n" +
					"WHERE BP.ID = " + id);

			while (rs.next()) {
				OrderSet orderSet = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillOrderSet(orderSet));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	private OrderTask fillOrderTask(OrderTask orderTask) {
		if (!StringUtil.isNull(orderTask.REQUEST_NUMBER)) {
			orderTask.PRF_ID = template.getAddressId(orderTask.REQUEST_NUMBER);
		}
		MessageImportWork importWork = workService.findImportWorkFromId(orderTask.XXBR_WIP_ENTITY_ID);
		if (importWork != null) {
			orderTask.WIP_ENTITY_RAW_ID = importWork.WIP_ENTITY_ID;
		}
		LOGGER.info(orderTask.debugInfo());
		return orderTask;
	}

	@GetMapping(value = "orders/tasks", params = {"from", "to"})
	@ResponseBody
	public List<OrderTask> getOrderTasks(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<OrderTask> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			OrderTaskRowMapper rowMapper = new OrderTaskRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT \n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS WIP_ENTITY_RAW_ID,\n" +
					"WO.START_DATE AS START_TIME,\n" +
					"WO.FINISH_DATE AS END_TIME,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS FACT_TIME,\n" +
					"WO.DESCRIPTION AS COMMENTS,\n" +
					"WO.CHANGE_DATE AS LAST_UPDATE_DATE,\n" +
					"UU.LASTNAME || ' ' || UU.FIRSTNAME || ' ' || UU.MIDDLENAME AS LAST_UPDATED_BY,\n" +
					"WO.CREATE_DATE AS CREATION_DATE,\n" +
					"AU.LASTNAME || ' ' || AU.FIRSTNAME || ' ' || AU.MIDDLENAME AS CREATED_BY,\n" +
					"UU.LOGIN AS LAST_UPDATE_LOGIN,\n" +
					"WO.START_DATE AS M_FACT_START,\n" +
					"WO.FINISH_DATE AS M_FACT_END,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS M_FACT_TIME,\n" +
					"NULL AS VALIDATION_TEXT,\n" +
					"TW.DESCRIPTION AS ACTIVITY_NAME,\n" +
					"CASE WHEN REQ.EXTERNAL_NUMBER IS NULL THEN 'Пр' ELSE 'ГЛ' END AS DOC_SOURCE,\n" +
					"REQ.EXTERNAL_NUMBER AS REQUEST_NUMBER,\n" +
					"NULL AS PRF_ID,\n" +
					"NULL AS ADDR_ID,\n" +
					"WO.ADDRESS AS ADDR_COMMENT\n" +
					"FROM WORK_ORDER wo " +
					"INNER JOIN BRIGADE br ON WO.BRIGADE_ID = BR.ID " +
					"LEFT JOIN USERS au ON WO.AUTHOR_ID = AU.ID \n" +
					"LEFT JOIN USERS uu ON WO.CHANGE_BY = UU.ID " +
					"INNER JOIN TYPE_WORK tw ON WO.TYPE_WORK_ID = TW.ID \n" +
					"LEFT JOIN WORK_ORDER_REQUEST wor ON WO.ID = WOR.WORK_ORDER_ID " +
					"LEFT JOIN REQUEST req ON WOR.REQUEST_ID = REQ.ID\n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID");
			while (rs.next()) {
				OrderTask orderTask = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillOrderTask(orderTask));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/tasks/{id}")
	@ResponseBody
	public List<OrderTask> getOrderTasks(@PathVariable("id") String id) {
		List<OrderTask> result = new ArrayList<>();
		try {
			OrderTaskRowMapper rowMapper = new OrderTaskRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT \n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS WIP_ENTITY_RAW_ID,\n" +
					"WO.START_DATE AS START_TIME,\n" +
					"WO.FINISH_DATE AS END_TIME,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE)*24,3) AS FACT_TIME,\n" +
					"WO.DESCRIPTION AS COMMENTS,\n" +
					"WO.CHANGE_DATE AS LAST_UPDATE_DATE,\n" +
					"UU.LASTNAME || ' ' || UU.FIRSTNAME || ' ' || UU.MIDDLENAME AS LAST_UPDATED_BY,\n" +
					"WO.CREATE_DATE AS CREATION_DATE,\n" +
					"AU.LASTNAME || ' ' || AU.FIRSTNAME || ' ' || AU.MIDDLENAME AS CREATED_BY,\n" +
					"UU.LOGIN AS LAST_UPDATE_LOGIN,\n" +
					"WO.START_DATE AS M_FACT_START,\n" +
					"WO.FINISH_DATE AS M_FACT_END,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS M_FACT_TIME,\n" +
					"NULL AS VALIDATION_TEXT,\n" +
					"TW.DESCRIPTION AS ACTIVITY_NAME,\n" +
					"CASE WHEN REQ.EXTERNAL_NUMBER IS NULL THEN 'Пр' ELSE 'ГЛ' END AS DOC_SOURCE,\n" +
					"REQ.EXTERNAL_NUMBER AS REQUEST_NUMBER,\n" +
					"NULL AS PRF_ID,\n" +
					"NULL AS ADDR_ID,\n" +
					"WO.ADDRESS AS ADDR_COMMENT\n" +
					"FROM WORK_ORDER wo " +
					"INNER JOIN BRIGADE br ON WO.BRIGADE_ID = BR.ID " +
					"LEFT JOIN USERS au ON WO.AUTHOR_ID = AU.ID \n" +
					"LEFT JOIN USERS uu ON WO.CHANGE_BY = UU.ID " +
					"INNER JOIN TYPE_WORK tw ON WO.TYPE_WORK_ID = TW.ID \n" +
					"LEFT JOIN WORK_ORDER_REQUEST wor ON WO.ID = WOR.WORK_ORDER_ID " +
					"LEFT JOIN REQUEST req ON WOR.REQUEST_ID = REQ.ID\n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);

			while (rs.next()) {
				OrderTask orderTask = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillOrderTask(orderTask));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	private WOEO fillWOEO(WOEO woeo) {
		TypeElement typeElement = workService.findTypeElementByParams(woeo.EO_TYPE_DESCR, woeo.EO_LENGTH_UNIT);
		if (typeElement != null) {
			woeo.EO_TYPE = typeElement.EO_TYPE;
		}
		LOGGER.info(woeo.debugInfo());
		return woeo;
	}

	@GetMapping(value = "orders/EO", params = {"from", "to"})
	@ResponseBody
	public List<WOEO> getWorkOrderEO(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<WOEO> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			WOEORowMapper rowMapper = new WOEORowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT \n" +
					"WE.WORK_ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WE.ID AS XXBR_ELEMENT_ID,\n" +
					"NULL AS ELEMENT_RAW_ID,\n" +
					"WE.CODE AS EO_TYPE,\n" +
					"WE.TYPE AS EO_TYPE_DESCR,\n" +
					"WE.LENGTH AS EO_LENGTH,\n" +
					"WE.LENGTH_UNIT AS EO_LENGTH_UNIT,\n" +
					"WE.DIAMETER AS EO_DIAMETR,\n" +
					"WE.MATERIAL AS EO_MAT_TYPE,\n" +
					"NULL AS ADDR_STREET_ID,\n" +
					"NULL AS ADDR_BUILD_ID,\n" +
					"WO.ADDRESS AS ADDR_COMMENT\n" +
					"FROM WORK_ELEMENT we " +
					"INNER JOIN WORK_ORDER wo ON WE.WORK_ID = WO.ID\n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID");
			while (rs.next()) {
				WOEO woeo = rowMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(woeo.debugInfo());
				result.add(woeo);
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/EO/{id}")
	@ResponseBody
	public List<WOEO> getWorkOrderEO(@PathVariable("id") String id) {
		List<WOEO> result = new ArrayList<>();
		try {
			WOEORowMapper rowMapper = new WOEORowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT \n" +
					"WE.WORK_ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WE.ID AS XXBR_ELEMENT_ID,\n" +
					"NULL AS ELEMENT_RAW_ID,\n" +
					"WE.CODE AS EO_TYPE,\n" +
					"WE.TYPE AS EO_TYPE_DESCR,\n" +
					"WE.LENGTH AS EO_LENGTH,\n" +
					"WE.LENGTH_UNIT AS EO_LENGTH_UNIT,\n" +
					"WE.DIAMETER AS EO_DIAMETR,\n" +
					"WE.MATERIAL AS EO_MAT_TYPE,\n" +
					"NULL AS ADDR_STREET_ID,\n" +
					"NULL AS ADDR_BUILD_ID,\n" +
					"WO.ADDRESS AS ADDR_COMMENT\n" +
					"FROM WORK_ELEMENT we " +
					"INNER JOIN WORK_ORDER wo ON WE.WORK_ID = WO.ID\n" +
					"WHERE WO.ID = " + id);

			while (rs.next()) {
				WOEO woeo = rowMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(woeo.debugInfo());
				result.add(woeo);
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	private WORouting fillWORouting(WORouting woRouting) {
		Standart standart = workService.findStandartByName(woRouting.OPERATION_DESCRIPTION, woRouting.OPERATION_CODE, woRouting.OPER);
		if (standart != null) {
			woRouting.OPERATION_ITEM_ID = Long.valueOf(standart.INVENTORY_ITEM_ID);
			woRouting.OPERATION_SEQ_NUM = Long.valueOf(standart.OPERATION_SEQUENCE_ID);
		}
		LOGGER.info(woRouting.debugInfo());
		return woRouting;
	}

	@GetMapping(value = "orders/routing", params = {"from", "to"})
	@ResponseBody
	public List<WORouting> getWorkOrderRouting(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<WORouting> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			WORoutingRowMapper rowMapper = new WORoutingRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WI.WORK_ORDER_ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WI.\"ORDER\" AS ACTION_SEQ_ID,\n" +
					"CASE WHEN WI.FINISH_DATE IS NULL THEN '0' ELSE '1' END AS ACTIVE_FLAG,\n" +
					"NULL AS OPERATION_ITEM_ID,\n" +
					"NULL AS OPERATION_SEQ_NUM,\n" +
					"WI.CODE AS OPERATION_CODE,\n" +
					"TW.CODE AS OPER,\n" +
					"WI.DESCRIPTION AS OPERATION_DESCRIPTION\n" +
					"FROM WORK_ITEM wi\n" +
					"INNER JOIN WORK_ORDER wo ON WI.WORK_ORDER_ID = WO.ID\n" +
					"INNER JOIN TYPE_WORK tw ON WO.TYPE_WORK_ID = TW.ID\n" +
					"WHERE WI.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WI.WORK_ORDER_ID, WI.\"ORDER\"");
			while (rs.next()) {
				WORouting woRouting = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillWORouting(woRouting));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/routing/{id}")
	@ResponseBody
	public List<WORouting> getWorkOrderRouting(@PathVariable("id") String id) {
		List<WORouting> result = new ArrayList<>();
		try {
			WORoutingRowMapper rowMapper = new WORoutingRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WI.WORK_ORDER_ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WI.\"ORDER\" AS ACTION_SEQ_ID,\n" +
					"CASE WHEN WI.FINISH_DATE IS NULL THEN '0' ELSE '1' END AS ACTIVE_FLAG,\n" +
					"NULL AS OPERATION_ITEM_ID,\n" +
					"NULL AS OPERATION_SEQ_NUM,\n" +
					"WI.CODE AS OPERATION_CODE,\n" +
					"TW.CODE AS OPER,\n" +
					"WI.DESCRIPTION AS OPERATION_DESCRIPTION\n" +
					"FROM WORK_ITEM wi\n" +
					"INNER JOIN WORK_ORDER wo ON WI.WORK_ORDER_ID = WO.ID\n" +
					"INNER JOIN TYPE_WORK tw ON WO.TYPE_WORK_ID = TW.ID\n" +
					"WHERE WI.WORK_ORDER_ID = " + id + " \n" +
					"ORDER BY WI.\"ORDER\"");

			while (rs.next()) {
				WORouting woRouting = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillWORouting(woRouting));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/metrics", params = {"from", "to"})
	@ResponseBody
	public List<WOMetric> getWorkOrderMetrics(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<WOMetric> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			WOMetricRowMapper rowMapper = new WOMetricRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WM.WORK_ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WM.TYPE AS METRICS_CODE,\n" +
					"WM.QUANTITY AS M_QUANTITY\n" +
					"FROM WORK_METRIC wm\n" +
					"INNER JOIN WORK_ORDER wo ON WM.WORK_ID = WO.ID\n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID");
			while (rs.next()) {
				WOMetric woMetric = rowMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(woMetric.debugInfo());
				result.add(woMetric);
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/metrics/{id}")
	@ResponseBody
	public List<WOMetric> getWorkOrderMetrics(@PathVariable("id") String id) {
		List<WOMetric> result = new ArrayList<>();
		try {
			WOMetricRowMapper rowMapper = new WOMetricRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WM.WORK_ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WM.TYPE AS METRICS_CODE,\n" +
					"WM.QUANTITY AS M_QUANTITY\n" +
					"FROM WORK_METRIC wm\n" +
					"WHERE WM.WORK_ID = " + id);

			while (rs.next()) {
				WOMetric woMetric = rowMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(woMetric.debugInfo());
				result.add(woMetric);
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/normdet", params = {"from", "to"})
	@ResponseBody
	public List<WONormDet> getWorkOrderNormDetails(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<WONormDet> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		//WONormDetRowMapper rowMapper = new WONormDetRowMapper();
		return result;
	}

	private OrderSetRes fillEmployeeOrderSetRes(OrderSetRes orderSetRes) {
		Employee employee = workService.findEmployeeByCode(String.valueOf(orderSetRes.PERSON_ID));
		if (employee != null) {
			orderSetRes.EMPLOYEER_CODE = employee.EMPLOYEE_NUMBER;
		}
		if (!StringUtil.isNull(orderSetRes.ABSENCE_REASON)) {
			AbsenceReason absenceReason = workService.findAbsenceReasonByName(orderSetRes.ABSENCE_REASON);
			if (absenceReason != null) {
				orderSetRes.ABSENCE_COMMENT = absenceReason.FLEX_VALUE;
			}
		}
		LOGGER.info(orderSetRes.debugInfo());
		return orderSetRes;
	}

	private OrderSetRes fillTechnicOrderSetRes(OrderSetRes orderSetRes) {
		if (orderSetRes.TYPE.equalsIgnoreCase("EQUIPMENT")) {
			SpecTechnic technic = workService.findTechnicByTechCode(orderSetRes.CODE);
			if (technic != null) {
				orderSetRes.CODE_ID = Long.valueOf(technic.CODE_ID);
			}
		} else if (orderSetRes.TYPE.equalsIgnoreCase("MECHANIZATION")) {
			SmallSpecTechnic smallSpecTechnic = workService.findSmallSpecTechnicByNumberAndCode(orderSetRes.NUMBER, orderSetRes.CODE);
			if (smallSpecTechnic != null) {
				orderSetRes.CODE_ID = Long.valueOf(smallSpecTechnic.CODE_ID);
				orderSetRes.SMM_ID = Long.valueOf(smallSpecTechnic.SMM_ID);
			}
		}
		LOGGER.info(orderSetRes.debugInfo());
		return orderSetRes;
	}

	@GetMapping(value = "orders/setres", params = {"from", "to"})
	@ResponseBody
	public List<OrderSetRes> getOrderSetRes(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<OrderSetRes> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			OrderSetResRowMapper rowMapper = new OrderSetResRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"BP.ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((BP.END_DATE - BP.BEGIN_DATE)*24,3) AS PLAN_TIME,\n" +
					"ROUND((BP.END_DATE - BP.BEGIN_DATE)*24,3) AS FACT_TIME,\n" +
					"BPE.EMPLOYEE_ID AS PERSON_ID,\n" +
					"BPE.LASTNAME || ' ' || BPE.FIRSTNAME || ' ' || BPE.MIDDLENAME AS COMMENTS,\n" +
					"BPE.ABSENCE_REASON AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"NULL AS ARRIVED,\n" +
					"NULL AS DEPARTED,\n" +
					"BPE.IS_BRIGADIER AS IS_BRIGADIER,\n" +
					"NULL AS TIME_COMMENTS,\n" +
					"BP.BEGIN_DATE AS ST_TIME,\n" +
					"BP.END_DATE AS EN_TIME,\n" +
					"NULL AS TYPE,\n" +
					"NULL AS CODE,\n" +
					"NULL AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM BRIGADE_PLAN bp \n" +
					"INNER JOIN BRIGADE_PLAN_EMPLOYEE bpe ON BP.ID = BPE.BRIGADE_PLAN_ID \n" +
					"WHERE BP.BEGIN_DATE BETWEEN " + between + " \n" +
					"ORDER BY BP.ID, BPE.EMPLOYEE_ID");
			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillEmployeeOrderSetRes(orderSetRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"BP.ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((BPA.FINISH_DATE_PLAN - BPA.START_DATE_PLAN) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((BPA.FINISH_DATE_FACT - BPA.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"BPA.MODEL || ' ' || BPA.\"NUMBER\" AS COMMENTS,\n" +
					"NULL AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"BPA.START_DATE_PLAN AS ARRIVED,\n" +
					"BPA.FINISH_DATE_PLAN AS DEPARTED,\n" +
					"NULL AS IS_BRIGADIER,\n" +
					"BPA.COMMENTARY AS TIME_COMMENTS,\n" +
					"BPA.START_DATE_FACT AS ST_TIME,\n" +
					"BPA.FINISH_DATE_FACT AS EN_TIME,\n" +
					"BPA.ASSET_TYPE AS TYPE,\n" +
					"BPA.CODE AS CODE,\n" +
					"BPA.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM BRIGADE_PLAN bp \n" +
					"INNER JOIN BRIGADE_PLAN_ASSET bpa ON BP.ID = BPA.BRIGADE_PLAN_ID \n" +
					"WHERE BP.BEGIN_DATE BETWEEN " + between + " \n" +
					"AND BPA.ASSET_TYPE IN ('EQUIPMENT','MECHANIZATION') \n" +
					"ORDER BY BP.ID, BPA.ID");
			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillTechnicOrderSetRes(orderSetRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((WE.FINISH_DATE_PLAN - WE.START_DATE_PLAN) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((WE.FINISH_DATE_FACT - WE.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"WE.MODEL || ' ' || WE.\"NUMBER\" AS COMMENTS,\n" +
					"NULL AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"WE.START_DATE_PLAN AS ARRIVED,\n" +
					"WE.FINISH_DATE_PLAN AS DEPARTED,\n" +
					"NULL AS IS_BRIGADIER,\n" +
					"WE.COMMENTS AS TIME_COMMENTS,\n" +
					"WE.START_DATE_FACT AS ST_TIME,\n" +
					"WE.FINISH_DATE_FACT AS EN_TIME,\n" +
					"'EQUIPMENT' AS TYPE,\n" +
					"WE.CODE AS CODE,\n" +
					"WE.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_EQUIPMENT we ON WO.ID = WE.WORK_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID, WE.ID");
			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillTechnicOrderSetRes(orderSetRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((WM.FINISH_DATE_PLAN - WM.START_DATE_PLAN) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((WM.FINISH_DATE_FACT - WM.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"WM.MODEL || ' ' || WM.\"NUMBER\" AS COMMENTS,\n" +
					"NULL AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"WM.START_DATE_PLAN AS ARRIVED,\n" +
					"WM.FINISH_DATE_PLAN AS DEPARTED,\n" +
					"NULL AS IS_BRIGADIER,\n" +
					"WM.COMMENTS AS TIME_COMMENTS,\n" +
					"WM.START_DATE_FACT AS ST_TIME,\n" +
					"WM.FINISH_DATE_FACT AS EN_TIME,\n" +
					"'MECHANIZATION' AS TYPE,\n" +
					"WM.CODE AS CODE,\n" +
					"WM.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_MECHANIZATION wm ON WO.ID = WM.WORK_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID, WM.ID");
			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillTechnicOrderSetRes(orderSetRes));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/setres/{id}")
	@ResponseBody
	public List<OrderSetRes> getOrderSetRes(@PathVariable("id") String id) {
		List<OrderSetRes> result = new ArrayList<>();
		try {
			OrderSetResRowMapper rowMapper = new OrderSetResRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"BP.ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((BP.END_DATE - BP.BEGIN_DATE) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((BP.END_DATE - BP.BEGIN_DATE) * 24,3) AS FACT_TIME,\n" +
					"BPE.EMPLOYEE_ID AS PERSON_ID,\n" +
					"BPE.LASTNAME || ' ' || BPE.FIRSTNAME || ' ' || BPE.MIDDLENAME AS COMMENTS,\n" +
					"BPE.ABSENCE_REASON AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"NULL AS ARRIVED,\n" +
					"NULL AS DEPARTED,\n" +
					"BPE.IS_BRIGADIER AS IS_BRIGADIER,\n" +
					"NULL AS TIME_COMMENTS,\n" +
					"BP.BEGIN_DATE AS ST_TIME,\n" +
					"BP.END_DATE AS EN_TIME,\n" +
					"NULL AS TYPE,\n" +
					"NULL AS CODE,\n" +
					"NULL AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM BRIGADE_PLAN bp \n" +
					"INNER JOIN BRIGADE_PLAN_EMPLOYEE bpe ON BP.ID = BPE.BRIGADE_PLAN_ID \n" +
					"WHERE BP.ID = " + id);

			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillEmployeeOrderSetRes(orderSetRes));
			}
			rs.close();

			rs = st.executeQuery("SELECT\n" +
					"BP.ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((BPA.FINISH_DATE_PLAN - BPA.START_DATE_PLAN) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((BPA.FINISH_DATE_FACT - BPA.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"BPA.MODEL || ' ' || BPA.\"NUMBER\" AS COMMENTS,\n" +
					"NULL AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"BPA.START_DATE_PLAN AS ARRIVED,\n" +
					"BPA.FINISH_DATE_PLAN AS DEPARTED,\n" +
					"NULL AS IS_BRIGADIER,\n" +
					"BPA.COMMENTARY AS TIME_COMMENTS,\n" +
					"BPA.START_DATE_FACT AS ST_TIME,\n" +
					"BPA.FINISH_DATE_FACT AS EN_TIME,\n" +
					"BPA.ASSET_TYPE AS TYPE,\n" +
					"BPA.CODE AS CODE,\n" +
					"BPA.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM BRIGADE_PLAN bp \n" +
					"INNER JOIN BRIGADE_PLAN_ASSET bpa ON BP.ID = BPA.BRIGADE_PLAN_ID \n" +
					"WHERE BP.ID = " + id);

			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillTechnicOrderSetRes(orderSetRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((WE.FINISH_DATE_PLAN - WE.START_DATE_PLAN) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((WE.FINISH_DATE_FACT - WE.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"WE.MODEL || ' ' || WE.\"NUMBER\" AS COMMENTS,\n" +
					"NULL AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"WE.START_DATE_PLAN AS ARRIVED,\n" +
					"WE.FINISH_DATE_PLAN AS DEPARTED,\n" +
					"NULL AS IS_BRIGADIER,\n" +
					"WE.COMMENTS AS TIME_COMMENTS,\n" +
					"WE.START_DATE_FACT AS ST_TIME,\n" +
					"WE.FINISH_DATE_FACT AS EN_TIME,\n" +
					"'EQUIPMENT' AS TYPE,\n" +
					"WE.CODE AS CODE,\n" +
					"WE.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_EQUIPMENT we ON WO.ID = WE.WORK_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);
			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillTechnicOrderSetRes(orderSetRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"ROUND((WM.FINISH_DATE_PLAN - WM.START_DATE_PLAN) * 24,3) AS PLAN_TIME,\n" +
					"ROUND((WM.FINISH_DATE_FACT - WM.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"WM.MODEL || ' ' || WM.\"NUMBER\" AS COMMENTS,\n" +
					"NULL AS ABSENCE_REASON,\n" +
					"NULL AS ABSENCE_COMMENT,\n" +
					"WM.START_DATE_PLAN AS ARRIVED,\n" +
					"WM.FINISH_DATE_PLAN AS DEPARTED,\n" +
					"NULL AS IS_BRIGADIER,\n" +
					"WM.COMMENTS AS TIME_COMMENTS,\n" +
					"WM.START_DATE_FACT AS ST_TIME,\n" +
					"WM.FINISH_DATE_FACT AS EN_TIME,\n" +
					"'MECHANIZATION' AS TYPE,\n" +
					"WM.CODE AS CODE,\n" +
					"WM.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_MECHANIZATION wm ON WO.ID = WM.WORK_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);
			while (rs.next()) {
				OrderSetRes orderSetRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillTechnicOrderSetRes(orderSetRes));
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	private OrderRes fillEmployeeOrderRes(OrderRes orderRes) {
		Employee employee = workService.findEmployeeByCode(String.valueOf(orderRes.PERSON_ID));
		if (employee != null) {
			orderRes.EMPLOYEER_CODE = employee.EMPLOYEE_NUMBER;
		}
		LOGGER.info(orderRes.debugInfo());
		return orderRes;
	}

	private OrderRes fillEquipmentOrderRes(OrderRes orderRes) {
		SpecTechnic technic = workService.findTechnicByTechCode(orderRes.CODE);
		if (technic != null) {
			orderRes.CODE_ID = Long.valueOf(technic.CODE_ID);
		}
		LOGGER.info(orderRes.debugInfo());
		return orderRes;
	}

	private OrderRes fillMechanizationOrderRes(OrderRes orderRes) {
		SmallSpecTechnic smallSpecTechnic = workService.findSmallSpecTechnicByNumberAndCode(orderRes.NUMBER, orderRes.CODE);
		if (smallSpecTechnic != null) {
			orderRes.CODE_ID = Long.valueOf(smallSpecTechnic.CODE_ID);
			orderRes.SMM_ID = Long.valueOf(smallSpecTechnic.SMM_ID);
		}
		LOGGER.info(orderRes.debugInfo());
		return orderRes;
	}

	@GetMapping(value = "orders/res", params = {"from", "to"})
	@ResponseBody
	public List<OrderRes> getOrderRes(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<OrderRes> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			OrderResRowMapper rowMapper = new OrderResRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WO.START_DATE AS START_TIME,\n" +
					"WO.FINISH_DATE AS END_TIME,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS FACT_TIME,\n" +
					"BPE.EMPLOYEE_ID AS PERSON_ID,\n" +
					"'EMPLOYEE' AS TYPE,\n" +
					"NULL AS CODE,\n" +
					"NULL AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN BRIGADE_PLAN_EMPLOYEE bpe ON WO.BRIGADE_PLAN_ID = BPE.BRIGADE_PLAN_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID, BPE.EMPLOYEE_ID");
			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillEmployeeOrderRes(orderRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WE.START_DATE_FACT AS START_TIME,\n" +
					"WE.FINISH_DATE_FACT AS END_TIME,\n" +
					"ROUND((WE.FINISH_DATE_FACT - WE.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"'EQUIPMENT' AS TYPE,\n" +
					"WE.CODE AS CODE,\n" +
					"WE.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_EQUIPMENT we ON WO.ID = WE.WORK_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID, WE.ID");
			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillEquipmentOrderRes(orderRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WM.START_DATE_FACT AS START_TIME,\n" +
					"WM.FINISH_DATE_FACT AS END_TIME,\n" +
					"ROUND((WM.FINISH_DATE_FACT - WM.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"'MECHANIZATION' AS TYPE,\n" +
					"WM.CODE AS CODE,\n" +
					"WM.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID \n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_MECHANIZATION wm ON WO.ID = WM.WORK_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID, WM.ID");
			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillMechanizationOrderRes(orderRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WO.START_DATE AS START_TIME,\n" +
					"WO.FINISH_DATE AS END_TIME,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"BPA.ASSET_TYPE AS TYPE,\n" +
					"BPA.CODE AS CODE,\n" +
					"BPA.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID \n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN BRIGADE_PLAN_ASSET bpa ON WO.BRIGADE_PLAN_ID = BPA.BRIGADE_PLAN_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"AND BPA.ASSET_TYPE IN ('EQUIPMENT','MECHANIZATION') \n" +
					"ORDER BY WO.ID, BPA.ID");

			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				if (orderRes.TYPE.equalsIgnoreCase("MECHANIZATION")) {
					result.add(fillMechanizationOrderRes(orderRes));
				} else if (orderRes.TYPE.equalsIgnoreCase("EQUIPMENT")) {
					result.add(fillEquipmentOrderRes(orderRes));
				}
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/res/{id}")
	@ResponseBody
	public List<OrderRes> getOrderRes(@PathVariable("id") String id) {
		List<OrderRes> result = new ArrayList<>();
		try {
			OrderResRowMapper rowMapper = new OrderResRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WO.START_DATE AS START_TIME,\n" +
					"WO.FINISH_DATE AS END_TIME,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS FACT_TIME,\n" +
					"BPE.EMPLOYEE_ID AS PERSON_ID,\n" +
					"'EMPLOYEE' AS TYPE,\n" +
					"NULL AS CODE,\n" +
					"NULL AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN BRIGADE_PLAN_EMPLOYEE bpe ON WO.BRIGADE_PLAN_ID = BPE.BRIGADE_PLAN_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);

			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillEmployeeOrderRes(orderRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WE.START_DATE_FACT AS START_TIME,\n" +
					"WE.FINISH_DATE_FACT AS END_TIME,\n" +
					"ROUND((WE.FINISH_DATE_FACT - WE.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"'EQUIPMENT' AS TYPE,\n" +
					"WE.CODE AS CODE,\n" +
					"WE.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_EQUIPMENT we ON WO.ID = WE.WORK_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);

			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillEquipmentOrderRes(orderRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WM.START_DATE_FACT AS START_TIME,\n" +
					"WM.FINISH_DATE_FACT AS END_TIME,\n" +
					"ROUND((WM.FINISH_DATE_FACT - WM.START_DATE_FACT) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"'MECHANIZATION' AS TYPE,\n" +
					"WM.CODE AS CODE,\n" +
					"WM.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_MECHANIZATION wm ON WO.ID = WM.WORK_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);

			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				result.add(fillMechanizationOrderRes(orderRes));
			}

			rs.close();
			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"NULL AS EMPLOYEER_CODE,\n" +
					"WO.START_DATE AS START_TIME,\n" +
					"WO.FINISH_DATE AS END_TIME,\n" +
					"ROUND((WO.FINISH_DATE - WO.START_DATE) * 24,3) AS FACT_TIME,\n" +
					"NULL AS PERSON_ID,\n" +
					"BPA.ASSET_TYPE AS TYPE,\n" +
					"BPA.CODE AS CODE,\n" +
					"BPA.\"NUMBER\" AS \"NUMBER\",\n" +
					"NULL AS SMM_ID,\n" +
					"NULL AS CODE_ID\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN BRIGADE_PLAN_ASSET bpa ON WO.BRIGADE_PLAN_ID = BPA.BRIGADE_PLAN_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id + " \n" +
					"AND BPA.ASSET_TYPE IN ('EQUIPMENT','MECHANIZATION') \n");

			while (rs.next()) {
				OrderRes orderRes = rowMapper.mapRow(rs, rs.getRow());
				if (orderRes.TYPE.equalsIgnoreCase("EQUIPMENT")) {
					result.add(fillEquipmentOrderRes(orderRes));
				} else if (orderRes.TYPE.equalsIgnoreCase("MECHANIZATION")) {
					result.add(fillMechanizationOrderRes(orderRes));
				}
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/materials", params = {"from", "to"})
	@ResponseBody
	public List<WOMaterial> getWorkOrderMaterials(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<WOMaterial> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		try {
			WOMaterialRowMapper rowMapper = new WOMaterialRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"BPA.\"TYPE\" AS XXBR_MATERIAL_NAME,\n" +
					"BPA.UNIT AS XXBR_MATERIAL_UOM,\n" +
					"BPA.QUANTITY AS XXBR_QUANTITY\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN BRIGADE_PLAN_ASSET bpa ON WO.BRIGADE_PLAN_ID = BPA.BRIGADE_PLAN_ID \n" +
					"WHERE BPA.ASSET_TYPE = 'INVENTORY' AND BP.BEGIN_DATE BETWEEN " + between + " \n" +
					"ORDER BY WO.BRIGADE_PLAN_ID, BPA.ID");
			while (rs.next()) {
				WOMaterial material = rowMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(material.debugInfo());
				result.add(material);
			}
			rs.close();

			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WI.\"TYPE\" AS XXBR_MATERIAL_NAME,\n" +
					"WI.UNIT AS XXBR_MATERIAL_UOM,\n" +
					"WI.QUANTITY AS XXBR_QUANTITY\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_INVENTORY wi ON WO.ID = WI.WORK_ID \n" +
					"WHERE WO.START_DATE_PLAN BETWEEN " + between + " \n" +
					"ORDER BY WO.ID, WI.ID");
			while (rs.next()) {
				WOMaterial material = rowMapper.mapRow(rs, rs.getRow());
				result.add(material);
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/materials/{id}")
	@ResponseBody
	public List<WOMaterial> getWorkOrderMaterials(@PathVariable("id") String id) {
		List<WOMaterial> result = new ArrayList<>();
		try {
			WOMaterialRowMapper rowMapper = new WOMaterialRowMapper();
			Connection conn = dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"BPA.\"TYPE\" AS XXBR_MATERIAL_NAME,\n" +
					"BPA.UNIT AS XXBR_MATERIAL_UOM,\n" +
					"BPA.QUANTITY AS XXBR_QUANTITY\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN BRIGADE_PLAN_ASSET bpa ON WO.BRIGADE_PLAN_ID = BPA.BRIGADE_PLAN_ID \n" +
					"WHERE BPA.ASSET_TYPE = 'INVENTORY' AND WO.BRIGADE_PLAN_ID = " + id);
			while (rs.next()) {
				WOMaterial material = rowMapper.mapRow(rs, rs.getRow());
				result.add(material);
			}
			rs.close();

			rs = st.executeQuery("SELECT\n" +
					"WO.BRIGADE_PLAN_ID AS XXBR_SET_ID,\n" +
					"WO.ID AS XXBR_WIP_ENTITY_ID,\n" +
					"WI.\"TYPE\" AS XXBR_MATERIAL_NAME,\n" +
					"WI.UNIT AS XXBR_MATERIAL_UOM,\n" +
					"WI.QUANTITY AS XXBR_QUANTITY\n" +
					"FROM WORK_ORDER wo \n" +
					"INNER JOIN WORK_INVENTORY wi ON WO.ID = WI.WORK_ID \n" +
					"WHERE WO.BRIGADE_PLAN_ID = " + id);
			while (rs.next()) {
				WOMaterial material = rowMapper.mapRow(rs, rs.getRow());
				LOGGER.debug(material.debugInfo());
				result.add(material);
			}

			rs.close();
			st.close();
			conn.close();
		} catch (SQLException e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
		return result;
	}

	@GetMapping(value = "orders/loadtrip", params = {"from", "to"})
	@ResponseBody
	public List<WOLoadTrip> getWorkOrderLoadTrips(
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT) LocalDateTime to) {
		List<WOLoadTrip> result = new ArrayList<>();
		String between = DatesUtil.getSqlBetween(from, to);
		if (between == null) return result;
		//WOLoadTripRowMapper rowMapper = new WOLoadTripRowMapper();
		return result;
	}
}
