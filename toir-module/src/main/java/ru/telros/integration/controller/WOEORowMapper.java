package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.WOEO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WOEORowMapper implements RowMapper<WOEO> {
	@Override
	public WOEO mapRow(ResultSet resultSet, int i) throws SQLException {
		WOEO woeo = new WOEO();
		woeo.XXBR_WIP_ENTITY_ID = resultSet.getLong("XXBR_WIP_ENTITY_ID");
		woeo.XXBR_ELEMENT_ID = resultSet.getLong("XXBR_ELEMENT_ID");
		woeo.ELEMENT_RAW_ID = resultSet.getLong("ELEMENT_RAW_ID");
		woeo.EO_TYPE = StringUtil.returnString(resultSet.getString("EO_TYPE"));
		woeo.EO_TYPE_DESCR = StringUtil.returnString(resultSet.getString("EO_TYPE_DESCR"));
		woeo.EO_LENGTH = StringUtil.returnString(resultSet.getString("EO_LENGTH"));
		woeo.EO_LENGTH_UNIT = StringUtil.returnString(resultSet.getString("EO_LENGTH_UNIT"));
		woeo.EO_DIAMETR = StringUtil.returnString(resultSet.getString("EO_DIAMETR"));
		woeo.EO_MAT_TYPE = StringUtil.returnString(resultSet.getString("EO_MAT_TYPE"));
		woeo.ADDR_STREET_ID = StringUtil.returnString(resultSet.getString("ADDR_STREET_ID"));
		woeo.ADDR_BUILD_ID = StringUtil.returnString(resultSet.getString("ADDR_BUILD_ID"));
		woeo.ADDR_COMMENT = StringUtil.returnString(resultSet.getString("ADDR_COMMENT"));
		return woeo;
	}
}
