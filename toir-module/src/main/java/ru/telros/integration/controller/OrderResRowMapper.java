package ru.telros.integration.controller;

import org.springframework.jdbc.core.RowMapper;
import ru.telros.integration.DatesUtil;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.OrderRes;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderResRowMapper implements RowMapper<OrderRes> {
	@Override
	public OrderRes mapRow(ResultSet resultSet, int i) throws SQLException {
		OrderRes orderRes = new OrderRes();
		orderRes.XXBR_SET_ID = resultSet.getLong("XXBR_SET_ID");
		orderRes.XXBR_WIP_ENTITY_ID = resultSet.getLong("XXBR_WIP_ENTITY_ID");
		orderRes.EMPLOYEER_CODE = StringUtil.returnString(resultSet.getString("EMPLOYEER_CODE"));
		orderRes.START_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("START_TIME"));
		orderRes.END_TIME = DatesUtil.timestampToString(resultSet.getTimestamp("END_TIME"));
		orderRes.FACT_TIME = StringUtil.returnString(resultSet.getString("FACT_TIME"));
		orderRes.PERSON_ID = resultSet.getLong("PERSON_ID");
		orderRes.TYPE = StringUtil.returnString(resultSet.getString("TYPE"));
		orderRes.NUMBER = StringUtil.returnString(resultSet.getString("NUMBER"));
		orderRes.CODE = StringUtil.returnString(resultSet.getString("CODE"));
		orderRes.SMM_ID = resultSet.getLong("SMM_ID");
		orderRes.CODE_ID = resultSet.getLong("CODE_ID");
		return orderRes;
	}
}
