package ru.telros.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StringUtil {
	public static Boolean isNull(String s) {
		return s == null || s.trim().isEmpty() || s.equalsIgnoreCase("null");
	}

	public static String writeValueAsString(Object value) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String returnString(String value) {
		//String.valueOf() returns "null", that's bad
		return value == null ? "" : value;
	}
}
