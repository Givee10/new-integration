package ru.telros.integration.elements.toircalc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCalcNormResponse", namespace="urn:xmethods-delayed-wcnorm")
public class GetCalcNormResponse extends AbstractResponse {
}
