package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getBrigadesResponse", namespace="urn:xmethods-delayed-brigades")
public class BrigadesResponse extends AbstractResponse {
}
