package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkEos", namespace="urn:xmethods-delayed-workeos")
public class WorkEosRequest extends AbstractRequest {
}
