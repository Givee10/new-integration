package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzTechnicsResponse", namespace="urn:xmethods-delayed-nztechs")
public class NzTechnicsResponse extends AbstractResponse {
}
