package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getSmallTechnicTypesResponse", namespace="urn:xmethods-delayed-smtechtypes")
public class SmallTechnicTypesResponse extends AbstractResponse {
}
