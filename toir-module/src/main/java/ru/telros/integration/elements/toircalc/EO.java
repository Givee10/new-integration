package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EO", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
    "pWipEntityId",
    "pEoType",
    "pEoLength",
    "pEoDiametr",
    "pEoMatType"
})
public class EO {
    @XmlElement(name = "p_wip_entity_id")
    protected String pWipEntityId;
    @XmlElement(name = "p_eo_type")
    protected String pEoType;
    @XmlElement(name = "p_eo_length")
    protected String pEoLength;
    @XmlElement(name = "p_eo_diametr")
    protected String pEoDiametr;
    @XmlElement(name = "p_eo_mat_type")
    protected String pEoMatType;

    public String getpWipEntityId() {
        return pWipEntityId;
    }

    public void setpWipEntityId(String pWipEntityId) {
        this.pWipEntityId = pWipEntityId;
    }

    public String getpEoType() {
        return pEoType;
    }

    public void setpEoType(String pEoType) {
        this.pEoType = pEoType;
    }

    public String getpEoLength() {
        return pEoLength;
    }

    public void setpEoLength(String pEoLength) {
        this.pEoLength = pEoLength;
    }

    public String getpEoDiametr() {
        return pEoDiametr;
    }

    public void setpEoDiametr(String pEoDiametr) {
        this.pEoDiametr = pEoDiametr;
    }

    public String getpEoMatType() {
        return pEoMatType;
    }

    public void setpEoMatType(String pEoMatType) {
        this.pEoMatType = pEoMatType;
    }
}
