package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getAbsenceReasonsResponse", namespace="urn:xmethods-delayed-absreasons")
public class AbsenceReasonsResponse extends AbstractResponse {
}
