package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTypeElements", namespace="urn:xmethods-delayed-tpelements")
public class TypeElementsRequest extends AbstractRequest {
}
