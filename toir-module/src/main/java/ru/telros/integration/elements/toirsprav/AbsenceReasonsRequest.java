package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getAbsenceReasons", namespace="urn:xmethods-delayed-absreasons")
public class AbsenceReasonsRequest extends AbstractRequest {
}
