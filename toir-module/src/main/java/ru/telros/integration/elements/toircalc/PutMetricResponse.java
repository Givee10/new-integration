package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putMetricResponse", namespace="urn:xmethods-delayed-wmetric")
public class PutMetricResponse extends AbstractPutResponse {
}
