package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putE0Response", namespace="urn:xmethods-delayed-weo")
public class PutEOResponse extends AbstractPutResponse {
}
