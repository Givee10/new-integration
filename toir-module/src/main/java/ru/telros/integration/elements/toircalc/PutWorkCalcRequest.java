package ru.telros.integration.elements.toircalc;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putWorkCalc", namespace="urn:xmethods-delayed-wworkcalc")
public class PutWorkCalcRequest extends AbstractRequest {
}
