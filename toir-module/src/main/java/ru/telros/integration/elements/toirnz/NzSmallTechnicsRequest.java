package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzSmallTechnics", namespace="urn:xmethods-delayed-nzsmtechs")
public class NzSmallTechnicsRequest extends AbstractRequest {
}
