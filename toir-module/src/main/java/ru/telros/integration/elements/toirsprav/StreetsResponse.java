package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStreetsResponse", namespace="urn:xmethods-delayed-streets")
public class StreetsResponse extends AbstractResponse {
}
