package ru.telros.integration.elements.toirsprav;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class ToirRefClient extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(ToirRefClient.class);

    public AbsenceReasonsResponse getAbsenceReasons() {
        AbsenceReasonsRequest request = new AbsenceReasonsRequest();
        request.setId("0");

        log.info("Requesting All Absence Reasons");
        return (AbsenceReasonsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public AddressResponse getAddresses() {
        AddressRequest request = new AddressRequest();
        request.setId("0");

        log.info("Requesting All Addresses");
        return (AddressResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public BrigadesResponse getBrigades() {
        BrigadesRequest request = new BrigadesRequest();
        request.setId("0");

        log.info("Requesting All Brigades");
        return (BrigadesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public DeviationReasonsResponse getDeviationReasons() {
        DeviationReasonsRequest request = new DeviationReasonsRequest();
        request.setId("0");

        log.info("Requesting All Deviation Reasons");
        return (DeviationReasonsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public EmployeersResponse getEmployees() {
        EmployeersRequest request = new EmployeersRequest();
        request.setId("0");

        log.info("Requesting All Employees");
        return (EmployeersResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public MetricsResponse getMetrics() {
        MetricsRequest request = new MetricsRequest();
        request.setId("0");

        log.info("Requesting All Metrics");
        return (MetricsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public ResourcesResponse getResources() {
        ResourcesRequest request = new ResourcesRequest();
        request.setId("0");

        log.info("Requesting All Resources");
        return (ResourcesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public SmallSpecTechnicTypesResponse getSmallSpecTechnicTypes() {
        SmallSpecTechnicTypesRequest request = new SmallSpecTechnicTypesRequest();
        request.setId("0");

        log.info("Requesting All Small Spec Technical Types");
        return (SmallSpecTechnicTypesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public SmallTechnicTypesResponse getSmallTechnicTypes() {
        SmallTechnicTypesRequest request = new SmallTechnicTypesRequest();
        request.setId("0");

        log.info("Requesting All Small Technical Types");
        return (SmallTechnicTypesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public SourceResponse getSources() {
        SourceRequest request = new SourceRequest();
        request.setId("0");

        log.info("Requesting All Sources");
        return (SourceResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public SpecTechnicTypesResponse getSpecTechnicTypes() {
        SpecTechnicTypesRequest request = new SpecTechnicTypesRequest();
        request.setId("0");

        log.info("Requesting All Spec Technical Types");
        return (SpecTechnicTypesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StandartsResponse getStandarts() {
        StandartsRequest request = new StandartsRequest();
        request.setId("0");

        log.info("Requesting All Standards");
        return (StandartsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StreetsResponse getStreets() {
        StreetsRequest request = new StreetsRequest();
        request.setId("0");

        log.info("Requesting All Streets");
        return (StreetsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StructureBrigadesResponse getStructureBrigades() {
        StructureBrigadesRequest request = new StructureBrigadesRequest();
        request.setId("0");

        log.info("Requesting All Structure Brigades");
        return (StructureBrigadesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public TechnicTypesResponse getTechnicTypes() {
        TechnicTypesRequest request = new TechnicTypesRequest();
        request.setId("0");

        log.info("Requesting All Technicals Types");
        return (TechnicTypesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public TypeElementsResponse getTypeElements() {
        TypeElementsRequest request = new TypeElementsRequest();
        request.setId("0");

        log.info("Requesting All Type Elements");
        return (TypeElementsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public TypeElementsWorkResponse getTypeElementsWorks() {
        TypeElementsWorkRequest request = new TypeElementsWorkRequest();
        request.setId("0");

        log.info("Requesting All Type Elements Work");
        return (TypeElementsWorkResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public TypesWorksResponse getTypesWorks() {
        TypesWorksRequest request = new TypesWorksRequest();
        request.setId("0");

        log.info("Requesting All Types Works");
        return (TypesWorksResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public ZonesResponse getZones() {
        ZonesRequest request = new ZonesRequest();
        request.setId("0");

        log.info("Requesting All Zones");
        return (ZonesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
