package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putOprcResponse", namespace="urn:xmethods-delayed-woprc")
public class PutOprcResponse extends AbstractPutResponse {
}
