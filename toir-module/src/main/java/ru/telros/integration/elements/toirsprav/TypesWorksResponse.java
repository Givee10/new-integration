package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTypesWorksResponse", namespace="urn:xmethods-delayed-tpworks")
public class TypesWorksResponse extends AbstractResponse {
}
