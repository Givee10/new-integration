package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStructureBrigadesResponse", namespace="urn:xmethods-delayed-strbrigades")
public class StructureBrigadesResponse extends AbstractResponse {
}
