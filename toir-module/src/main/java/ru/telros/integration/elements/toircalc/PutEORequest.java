package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putE0", namespace="urn:xmethods-delayed-weo")
public class PutEORequest {
	@XmlElement(name = "InputData", required = true)
	protected EO InputData;

	public EO getInputData() {
		return InputData;
	}

	public void setInputData(EO inputData) {
		InputData = inputData;
	}
}
