package ru.telros.integration.elements.toircalc;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCalcNorm", namespace="urn:xmethods-delayed-wcnorm")
public class GetCalcNormRequest extends AbstractRequest {
}
