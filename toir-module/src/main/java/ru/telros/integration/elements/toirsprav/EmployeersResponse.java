package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getEmployeersResponse", namespace="urn:xmethods-delayed-emps")
public class EmployeersResponse extends AbstractResponse {
}
