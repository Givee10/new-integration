package ru.telros.integration.elements.toircalc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCalcNormDetailResponse", namespace="urn:xmethods-delayed-wcnormdet")
public class GetCalcNormDetailResponse extends AbstractResponse {
}
