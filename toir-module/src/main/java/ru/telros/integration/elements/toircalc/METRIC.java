package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "METRIC", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
    "pWipEntityId",
    "pMetricsCode",
    "pmQuantity"
})
public class METRIC {
    @XmlElement(name = "p_wip_entity_id")
    protected String pWipEntityId;
    @XmlElement(name = "p_metrics_code")
    protected String pMetricsCode;
    @XmlElement(name = "p_m_quantity")
    protected String pmQuantity;

    public String getpWipEntityId() {
        return pWipEntityId;
    }

    public void setpWipEntityId(String pWipEntityId) {
        this.pWipEntityId = pWipEntityId;
    }

    public String getpMetricsCode() {
        return pMetricsCode;
    }

    public void setpMetricsCode(String pMetricsCode) {
        this.pMetricsCode = pMetricsCode;
    }

    public String getPmQuantity() {
        return pmQuantity;
    }

    public void setPmQuantity(String pmQuantity) {
        this.pmQuantity = pmQuantity;
    }
}
