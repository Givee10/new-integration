package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTechnicTypes", namespace="urn:xmethods-delayed-techtypes")
public class TechnicTypesRequest extends AbstractRequest {
}
