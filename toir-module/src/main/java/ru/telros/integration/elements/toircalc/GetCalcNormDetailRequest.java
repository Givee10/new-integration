package ru.telros.integration.elements.toircalc;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCalcNormDetail", namespace="urn:xmethods-delayed-wcnormdet")
public class GetCalcNormDetailRequest extends AbstractRequest {
}
