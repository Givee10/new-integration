package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getZonesResponse", namespace="urn:xmethods-delayed-zones")
public class ZonesResponse extends AbstractResponse {
}
