package ru.telros.integration.elements.toirnz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class ToirClient extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(ToirClient.class);

    public WorkNormsResponse getWorkNorms() {
        WorkNormsRequest request = new WorkNormsRequest();
        request.setId("0");

        log.info("Requesting All Work Norms");
        return (WorkNormsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public WorkMetricsResponse getWorkMetrics() {
        WorkMetricsRequest request = new WorkMetricsRequest();
        request.setId("0");

        log.info("Requesting All Work Metrics");
        return (WorkMetricsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public WorkMapsResponse getWorkMaps() {
        WorkMapsRequest request = new WorkMapsRequest();
        request.setId("0");

        log.info("Requesting All Work Maps");
        return (WorkMapsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public WorkHeadersResponse getWorkHeaders() {
        WorkHeadersRequest request = new WorkHeadersRequest();
        request.setId("0");

        log.info("Requesting All Work Headers");
        return (WorkHeadersResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public WorkEosResponse getWorkEos() {
        WorkEosRequest request = new WorkEosRequest();
        request.setId("0");

        log.info("Requesting All Work Eos");
        return (WorkEosResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public WorkDetNormsResponse getWorkDetNorms() {
        WorkDetNormsRequest request = new WorkDetNormsRequest();
        request.setId("0");

        log.info("Requesting All Work Det Norms");
        return (WorkDetNormsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public NzTechnicsResponse getNzTechnics() {
        NzTechnicsRequest request = new NzTechnicsRequest();
        request.setId("0");

        log.info("Requesting All Nz Technics");
        return (NzTechnicsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public NzSmallTechnicsResponse getNzSmallTechnics() {
        NzSmallTechnicsRequest request = new NzSmallTechnicsRequest();
        request.setId("0");

        log.info("Requesting All Nz Small Technics");
        return (NzSmallTechnicsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public NzHeadersResponse getNzHeaders() {
        NzHeadersRequest request = new NzHeadersRequest();
        request.setId("0");

        log.info("Requesting All Nz Headers");
        return (NzHeadersResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public NzEmployeersResponse getNzEmployees() {
        NzEmployeersRequest request = new NzEmployeersRequest();
        request.setId("0");

        log.info("Requesting All Nz Employees");
        return (NzEmployeersResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
