package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getAddressResponse", namespace="urn:xmethods-delayed-addresses")
public class AddressResponse extends AbstractResponse {
}
