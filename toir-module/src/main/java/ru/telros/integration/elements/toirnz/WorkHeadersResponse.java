package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkHeadersResponse", namespace="urn:xmethods-delayed-wheaders")
public class WorkHeadersResponse extends AbstractResponse {
}
