package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkMetrics", namespace="urn:xmethods-delayed-wmetrics")
public class WorkMetricsRequest extends AbstractRequest {
}
