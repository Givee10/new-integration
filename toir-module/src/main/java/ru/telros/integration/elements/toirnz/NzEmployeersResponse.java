package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzEmployeersResponse", namespace="urn:xmethods-delayed-nzemps")
public class NzEmployeersResponse extends AbstractResponse {
}
