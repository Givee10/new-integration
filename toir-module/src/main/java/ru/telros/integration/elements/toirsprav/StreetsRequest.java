package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStreets", namespace="urn:xmethods-delayed-streets")
public class StreetsRequest extends AbstractRequest {
}
