package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getAddress", namespace="urn:xmethods-delayed-addresses")
public class AddressRequest extends AbstractRequest {
}
