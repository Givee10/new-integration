package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkEosResponse", namespace="urn:xmethods-delayed-workeos")
public class WorkEosResponse extends AbstractResponse {
}
