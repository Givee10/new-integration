package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putOprc", namespace="urn:xmethods-delayed-woprc")
public class PutOprcRequest {
	@XmlElement(name = "InputData", required = true)
	protected OPRC InputData;

	public OPRC getInputData() {
		return InputData;
	}

	public void setInputData(OPRC inputData) {
		InputData = inputData;
	}
}
