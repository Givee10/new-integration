package ru.telros.integration.elements;

import ru.telros.integration.elements.toircalc.*;
import ru.telros.integration.elements.toirnz.*;
import ru.telros.integration.elements.toirsprav.*;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {
	public ObjectFactory() {
	}

	/* toirnz */

	public NzEmployeersRequest createNzEmployeersRequest() {
		return new NzEmployeersRequest();
	}

	public NzEmployeersResponse createNzEmployeersResponse() {
		return new NzEmployeersResponse();
	}

	public NzHeadersRequest createNzHeadersRequest() {
		return new NzHeadersRequest();
	}

	public NzHeadersResponse createNzHeadersResponse() {
		return new NzHeadersResponse();
	}

	public NzSmallTechnicsRequest createNzSmallTechnicsRequest() {
		return new NzSmallTechnicsRequest();
	}

	public NzSmallTechnicsResponse createNzSmallTechnicsResponse() {
		return new NzSmallTechnicsResponse();
	}

	public NzTechnicsRequest createNzTechnicsRequest() {
		return new NzTechnicsRequest();
	}

	public NzTechnicsResponse createNzTechnicsResponse() {
		return new NzTechnicsResponse();
	}

	public WorkDetNormsRequest createWorkDetNormsRequest() {
		return new WorkDetNormsRequest();
	}

	public WorkDetNormsResponse createWorkDetNormsResponse() {
		return new WorkDetNormsResponse();
	}

	public WorkEosRequest createWorkEosRequest() {
		return new WorkEosRequest();
	}

	public WorkEosResponse createWorkEosResponse() {
		return new WorkEosResponse();
	}

	public WorkHeadersRequest createWorkHeadersRequest() {
		return new WorkHeadersRequest();
	}

	public WorkHeadersResponse createWorkHeadersResponse() {
		return new WorkHeadersResponse();
	}

	public WorkMapsRequest createWorkMapsRequest() {
		return new WorkMapsRequest();
	}

	public WorkMapsResponse createWorkMapsResponse() {
		return new WorkMapsResponse();
	}

	public WorkMetricsRequest createWorkMetricsRequest() {
		return new WorkMetricsRequest();
	}

	public WorkMetricsResponse createWorkMetricsResponse() {
		return new WorkMetricsResponse();
	}

	public WorkNormsRequest createWorkNormsRequest() {
		return new WorkNormsRequest();
	}

	public WorkNormsResponse createWorkNormsResponse() {
		return new WorkNormsResponse();
	}

	/* toirsprav */

	public AbsenceReasonsRequest createAbsenceReasonsRequest() {
		return new AbsenceReasonsRequest();
	}

	public AbsenceReasonsResponse createAbsenceReasonsResponse() {
		return new AbsenceReasonsResponse();
	}

	public AddressRequest createAddressRequest() {
		return new AddressRequest();
	}

	public AddressResponse createAddressResponse() {
		return new AddressResponse();
	}

	public BrigadesRequest createBrigadesRequest() {
		return new BrigadesRequest();
	}

	public BrigadesResponse createBrigadesResponse() {
		return new BrigadesResponse();
	}

	public DeviationReasonsRequest createDeviationReasonsRequest() {
		return new DeviationReasonsRequest();
	}

	public DeviationReasonsResponse createDeviationReasonsResponse() {
		return new DeviationReasonsResponse();
	}

	public EmployeersRequest createEmployeersRequest() {
		return new EmployeersRequest();
	}

	public EmployeersResponse createEmployeersResponse() {
		return new EmployeersResponse();
	}

	public MetricsRequest createMetricsRequest() {
		return new MetricsRequest();
	}

	public MetricsResponse createMetricsResponse() {
		return new MetricsResponse();
	}

	public ResourcesRequest createResourcesRequest() {
		return new ResourcesRequest();
	}

	public ResourcesResponse createResourcesResponse() {
		return new ResourcesResponse();
	}

	public SmallSpecTechnicTypesRequest createSmallSpecTechnicTypesRequest() {
		return new SmallSpecTechnicTypesRequest();
	}

	public SmallSpecTechnicTypesResponse createSmallSpecTechnicTypesResponse() {
		return new SmallSpecTechnicTypesResponse();
	}

	public SmallTechnicTypesRequest createSmallTechnicTypesRequest() {
		return new SmallTechnicTypesRequest();
	}

	public SmallTechnicTypesResponse createSmallTechnicTypesResponse() {
		return new SmallTechnicTypesResponse();
	}

	public SourceRequest createSourceRequest() {
		return new SourceRequest();
	}

	public SourceResponse createSourceResponse() {
		return new SourceResponse();
	}

	public SpecTechnicTypesRequest createSpecTechnicTypesRequest() {
		return new SpecTechnicTypesRequest();
	}

	public SpecTechnicTypesResponse createSpecTechnicTypesResponse() {
		return new SpecTechnicTypesResponse();
	}

	public StandartsRequest createStandartsRequest() {
		return new StandartsRequest();
	}

	public StandartsResponse createStandartsResponse() {
		return new StandartsResponse();
	}

	public StreetsRequest createStreetsRequest() {
		return new StreetsRequest();
	}

	public StreetsResponse createStreetsResponse() {
		return new StreetsResponse();
	}

	public StructureBrigadesRequest createStructureBrigadesRequest() {
		return new StructureBrigadesRequest();
	}

	public StructureBrigadesResponse createStructureBrigadesResponse() {
		return new StructureBrigadesResponse();
	}

	public TechnicTypesRequest createTechnicTypesRequest() {
		return new TechnicTypesRequest();
	}

	public TechnicTypesResponse createTechnicTypesResponse() {
		return new TechnicTypesResponse();
	}

	public TypeElementsRequest createTypeElementsRequest() {
		return new TypeElementsRequest();
	}

	public TypeElementsResponse createTypeElementsResponse() {
		return new TypeElementsResponse();
	}

	public TypeElementsWorkRequest createTypeElementsWorkRequest() {
		return new TypeElementsWorkRequest();
	}

	public TypeElementsWorkResponse createTypeElementsWorkResponse() {
		return new TypeElementsWorkResponse();
	}

	public TypesWorksRequest createTypesWorksRequest() {
		return new TypesWorksRequest();
	}

	public TypesWorksResponse createTypesWorksResponse() {
		return new TypesWorksResponse();
	}

	public ZonesRequest createZonesRequest() {
		return new ZonesRequest();
	}

	public ZonesResponse createZonesResponse() {
		return new ZonesResponse();
	}

	/* toircalc */

	public GetCalcNormRequest createGetCalcNormRequest() {
		return new GetCalcNormRequest();
	}

	public GetCalcNormResponse createGetCalcNormResponse() {
		return new GetCalcNormResponse();
	}

	public GetCalcNormDetailRequest createGetCalcNormDetailRequest() {
		return new GetCalcNormDetailRequest();
	}

	public GetCalcNormDetailResponse createGetCalcNormDetailResponse() {
		return new GetCalcNormDetailResponse();
	}

	public PutEORequest createPutEORequest() {
		return new PutEORequest();
	}

	public PutEOResponse createPutEOResponse() {
		return new PutEOResponse();
	}

	public PutMetricRequest createPutMetricRequest() {
		return new PutMetricRequest();
	}

	public PutMetricResponse createPutMetricResponse() {
		return new PutMetricResponse();
	}

	public PutOprcRequest createPutOprcRequest() {
		return new PutOprcRequest();
	}

	public PutOprcResponse createPutOprcResponse() {
		return new PutOprcResponse();
	}

	public PutWorkCalcRequest createPutWorkCalcRequest() {
		return new PutWorkCalcRequest();
	}

	public PutWorkCalcResponse createPutWorkCalcResponse() {
		return new PutWorkCalcResponse();
	}

	public PutWorkHeadRequest createPutWorkHeadRequest() {
		return new PutWorkHeadRequest();
	}

	public PutWorkHeadResponse createPutWorkHeadResponse() {
		return new PutWorkHeadResponse();
	}

}
