package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTypeElements_TypeWorksResponse", namespace="urn:xmethods-delayed-tpeltpworks")
public class TypeElementsWorkResponse extends AbstractResponse {
}
