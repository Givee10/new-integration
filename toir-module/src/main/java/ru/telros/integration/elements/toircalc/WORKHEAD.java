package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WORKHEAD", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
    "pWipEntityId",
    "pInventoryItemId",
    "pScheduledStartDate"
})
public class WORKHEAD {
    @XmlElement(name = "p_wip_entity_id")
    protected String pWipEntityId;
    @XmlElement(name = "p_inventory_item_id")
    protected String pInventoryItemId;
    @XmlElement(name = "p_scheduled_start_date")
    protected String pScheduledStartDate;

    public String getpWipEntityId() {
        return pWipEntityId;
    }

    public void setpWipEntityId(String pWipEntityId) {
        this.pWipEntityId = pWipEntityId;
    }

    public String getpInventoryItemId() {
        return pInventoryItemId;
    }

    public void setpInventoryItemId(String pInventoryItemId) {
        this.pInventoryItemId = pInventoryItemId;
    }

    public String getpScheduledStartDate() {
        return pScheduledStartDate;
    }

    public void setpScheduledStartDate(String pScheduledStartDate) {
        this.pScheduledStartDate = pScheduledStartDate;
    }
}
