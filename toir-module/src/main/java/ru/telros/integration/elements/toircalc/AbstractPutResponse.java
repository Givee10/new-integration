package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"Retcode"})
public class AbstractPutResponse {
	@XmlElement(required = true)
	protected Retcode Retcode;

	public Retcode getRetcode() {
		return Retcode;
	}

	public void setRetcode(Retcode retcode) {
		Retcode = retcode;
	}
}
