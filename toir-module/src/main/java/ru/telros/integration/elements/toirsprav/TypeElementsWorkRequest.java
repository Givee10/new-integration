package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTypeElements_TypeWorks", namespace="urn:xmethods-delayed-tpeltpworks")
public class TypeElementsWorkRequest extends AbstractRequest {
}
