package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getSmallSpecTechnicTypes", namespace="urn:xmethods-delayed-smsptechtypes")
public class SmallSpecTechnicTypesRequest extends AbstractRequest {
}
