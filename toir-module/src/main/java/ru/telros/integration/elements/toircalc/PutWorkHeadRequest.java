package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putWorkHead", namespace="urn:xmethods-delayed-wworkhead")
public class PutWorkHeadRequest {
	@XmlElement(name = "InputData", required = true)
	protected WORKHEAD InputData;

	public WORKHEAD getInputData() {
		return InputData;
	}

	public void setInputData(WORKHEAD inputData) {
		InputData = inputData;
	}
}
