package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getMetrics", namespace="urn:xmethods-delayed-metrics")
public class MetricsRequest extends AbstractRequest {
}
