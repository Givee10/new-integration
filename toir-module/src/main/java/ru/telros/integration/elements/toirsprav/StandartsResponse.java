package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStandartsResponse", namespace="urn:xmethods-delayed-standarts")
public class StandartsResponse extends AbstractResponse {
}
