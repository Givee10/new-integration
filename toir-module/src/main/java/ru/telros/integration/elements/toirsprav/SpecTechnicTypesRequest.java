package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getSpecTechnicTypes", namespace="urn:xmethods-delayed-sptechtypes")
public class SpecTechnicTypesRequest extends AbstractRequest {
}
