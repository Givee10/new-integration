package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putWorkHeadResponse", namespace="urn:xmethods-delayed-wworkhead")
public class PutWorkHeadResponse extends AbstractPutResponse {
}
