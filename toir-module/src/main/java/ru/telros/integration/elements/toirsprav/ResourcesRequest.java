package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getResources", namespace="urn:xmethods-delayed-resources")
public class ResourcesRequest extends AbstractRequest {
}
