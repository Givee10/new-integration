package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getSourceResponse", namespace="urn:xmethods-delayed-sources")
public class SourceResponse extends AbstractResponse {
}
