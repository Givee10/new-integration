package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTypesWorks", namespace="urn:xmethods-delayed-tpworks")
public class TypesWorksRequest extends AbstractRequest {
}
