package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getSmallSpecTechnicTypesResponse", namespace="urn:xmethods-delayed-smsptechtypes")
public class SmallSpecTechnicTypesResponse extends AbstractResponse {
}
