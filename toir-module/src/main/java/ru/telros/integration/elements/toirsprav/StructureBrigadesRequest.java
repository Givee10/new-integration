package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStructureBrigades", namespace="urn:xmethods-delayed-strbrigades")
public class StructureBrigadesRequest extends AbstractRequest {
}
