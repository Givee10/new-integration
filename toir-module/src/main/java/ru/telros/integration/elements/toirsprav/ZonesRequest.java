package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getZones", namespace="urn:xmethods-delayed-zones")
public class ZonesRequest extends AbstractRequest {
}
