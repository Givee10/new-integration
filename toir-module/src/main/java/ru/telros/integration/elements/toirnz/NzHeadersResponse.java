package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzHeadersResponse", namespace="urn:xmethods-delayed-nzheaders")
public class NzHeadersResponse extends AbstractResponse {
}
