package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkDetNormsResponse", namespace="urn:xmethods-delayed-workdetnorms")
public class WorkDetNormsResponse extends AbstractResponse {
}
