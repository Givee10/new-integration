package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkMaps", namespace="urn:xmethods-delayed-workmaps")
public class WorkMapsRequest extends AbstractRequest {
}
