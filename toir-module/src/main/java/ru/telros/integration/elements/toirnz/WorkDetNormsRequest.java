package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkDetNorms", namespace="urn:xmethods-delayed-workdetnorms")
public class WorkDetNormsRequest extends AbstractRequest {
}
