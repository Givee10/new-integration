package ru.telros.integration.elements.toircalc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class ToirCalcClient extends WebServiceGatewaySupport {
	private static final Logger log = LoggerFactory.getLogger(ToirCalcClient.class);

	public PutEOResponse putEO(EO inputData) {
		PutEORequest request = new PutEORequest();
		request.setInputData(inputData);

		log.info("Put EO");
		return (PutEOResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutMetricResponse putMetric(METRIC inputData) {
		PutMetricRequest request = new PutMetricRequest();
		request.setInputData(inputData);

		log.info("Put Metric");
		return (PutMetricResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutOprcResponse putOprc(OPRC inputData) {
		PutOprcRequest request = new PutOprcRequest();
		request.setInputData(inputData);

		log.info("Put Operation");
		return (PutOprcResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutWorkHeadResponse putWorkHead(WORKHEAD inputData) {
		PutWorkHeadRequest request = new PutWorkHeadRequest();
		request.setInputData(inputData);

		log.info("Put Work Head");
		return (PutWorkHeadResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutWorkCalcResponse putWorkCalc(String id) {
		PutWorkCalcRequest request = new PutWorkCalcRequest();
		request.setId(id);

		log.info("Put Work Calc: {}", id);
		return (PutWorkCalcResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public GetCalcNormResponse getCalcNorm(String id) {
		GetCalcNormRequest request = new GetCalcNormRequest();
		request.setId(id);

		log.info("Get Calc Norm: {}", id);
		return (GetCalcNormResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public GetCalcNormDetailResponse getCalcNormDetail(String id) {
		GetCalcNormDetailRequest request = new GetCalcNormDetailRequest();
		request.setId(id);

		log.info("Get Calc Norm Detail: {}", id);
		return (GetCalcNormDetailResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}
}
