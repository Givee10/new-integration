package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzSmallTechnicsResponse", namespace="urn:xmethods-delayed-nzsmtechs")
public class NzSmallTechnicsResponse extends AbstractResponse {
}
