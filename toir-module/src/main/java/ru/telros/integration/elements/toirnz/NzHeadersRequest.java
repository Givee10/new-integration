package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzHeaders", namespace="urn:xmethods-delayed-nzheaders")
public class NzHeadersRequest extends AbstractRequest {
}
