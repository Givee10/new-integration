package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkNorms", namespace="urn:xmethods-delayed-worknorms")
public class WorkNormsRequest extends AbstractRequest {
}
