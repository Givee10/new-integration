package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getEmployeers", namespace="urn:xmethods-delayed-emps")
public class EmployeersRequest extends AbstractRequest {
}
