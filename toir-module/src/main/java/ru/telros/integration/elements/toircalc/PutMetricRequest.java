package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putMetric", namespace="urn:xmethods-delayed-wmetric")
public class PutMetricRequest {
	@XmlElement(name = "InputData", required = true)
	protected METRIC InputData;

	public METRIC getInputData() {
		return InputData;
	}

	public void setInputData(METRIC inputData) {
		InputData = inputData;
	}
}
