package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTechnicTypesResponse", namespace="urn:xmethods-delayed-techtypes")
public class TechnicTypesResponse extends AbstractResponse {
}
