package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getMetricsResponse", namespace="urn:xmethods-delayed-metrics")
public class MetricsResponse extends AbstractResponse {
}
