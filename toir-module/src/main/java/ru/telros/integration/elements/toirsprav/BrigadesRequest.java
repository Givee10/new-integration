package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getBrigades", namespace="urn:xmethods-delayed-brigades")
public class BrigadesRequest extends AbstractRequest {
}
