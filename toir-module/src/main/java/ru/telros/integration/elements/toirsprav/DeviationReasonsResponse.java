package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getDeviationReasonsResponse", namespace="urn:xmethods-delayed-devreasons")
public class DeviationReasonsResponse extends AbstractResponse {
}
