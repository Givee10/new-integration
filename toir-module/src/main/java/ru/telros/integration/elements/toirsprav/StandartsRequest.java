package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStandarts", namespace="urn:xmethods-delayed-standarts")
public class StandartsRequest extends AbstractRequest {
}
