package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkMetricsResponse", namespace="urn:xmethods-delayed-wmetrics")
public class WorkMetricsResponse extends AbstractResponse {
}
