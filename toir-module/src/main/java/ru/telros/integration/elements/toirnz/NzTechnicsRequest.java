package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzTechnics", namespace="urn:xmethods-delayed-nztechs")
public class NzTechnicsRequest extends AbstractRequest {
}
