package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getDeviationReasons", namespace="urn:xmethods-delayed-devreasons")
public class DeviationReasonsRequest extends AbstractRequest {
}
