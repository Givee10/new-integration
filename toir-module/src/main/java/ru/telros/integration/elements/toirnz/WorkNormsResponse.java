package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkNormsResponse", namespace="urn:xmethods-delayed-worknorms")
public class WorkNormsResponse extends AbstractResponse {
}
