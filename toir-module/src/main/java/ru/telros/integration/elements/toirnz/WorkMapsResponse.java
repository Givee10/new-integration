package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkMapsResponse", namespace="urn:xmethods-delayed-workmaps")
public class WorkMapsResponse extends AbstractResponse {
}
