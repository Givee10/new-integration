package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putWorkCalcResponse", namespace="urn:xmethods-delayed-wworkcalc")
public class PutWorkCalcResponse extends AbstractPutResponse {
}
