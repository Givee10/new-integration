package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getResourcesResponse", namespace="urn:xmethods-delayed-resources")
public class ResourcesResponse extends AbstractResponse {
}
