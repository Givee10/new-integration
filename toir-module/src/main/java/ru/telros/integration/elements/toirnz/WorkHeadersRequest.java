package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getWorkHeaders", namespace="urn:xmethods-delayed-wheaders")
public class WorkHeadersRequest extends AbstractRequest {
}
