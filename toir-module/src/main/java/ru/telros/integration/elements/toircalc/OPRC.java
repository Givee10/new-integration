package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OPRC", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
    "pWipEntityId",
    "pOperationSequenceId"
})
public class OPRC {
    @XmlElement(name = "p_wip_entity_id")
    protected String pWipEntityId;
    @XmlElement(name = "p_operation_sequence_id")
    protected String pOperationSequenceId;

    public String getpWipEntityId() {
        return pWipEntityId;
    }

    public void setpWipEntityId(String pWipEntityId) {
        this.pWipEntityId = pWipEntityId;
    }

    public String getpOperationSequenceId() {
        return pOperationSequenceId;
    }

    public void setpOperationSequenceId(String pOperationSequenceId) {
        this.pOperationSequenceId = pOperationSequenceId;
    }
}
