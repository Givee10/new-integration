package ru.telros.integration.elements.toirnz;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getNzEmployeers", namespace="urn:xmethods-delayed-nzemps")
public class NzEmployeersRequest extends AbstractRequest {
}
