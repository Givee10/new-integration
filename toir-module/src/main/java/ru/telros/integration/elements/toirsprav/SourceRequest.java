package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getSource", namespace="urn:xmethods-delayed-sources")
public class SourceRequest extends AbstractRequest {
}
