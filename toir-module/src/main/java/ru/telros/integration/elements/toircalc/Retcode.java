package ru.telros.integration.elements.toircalc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Retcode", propOrder = {"Retcode"})
public class Retcode {
	@XmlElement(required = true)
	protected String Retcode;

	public String getRetcode() {
		return Retcode;
	}

	public void setRetcode(String retcode) {
		Retcode = retcode;
	}
}
