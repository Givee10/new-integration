package ru.telros.integration.elements.toirsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getTypeElementsResponse", namespace="urn:xmethods-delayed-tpelements")
public class TypeElementsResponse extends AbstractResponse {
}
