package ru.telros.integration;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DatesUtil {
	//public static final String GRID_DATE_FORMAT = "%1$td.%1$tm.%1$tY %1$tH:%1$tM:%1$tS";
	private static final String FILE_DATE_FORMAT = "yyyy-MM-dd";
	public static final String GRID_DATE_FORMAT = "HH:mm dd.MM.yyyy";
	public static final String JSON_DATE_FORMAT = "dd.MM.yyyy_HH:mm:ss";
	public static final String NORMAL_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";
	public static final String ORACLE_DATE_FORMAT = "DD.MM.YYYY HH24:MI:SS";
	public static final String STANDARD_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	public static final String TABLE_DATE_FORMAT = "HH:mm dd.MM.yyyy 'г.'";

	public static String formatNormal(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, NORMAL_DATE_FORMAT);
	}

	public static String formatZoned(ZonedDateTime dateToConvert) {
		return zonedDateTimeToString(dateToConvert, NORMAL_DATE_FORMAT);
	}

	public static String formatToUTC(ZonedDateTime dateToConvert) {
		return zonedDateTimeToString(dateToConvert, NORMAL_DATE_FORMAT, ZoneOffset.UTC);
	}

	public static String timestampToString(Timestamp dateToConvert) {
		return dateToConvert == null ? "" : formatNormal(convertFromUTC(dateToConvert.toLocalDateTime()));
	}

	public static String getSqlBetween(LocalDateTime from, LocalDateTime to) {
		if (from == null || to == null) return null;
		if (from.equals(to)) to = setHourInDate(from, 24);
		return "TO_DATE('" + formatNormal(convertToUTC(from)) + "', '" + ORACLE_DATE_FORMAT + "') AND " +
				"TO_DATE('" + formatNormal(convertToUTC(to)) + "', '" + ORACLE_DATE_FORMAT + "')";
	}

	public static ZonedDateTime setNullTime(ZonedDateTime date) {
		return date.withHour(0).withMinute(0).withSecond(0).withNano(0);
	}

	public static LocalDateTime setNullTime(LocalDateTime date) {
		return date.withHour(0).withMinute(0).withSecond(0).withNano(0);
	}

	public static ZonedDateTime setNullDateTime() {
		return setNullTime(ZonedDateTime.now());
	}

	public static ZonedDateTime setTomorrowNullTime() {
		return setNullDateTime().plusDays(1);
	}

	public static ZonedDateTime setHourInDate(ZonedDateTime date, Integer hour) {
		return setNullTime(date).plusHours(hour);
	}

	public static LocalDateTime setHourInDate(LocalDateTime date, Integer hour) {
		return setNullTime(date).plusHours(hour);
	}

	public static ZonedDateTime setCurrentDate(ZonedDateTime date) {
		ZonedDateTime now = ZonedDateTime.now();
		return ZonedDateTime.of(now.getYear(), now.getMonth().getValue(), now.getDayOfMonth(),
				date.getHour(), date.getMinute(), date.getSecond(), date.getNano(), ZoneId.systemDefault());
	}

	public static LocalDateTime convertFromUTC(LocalDateTime date) {
		return date == null ? null : date.atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static LocalDateTime convertToUTC(LocalDateTime date) {
		return date == null ? null : date.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
	}

	public static ZonedDateTime localToZoned(LocalDateTime date) {
		return date == null ? null : date.atZone(ZoneId.systemDefault());
	}

	public static LocalDateTime zonedToLocal(ZonedDateTime date) {
		return date == null ? null : date.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static String zonedDateTimeToString(ZonedDateTime dateToConvert, String pattern) {
		return zonedDateTimeToString(dateToConvert, pattern, ZoneId.systemDefault());
	}

	public static String zonedDateTimeToString(ZonedDateTime dateToConvert, String pattern, ZoneId zoneId) {
		return dateToConvert == null ? "" :
				dateToConvert.withZoneSameInstant(zoneId).format(DateTimeFormatter.ofPattern(pattern));
	}

	public static String localDateTimeToString(LocalDateTime dateToConvert, String pattern) {
		return dateToConvert == null ? "" : dateToConvert.format(DateTimeFormatter.ofPattern(pattern));
	}
}
