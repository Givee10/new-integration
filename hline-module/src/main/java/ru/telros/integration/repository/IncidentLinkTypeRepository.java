package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentLinkType;

import java.util.List;

@Repository
public interface IncidentLinkTypeRepository extends JpaRepository<IncidentLinkType, Long> {
	@Query(value = "SELECT o FROM IncidentLinkType o WHERE (o.TYPE_ID = ?1)")
	IncidentLinkType findByParameters(String TYPE_ID);

	@Query(value = "SELECT * FROM INCIDENT_LINK_TYPE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentLinkType> findActive();
}
