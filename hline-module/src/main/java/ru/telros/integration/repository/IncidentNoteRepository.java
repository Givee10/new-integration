package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentNote;

import java.util.List;

@Repository
public interface IncidentNoteRepository extends JpaRepository<IncidentNote, Long> {
	@Query(value = "SELECT o FROM IncidentNote o WHERE (o.INCIDENT_NUMBER = ?1 AND o.INCIDENT_ID = ?2 AND o.JTF_NOTE_ID = ?3)")
	IncidentNote findByParameters(String INCIDENT_NUMBER, String INCIDENT_ID, String JTF_NOTE_ID);

	@Query(value = "SELECT * FROM INCIDENT_NOTE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentNote> findActive();
}
