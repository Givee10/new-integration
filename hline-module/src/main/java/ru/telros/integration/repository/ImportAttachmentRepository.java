package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.MessageImportAttachment;

@Repository
public interface ImportAttachmentRepository extends JpaRepository<MessageImportAttachment, Long> {
	@Query(value = "SELECT o FROM MessageImportAttachment o WHERE (o.INCIDENT_ATTACHMENT_ID = ?1)")
	MessageImportAttachment findByParameters(String INCIDENT_ATTACHMENT_ID);
}
