package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.MessageImportIncident;

import java.util.List;

@Repository
public interface ImportIncidentRepository extends JpaRepository<MessageImportIncident, Long> {
	@Query(value = "SELECT o FROM MessageImportIncident o WHERE (o.INCIDENT_ID = ?1 AND o.TYPE_MESSAGE = 'CREATE')")
	MessageImportIncident findByParameters(String INCIDENT_ID);

	@Query(value = "SELECT o FROM MessageImportIncident o WHERE (o.TYPE_MESSAGE = 'CREATE' AND o.REQUEST_ID IS NOT NULL)")
	List<MessageImportIncident> findImportIncidents();
}
