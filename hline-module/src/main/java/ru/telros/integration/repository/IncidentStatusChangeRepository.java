package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentStatusChange;

import java.util.List;

@Repository
public interface IncidentStatusChangeRepository extends JpaRepository<IncidentStatusChange, Long> {
	@Query(value = "SELECT o FROM IncidentStatusChange o WHERE (o.STATUS_TRANSITION_ID = ?1)")
	IncidentStatusChange findByParameters(String STATUS_TRANSITION_ID);

	@Query(value = "SELECT * FROM INCIDENT_STATUS_CHANGE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentStatusChange> findActive();
}
