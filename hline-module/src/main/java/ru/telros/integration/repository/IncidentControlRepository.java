package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentControl;

import java.util.List;

@Repository
public interface IncidentControlRepository extends JpaRepository<IncidentControl, Long> {
	@Query(value = "SELECT o FROM IncidentControl o WHERE (o.INCIDENT_NUMBER = ?1 AND o.INCIDENT_ID = ?2)")
	IncidentControl findByParameters(String INCIDENT_NUMBER, String INCIDENT_ID);

	@Query(value = "SELECT * FROM INCIDENT_CONTROL WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentControl> findActive();
}
