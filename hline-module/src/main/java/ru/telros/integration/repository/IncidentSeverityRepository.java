package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentSeverity;

import java.util.List;

@Repository
public interface IncidentSeverityRepository extends JpaRepository<IncidentSeverity, Long> {
	@Query(value = "SELECT o FROM IncidentSeverity o WHERE (o.INCIDENT_SEVERITY_ID = ?1)")
	IncidentSeverity findByParameters(String INCIDENT_SEVERITY_ID);

	@Query(value = "SELECT * FROM INCIDENT_SEVERITY WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentSeverity> findActive();
}
