package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentTask;

import java.util.List;

@Repository
public interface IncidentTaskRepository extends JpaRepository<IncidentTask, Long> {
	@Query(value = "SELECT o FROM IncidentTask o WHERE (o.TASK_NUMBER = ?1 AND o.TASK_ID = ?2)")
	IncidentTask findByParameters(String TASK_NUMBER, String TASK_ID);

	@Query(value = "SELECT * FROM INCIDENT_TASK WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentTask> findActive();
}
