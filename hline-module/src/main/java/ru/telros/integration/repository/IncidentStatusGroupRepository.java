package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentStatusGroup;

import java.util.List;

@Repository
public interface IncidentStatusGroupRepository extends JpaRepository<IncidentStatusGroup, Long> {
	@Query(value = "SELECT o FROM IncidentStatusGroup o WHERE (o.GROUP_ID = ?1)")
	IncidentStatusGroup findByParameters(String GROUP_ID);

	@Query(value = "SELECT * FROM INCIDENT_STATUS_GROUP WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentStatusGroup> findActive();
}
