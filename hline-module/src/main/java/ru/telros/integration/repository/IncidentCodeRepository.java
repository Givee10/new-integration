package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentCode;

import java.util.List;

@Repository
public interface IncidentCodeRepository extends JpaRepository<IncidentCode, Long> {
	@Query(value = "SELECT o FROM IncidentCode o WHERE (o.INCIDENT_CODE = ?1)")
	IncidentCode findByParameters(String INCIDENT_CODE);

	@Query(value = "SELECT * FROM INCIDENT_CODE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentCode> findActive();
}
