package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentRepeatType;

import java.util.List;

@Repository
public interface IncidentRepeatTypeRepository extends JpaRepository<IncidentRepeatType, Long> {
	@Query(value = "SELECT o FROM IncidentRepeatType o WHERE (o.REPEATED_TYPE_ID = ?1)")
	IncidentRepeatType findByParameters(String REPEATED_TYPE_ID);

	@Query(value = "SELECT * FROM INCIDENT_REPEAT_TYPE WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentRepeatType> findActive();
}
