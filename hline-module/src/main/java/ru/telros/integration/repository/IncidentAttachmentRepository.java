package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.telros.integration.domain.IncidentAttachment;

import java.util.List;

public interface IncidentAttachmentRepository extends JpaRepository<IncidentAttachment, Long> {
	@Query(value = "SELECT o FROM IncidentAttachment o WHERE (o.ATTACHED_DOCUMENT_ID = ?1 AND o.INCIDENT_ID = ?2)")
	IncidentAttachment findByParameters(String ATTACHED_DOCUMENT_ID, String INCIDENT_ID);

	@Query(value = "SELECT * FROM INCIDENT_ATTACHMENT WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentAttachment> findActive();
}
