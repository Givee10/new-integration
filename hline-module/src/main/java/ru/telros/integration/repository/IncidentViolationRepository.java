package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentViolation;

import java.util.List;

@Repository
public interface IncidentViolationRepository extends JpaRepository<IncidentViolation, Long> {
	@Query(value = "SELECT o FROM IncidentViolation o WHERE (o.VIOLATION_ID = ?1)")
	IncidentViolation findByParameters(String VIOLATION_ID);

	@Query(value = "SELECT * FROM INCIDENT_VIOLATION WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentViolation> findActive();
}
