package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.Incident;

import java.util.List;

@Repository
public interface IncidentRepository extends JpaRepository<Incident, Long> {
	@Query(value = "SELECT o FROM Incident o WHERE (o.INCIDENT_NUMBER = ?1 AND o.INCIDENT_ID = ?2)")
	Incident findByParameters(String INCIDENT_NUMBER, String INCIDENT_ID);

	@Query(value = "SELECT o FROM Incident o WHERE (o.INCIDENT_ID = ?1)")
	Incident findById(String INCIDENT_ID);

	@Query(value = "SELECT o FROM Incident o WHERE (o.INCIDENT_NUMBER = ?1)")
	Incident findByNumber(String INCIDENT_NUMBER);

	@Query(value = "SELECT * FROM INCIDENT WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<Incident> findActive();
}
