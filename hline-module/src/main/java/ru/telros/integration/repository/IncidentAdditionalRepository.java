package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentAdditional;

import java.util.List;

@Repository
public interface IncidentAdditionalRepository extends JpaRepository<IncidentAdditional, Long> {
	@Query(value = "SELECT o FROM IncidentAdditional o WHERE (o.INCIDENT_NUMBER = ?1 AND o.INCIDENT_ID = ?2)")
	IncidentAdditional findByParameters(String INCIDENT_NUMBER, String INCIDENT_ID);

	@Query(value = "SELECT * FROM INCIDENT_ADDITIONAL WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentAdditional> findActive();
}
