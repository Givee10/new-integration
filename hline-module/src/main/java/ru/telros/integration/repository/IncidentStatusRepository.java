package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentStatus;

import java.util.List;

@Repository
public interface IncidentStatusRepository extends JpaRepository<IncidentStatus, Long> {
	@Query(value = "SELECT o FROM IncidentStatus o WHERE (o.INCIDENT_STATUS_ID = ?1)")
	IncidentStatus findByParameters(String INCIDENT_STATUS_ID);

	@Query(value = "SELECT * FROM INCIDENT_STATUS WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentStatus> findActive();
}
