package ru.telros.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.telros.integration.domain.IncidentLink;

import java.util.List;

@Repository
public interface IncidentLinkRepository extends JpaRepository<IncidentLink, Long> {
	@Query(value = "SELECT o FROM IncidentLink o WHERE (o.LINK_ID = ?1)")
	IncidentLink findByParameters(String LINK_ID);

	@Query(value = "SELECT * FROM INCIDENT_LINK WHERE IS_ACTIVE = '1'", nativeQuery = true)
	List<IncidentLink> findActive();
}
