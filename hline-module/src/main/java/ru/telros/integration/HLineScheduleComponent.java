package ru.telros.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.telros.integration.controller.CustomRestTemplate;
import ru.telros.integration.domain.*;
import ru.telros.integration.elements.AbstractResponse;
import ru.telros.integration.elements.ArrayINCATT;
import ru.telros.integration.elements.INCATT;
import ru.telros.integration.elements.hlinc.HotLineClient;
import ru.telros.integration.elements.hlinc.IncAttResponse;
import ru.telros.integration.elements.hlsprav.HotLineRefClient;
import ru.telros.integration.service.IncidentService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ru.telros.integration.controller.HLineController.logResponse;

@Component
public class HLineScheduleComponent {
	private static final Logger LOGGER = LoggerFactory.getLogger(HLineScheduleComponent.class);

	@Autowired
	private HotLineClient hlMainClient;
	@Autowired
	private HotLineRefClient hlRefClient;
	@Autowired
	private IncidentService incidentService;
	@Autowired
	private CustomRestTemplate restTemplate;

	private ObjectMapper objectMapper = new ObjectMapper();

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNewIncs() {
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getNewIncs();
			logResponse = logResponse(response);
			Incident[] incidents = objectMapper.readValue(logResponse, Incident[].class);
			for (Incident incident : incidents) {
				Incident savedEntity = incidentService.saveEntity(incident);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkIncs() {
		List<Incident> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getIncs();
			logResponse = logResponse(response);
			Incident[] incidents = objectMapper.readValue(logResponse, Incident[].class);
			for (Incident incident : incidents) {
				Incident savedEntity = incidentService.saveEntity(incident);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldIncidents(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNewIncAdds() {
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getNewIncAdds();
			logResponse = logResponse(response);
			IncidentAdditional[] additionals = objectMapper.readValue(logResponse, IncidentAdditional[].class);
			for (IncidentAdditional additional : additionals) {
				IncidentAdditional savedEntity = incidentService.saveEntity(additional);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkIncAdds() {
		List<IncidentAdditional> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getIncAdds();
			logResponse = logResponse(response);
			IncidentAdditional[] additionals = objectMapper.readValue(logResponse, IncidentAdditional[].class);
			for (IncidentAdditional additional : additionals) {
				IncidentAdditional savedEntity = incidentService.saveEntity(additional);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(additional);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldAdditionals(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkIncLinks() {
		List<IncidentLink> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getIncLinks();
			logResponse = logResponse(response);
			IncidentLink[] links = objectMapper.readValue(logResponse, IncidentLink[].class);
			for (IncidentLink link : links) {
				IncidentLink savedEntity = incidentService.saveEntity(link);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldLinks(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNewIncNotes() {
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getNewIncNotes();
			logResponse = logResponse(response);
			IncidentNote[] notes = objectMapper.readValue(logResponse, IncidentNote[].class);
			for (IncidentNote note : notes) {
				IncidentNote savedEntity = incidentService.saveEntity(note);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkIncNotes() {
		List<IncidentNote> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getIncNotes();
			logResponse = logResponse(response);
			IncidentNote[] notes = objectMapper.readValue(logResponse, IncidentNote[].class);
			for (IncidentNote note : notes) {
				IncidentNote savedEntity = incidentService.saveEntity(note);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldNotes(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.entities-interval-millis}")
	private void checkNewIncTasks() {
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getNewIncTasks();
			logResponse = logResponse(response);
			IncidentTask[] tasks = objectMapper.readValue(logResponse, IncidentTask[].class);
			for (IncidentTask task : tasks) {
				IncidentTask savedEntity = incidentService.saveEntity(task);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkIncTasks() {
		List<IncidentTask> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlMainClient.getIncTasks();
			logResponse = logResponse(response);
			IncidentTask[] tasks = objectMapper.readValue(logResponse, IncidentTask[].class);
			for (IncidentTask task : tasks) {
				IncidentTask savedEntity = incidentService.saveEntity(task);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldTasks(newEntityList);
	}


	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkCodes() {
		List<IncidentCode> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getCodes();
			logResponse = logResponse(response);
			IncidentCode[] codes = objectMapper.readValue(logResponse, IncidentCode[].class);
			for (IncidentCode code : codes) {
				IncidentCode savedEntity = incidentService.saveEntity(code);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldCodes(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkLinkTypes() {
		List<IncidentLinkType> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getLinkTypes();
			logResponse = logResponse(response);
			IncidentLinkType[] linkTypes = objectMapper.readValue(logResponse, IncidentLinkType[].class);
			for (IncidentLinkType linkType : linkTypes) {
				IncidentLinkType savedEntity = incidentService.saveEntity(linkType);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldLinkTypes(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkRelevans() {
		List<IncidentSeverity> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getRelevans();
			logResponse = logResponse(response);
			IncidentSeverity[] severities = objectMapper.readValue(logResponse, IncidentSeverity[].class);
			for (IncidentSeverity severity : severities) {
				IncidentSeverity savedEntity = incidentService.saveEntity(severity);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldSeverities(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkRepeatTypes() {
		List<IncidentRepeatType> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getRepeatTypes();
			logResponse = logResponse(response);
			IncidentRepeatType[] repeatTypes = objectMapper.readValue(logResponse, IncidentRepeatType[].class);
			for (IncidentRepeatType repeatType : repeatTypes) {
				IncidentRepeatType savedEntity = incidentService.saveEntity(repeatType);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldRepeatTypes(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkStates() {
		List<IncidentStatus> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getStates();
			logResponse = logResponse(response);
			IncidentStatus[] statuses = objectMapper.readValue(logResponse, IncidentStatus[].class);
			for (IncidentStatus status : statuses) {
				IncidentStatus savedEntity = incidentService.saveEntity(status);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldStatuses(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkStateChanges() {
		List<IncidentStatusChange> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getStateChanges();
			logResponse = logResponse(response);
			IncidentStatusChange[] statusChanges = objectMapper.readValue(logResponse, IncidentStatusChange[].class);
			for (IncidentStatusChange statusChange : statusChanges) {
				IncidentStatusChange savedEntity = incidentService.saveEntity(statusChange);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldStatusChanges(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkStateGroups() {
		List<IncidentStatusGroup> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getStateGroups();
			logResponse = logResponse(response);
			IncidentStatusGroup[] statusGroups = objectMapper.readValue(logResponse, IncidentStatusGroup[].class);
			for (IncidentStatusGroup statusGroup : statusGroups) {
				IncidentStatusGroup savedEntity = incidentService.saveEntity(statusGroup);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldStatusGroups(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.glossary-interval-millis}")
	private void checkViolReas() {
		List<IncidentViolation> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		try {
			response = hlRefClient.getViolReas();
			logResponse = logResponse(response);
			IncidentViolation[] violations = objectMapper.readValue(logResponse, IncidentViolation[].class);
			for (IncidentViolation violation : violations) {
				IncidentViolation savedEntity = incidentService.saveEntity(violation);
				LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
				newEntityList.add(savedEntity);
			}
		} catch (Exception e) {
			LOGGER.error("Request error: {}", e.getMessage());
		}
		incidentService.deleteOldViolations(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.additional-interval-millis}")
	private void checkIncControls() {
		List<IncidentControl> newEntityList = new ArrayList<>();
		AbstractResponse response;
		String logResponse;
		List<MessageImportIncident> importIncidents = incidentService.findImportIncidents();
		for (MessageImportIncident importIncident : importIncidents) {
			try {
				response = hlMainClient.getIncCntlTab(Long.valueOf(importIncident.INCIDENT_ID));
				if (response != null) {
					logResponse = logResponse(response);
					IncidentControl[] controls = objectMapper.readValue(logResponse, IncidentControl[].class);
					for (IncidentControl control : controls) {
						IncidentControl incidentControl = incidentService.saveEntity(control);
						LOGGER.debug(StringUtil.writeValueAsString(incidentControl));
						newEntityList.add(incidentControl);
					}
				}
			} catch (Exception e) {
				LOGGER.error("Request error: {}", e.getMessage());
			}
		}
		incidentService.deleteOldControls(newEntityList);
	}

	@Scheduled(fixedRateString = "${scheduler.additional-interval-millis}")
	private void checkIncAttachments() {
		List<MessageImportIncident> importIncidents = incidentService.findImportIncidents();
		for (MessageImportIncident importIncident : importIncidents) {
			Incident incident = incidentService.findIncidentById(importIncident.INCIDENT_ID);
			IncAttResponse attResponse = hlMainClient.getIncAtt(Long.valueOf(incident.INCIDENT_ID));
			if (attResponse != null) {
				ArrayINCATT arrayINCATT = attResponse.getArrayINCATT();
				List<INCATT> incattList = arrayINCATT.getIncatt();
				for (INCATT item : incattList) {
					String fileData = item.getFileData();
					String fileName = item.getFileName();
					MessageImportAttachment attachment = incidentService.findImportAttachmentById(item.getAttachedDocumentId());
					if (attachment == null) {
						try {
							File file = StringUtil.decodeBase64ToFile(fileData, importIncident.INCIDENT_ID, fileName);
							AttachmentDto attachmentDto = restTemplate.postAttachment(Long.valueOf(importIncident.REQUEST_ID), fileName, file);
							if (attachmentDto != null) {
								Date date = new Date();
								MessageImportAttachment importAttachment = new MessageImportAttachment();
								importAttachment.INCIDENT_ID = importIncident.INCIDENT_ID;
								importAttachment.INCIDENT_ATTACHMENT_ID = item.getAttachedDocumentId();
								importAttachment.REQUEST_ID = importIncident.REQUEST_ID;
								importAttachment.REQUEST_ATTACHMENT_ID = String.valueOf(attachmentDto.getId());
								importAttachment.INCIDENT_NUMBER = incident.INCIDENT_NUMBER;
								importAttachment.CREATE_DATE = date;
								importAttachment.CHANGE_DATE = date;
								MessageImportAttachment savedEntity = incidentService.saveEntity(importAttachment);
								LOGGER.debug(StringUtil.writeValueAsString(savedEntity));
							}
						} catch (IOException e) {
							LOGGER.error(e.getMessage());
						}
					}
				}
			}
		}
	}
}
