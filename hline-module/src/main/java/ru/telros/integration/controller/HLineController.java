package ru.telros.integration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.telros.integration.domain.IncAttach;
import ru.telros.integration.domain.IncInfo;
import ru.telros.integration.domain.IncItem;
import ru.telros.integration.domain.IncNote;
import ru.telros.integration.elements.*;
import ru.telros.integration.elements.hlinc.*;
import ru.telros.integration.elements.hlput.*;
import ru.telros.integration.elements.hlsprav.CodesResponse;
import ru.telros.integration.elements.hlsprav.HotLineRefClient;
import ru.telros.integration.elements.hlsprav.RelevansResponse;
import ru.telros.integration.elements.hlsprav.RepeatTypeResponse;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class HLineController {
	private static final Logger LOGGER = LoggerFactory.getLogger(HLineController.class);

	@Autowired
	private HotLineClient hlMainClient;
	@Autowired
	private HotLineRefClient hlRefClient;
	@Autowired
	private HotLinePutClient hlPutClient;

	public static String logResponse(AbstractResponse response) {
		if (response == null) return null;
		ObjectMapper objectMapper = new ObjectMapper();
		Result result = response.getResult();
		List<ResultItem> itemList = result.getItem();
		ArrayNode arrayNode = objectMapper.createArrayNode();
		//int i = 0;
		for (ResultItem item : itemList) {
			//i++;
			List<ResponseItem> responseItemList = item.getItem();
			ObjectNode objectNode = objectMapper.createObjectNode();
			for (ResponseItem responseItem : responseItemList) {
				//LOGGER.debug("{} {} - Key: {}, Value: {}", response.getClass().getSimpleName(), i, responseItem.getKey(), responseItem.getValue());
				objectNode.put(responseItem.getKey(), responseItem.getValue());
			}
			arrayNode.add(objectNode);
		}
		return arrayNode.toString();
	}

	public static String logResponse(AbstractInputResponse response) {
		if (response == null) return null;
		ObjectMapper objectMapper = new ObjectMapper();
		InputResult result = response.getResult();
		List<ResponseItem> responseItemList = result.getItem();
		ObjectNode objectNode = objectMapper.createObjectNode();
		for (ResponseItem responseItem : responseItemList) {
			//LOGGER.debug("{} {} - Key: {}, Value: {}", response.getClass().getSimpleName(), i, responseItem.getKey(), responseItem.getValue());
			objectNode.put(responseItem.getKey(), responseItem.getValue());
		}
		return objectNode.toString();
	}

	/*
	1. GETCODES() - коды проблем
	2. GETREPEATTYPE() - тип повторности заявки
	3. GETRELEVANS() - важность заявки
	4. GETLINKTYPES() - типы связей заявок
	5. GETSTATEGROUPS() - группы статусов для полномочий
	6. GETSTATECHANGE() -  переходы статусов по группам
	7. GETVIOLREAS() - причина отклонения сроков выполнения заявки
	8. GETSTATE() – справочник статусов
	*/
	@GetMapping(value = "codes")
	@ResponseBody
	public String getCodes() {
		CodesResponse response = hlRefClient.getCodes();
		return logResponse(response);
	}

	@GetMapping(value = "codes/{id}")
	@ResponseBody
	public String getCode(
			@PathVariable("id") Long id) {
		CodesResponse response = hlRefClient.getCode(id);
		return logResponse(response);
	}

	@GetMapping(value = "relevans")
	@ResponseBody
	public String getRelevans() {
		RelevansResponse response = hlRefClient.getRelevans();
		return logResponse(response);
	}

	@GetMapping(value = "relevans/{id}")
	@ResponseBody
	public String getRelevan(
			@PathVariable("id") Long id) {
		RelevansResponse response = hlRefClient.getRelevan(id);
		return logResponse(response);
	}

	@GetMapping(value = "repeattypes")
	@ResponseBody
	public String getRepeatTypes() {
		RepeatTypeResponse response = hlRefClient.getRepeatTypes();
		return logResponse(response);
	}

	@GetMapping(value = "repeattypes/{id}")
	@ResponseBody
	public String getRepeatType(
			@PathVariable("id") Long id) {
		RepeatTypeResponse response = hlRefClient.getRepeatType(id);
		return logResponse(response);
	}

	/*
	 * GETINC() - основная инфрмация по заявке
	 * GETINCNOTES() - комментарии к заявкам и работам
	 * GETINCCNTLTAB() - значения полей закладки "Контроль" в форме заявки ГЛ
	 * GETINCLINK() - связанные заявки (дочки)
	 * GETINCTASK() - закладка "Работы"
	 */
	@GetMapping(value = "incs")
	@ResponseBody
	public String getIncs() {
		IncResponse response = hlMainClient.getIncs();
		return logResponse(response);
	}

	@GetMapping(value = "incs/{id}")
	@ResponseBody
	public String getInc(@PathVariable("id") Long id) {
		IncResponse response = hlMainClient.getInc(id);
		return logResponse(response);
	}

	@GetMapping(value = "incs/adds")
	@ResponseBody
	public String getIncAdds() {
		IncAddResponse response = hlMainClient.getIncAdds();
		return logResponse(response);
	}

	@GetMapping(value = "incs/adds/{id}")
	@ResponseBody
	public String getIncAdd(@PathVariable("id") Long id) {
		IncAddResponse response = hlMainClient.getIncAdd(id);
		return logResponse(response);
	}

	@GetMapping(value = "incs/att/{id}")
	@ResponseBody
	public String getIncAtt(@PathVariable("id") Long id) {
		IncAttResponse response = hlMainClient.getIncAtt(id);
		if (response != null) {
			String result = "";
			List<INCATT> incattList = response.getArrayINCATT().getIncatt();
			for (INCATT incatt : incattList) {
				result = result.concat(incatt.toString()).concat("\n");
			}
			return result;
		} else {
			return "Error!";
		}
	}

	@GetMapping(value = "incs/cntltab/{id}")
	@ResponseBody
	public String getIncCntlTab(@PathVariable("id") Long id) {
		IncCntlTabResponse response = hlMainClient.getIncCntlTab(id);
		return logResponse(response);
	}

	@GetMapping(value = "incs/links")
	@ResponseBody
	public String getIncLinks() {
		IncLinkResponse response = hlMainClient.getIncLinks();
		return logResponse(response);
	}

	@GetMapping(value = "incs/links/{id}")
	@ResponseBody
	public String getIncLink(@PathVariable("id") Long id) {
		IncLinkResponse response = hlMainClient.getIncLink(id);
		return logResponse(response);
	}

	@GetMapping(value = "incs/notes")
	@ResponseBody
	public String getIncNotes() {
		IncNotesResponse response = hlMainClient.getIncNotes();
		return logResponse(response);
	}

	@GetMapping(value = "incnotes/{id}")
	@ResponseBody
	public String getIncNote(@PathVariable("id") Long id) {
		IncNotesResponse response = hlMainClient.getIncNote(id);
		return logResponse(response);
	}

	@GetMapping(value = "incs/tasks")
	@ResponseBody
	public String getIncTasks() {
		IncTaskResponse response = hlMainClient.getIncTasks();
		return logResponse(response);
	}

	@GetMapping(value = "incs/tasks/{id}")
	@ResponseBody
	public String getIncTask(@PathVariable("id") Long id) {
		IncTaskResponse response = hlMainClient.getIncTask(id);
		return logResponse(response);
	}

	/*
	 * PUTATTACHADD() - добавление вложения
	 * PUTINCINFO() - добавление информации
	 * PUTTEMADD() - добавление задания
	 * PUTNOTEADD() - добавление комментария
	 */
	@PostMapping(value = "incs/attach")
	@ResponseBody
	public String putIncAttach(@RequestBody IncAttach body) {
		PutAttachAddResponse response = hlPutClient.addAttach(createAttach(body));
		return logResponse(response);
	}

	@PostMapping(value = "incs/info")
	@ResponseBody
	public String putIncInfo(@RequestBody IncInfo body) {
		PutIncInfoResponse response = hlPutClient.addInfo(createInfo(body));
		return logResponse(response);
	}

	@PostMapping(value = "incs/item")
	@ResponseBody
	public String putIncItem(@RequestBody IncItem body) {
		PutItemAddResponse response = hlPutClient.addItem(createItem(body));
		return logResponse(response);
	}

	@PostMapping(value = "incs/note")
	@ResponseBody
	public String putIncNote(@RequestBody IncNote body) {
		PutNoteAddResponse response = hlPutClient.addNote(createNote(body));
		return logResponse(response);
	}

	private ATTACHADD createAttach(IncAttach incAttach) {
		ATTACHADD attach = new ATTACHADD();
		attach.setpIncidentId(incAttach.getpIncidentId());
		attach.setpIncidentNumber(incAttach.getpIncidentNumber());
		attach.setpFileName(incAttach.getpFileName());
		attach.setpFileDescr(incAttach.getpFileDescr());
		attach.setpFileData(incAttach.getpFileData());
		attach.setpMimeType(incAttach.getpMimeType());
		return attach;
	}

	private INCINFO createInfo(IncInfo incInfo) {
		INCINFO info = new INCINFO();
		info.setpIncidentId(incInfo.getpIncidentId());
		info.setpIncidentNumber(incInfo.getpIncidentNumber());
		info.setpStatusId(incInfo.getpStatusId());
		info.setpStatusName(incInfo.getpStatusName());
		info.setpAddAdrInfo(incInfo.getpAddAdrInfo());
		info.setpFilialNum(incInfo.getpFilialNum());
		info.setpStartDate(incInfo.getpStartDate());
		info.setpClosedDate(incInfo.getpClosedDate());
		info.setpUpdatedByTime(incInfo.getpUpdatedByTime());
		info.setpUpdatedByFio(incInfo.getpUpdatedByFio());
		info.setpUpdatedByUsr(incInfo.getpUpdatedByUsr());
		return info;
	}

	private ITEMADD createItem(IncItem incItem) {
		ITEMADD item = new ITEMADD();
		item.setpIncidentId(incItem.getpIncidentId());
		item.setpIncidentNumber(incItem.getpIncidentNumber());
		item.setpTaskNumber(incItem.getpTaskNumber());
		item.setpTaskStatusId(incItem.getpTaskStatusId());
		item.setpTaskType(incItem.getpTaskType());
		item.setpTaskDescr(incItem.getpTaskDescr());
		item.setpTaskNotes(incItem.getpTaskNotes());
		item.setpResponsibleName(incItem.getpResponsibleName());
		item.setpResponsibleFio(incItem.getpResponsibleFio());
		item.setpTaskStartTime(incItem.getpTaskStartTime());
		item.setpTaskEndTime(incItem.getpTaskEndTime());
		item.setpUpdatedByTime(incItem.getpUpdatedByTime());
		return item;
	}

	private NOTEADD createNote(IncNote incNote) {
		NOTEADD note = new NOTEADD();
		note.setpIncidentId(incNote.getpIncidentId());
		note.setpIncidentNumber(incNote.getpIncidentNumber());
		note.setpTaskId(incNote.getpTaskId());
		note.setpOrderNum(incNote.getpOrderNum());
		note.setpNotesId(incNote.getpNotesId());
		note.setpNotesText(incNote.getpNotesText());
		note.setpNotesType(incNote.getpNotesType());
		note.setpNotesVisible(incNote.getpNotesVisible());
		note.setpUpdatedByTime(incNote.getpUpdatedByTime());
		note.setpUpdatedByFio(incNote.getpUpdatedByFio());
		note.setpUpdatedByUsr(incNote.getpUpdatedByUsr());
		return note;
	}
}
