package ru.telros.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.telros.integration.domain.Incident;
import ru.telros.integration.service.IncidentService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class MainController {
	@Autowired
	private IncidentService service;

	@GetMapping(value = "incidents")
	@ResponseBody
	public List<Incident> getIncidents() {
		return service.findIncidents();
	}

	@GetMapping(value = "incidents/{id}")
	@ResponseBody
	public Incident getIncidentById(@PathVariable("id") String id) {
		return service.findIncidentById(id);
	}

	@GetMapping(value = "incidents/address")
	@ResponseBody
	public String getAddressByNumber(@RequestParam("number") String number) {
		return service.findAddressIdByNumber(number);
	}
}
