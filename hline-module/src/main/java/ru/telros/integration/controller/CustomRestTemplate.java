package ru.telros.integration.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import ru.telros.integration.domain.AttachmentDto;

import java.io.File;

@Component
public class CustomRestTemplate {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomRestTemplate.class);

	@Value("${test.server-ip}")
	private String serverIp;
	@Value("${test.server-port}")
	private String serverPort;

	@Autowired
	private RestTemplateBuilder builder;

	private RestTemplate getRestTemplate() {
		return builder.basicAuthorization("admin", "admin").build();
	}

	private static void logExceptions(String url, RestClientException e) {
		if (e instanceof RestClientResponseException) {
			LOGGER.error("Request {} returns with error {}", url, ((RestClientResponseException)e).getResponseBodyAsString());
		}
		if (e instanceof ResourceAccessException) {
			LOGGER.error(e.getMessage());
		}
	}

	public AttachmentDto postAttachment(Long requestId, String fileName, File file) {
		String url = String.format("http://%s:%s/api/v1/requests/%d/attachments", serverIp, serverPort, requestId);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("file", new FileSystemResource(file));
		body.add("type", "иной документ");
		body.add("description", fileName);
		body.add("mobile", false);
		body.add("source", "ГЛ");
		body.add("name", fileName);
		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(body, headers);
		try {
			ResponseEntity<AttachmentDto> entity = getRestTemplate().postForEntity(url, httpEntity, AttachmentDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}
}
