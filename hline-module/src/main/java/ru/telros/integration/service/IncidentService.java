package ru.telros.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.telros.integration.StringUtil;
import ru.telros.integration.domain.*;
import ru.telros.integration.repository.*;

import java.util.Date;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class IncidentService {
	private static final Logger LOGGER = LoggerFactory.getLogger(IncidentService.class);
	private final ImportAttachmentRepository importAttachmentRepository;
	private final ImportIncidentRepository importIncidentRepository;
	private final IncidentRepository incidentRepository;
	private final IncidentAdditionalRepository additionalRepository;
	private final IncidentAttachmentRepository attachmentRepository;
	private final IncidentCodeRepository codeRepository;
	private final IncidentControlRepository controlRepository;
	private final IncidentLinkRepository linkRepository;
	private final IncidentLinkTypeRepository linkTypeRepository;
	private final IncidentNoteRepository noteRepository;
	private final IncidentRepeatTypeRepository repeatTypeRepository;
	private final IncidentSeverityRepository severityRepository;
	private final IncidentStatusChangeRepository statusChangeRepository;
	private final IncidentStatusGroupRepository statusGroupRepository;
	private final IncidentStatusRepository statusRepository;
	private final IncidentTaskRepository taskRepository;
	private final IncidentViolationRepository violationRepository;

	@Autowired
	public IncidentService(ImportAttachmentRepository importAttachmentRepository, ImportIncidentRepository importIncidentRepository,
						   IncidentRepository incidentRepository, IncidentAdditionalRepository additionalRepository,
						   IncidentAttachmentRepository attachmentRepository, IncidentCodeRepository codeRepository,
						   IncidentControlRepository controlRepository, IncidentLinkRepository linkRepository,
						   IncidentLinkTypeRepository linkTypeRepository, IncidentNoteRepository noteRepository,
						   IncidentRepeatTypeRepository repeatTypeRepository, IncidentSeverityRepository severityRepository,
						   IncidentStatusChangeRepository statusChangeRepository, IncidentStatusGroupRepository statusGroupRepository,
						   IncidentStatusRepository statusRepository, IncidentTaskRepository taskRepository,
						   IncidentViolationRepository violationRepository) {
		this.importAttachmentRepository = importAttachmentRepository;
		this.importIncidentRepository = importIncidentRepository;
		this.incidentRepository = incidentRepository;
		this.additionalRepository = additionalRepository;
		this.attachmentRepository = attachmentRepository;
		this.codeRepository = codeRepository;
		this.controlRepository = controlRepository;
		this.linkRepository = linkRepository;
		this.linkTypeRepository = linkTypeRepository;
		this.noteRepository = noteRepository;
		this.repeatTypeRepository = repeatTypeRepository;
		this.severityRepository = severityRepository;
		this.statusChangeRepository = statusChangeRepository;
		this.statusGroupRepository = statusGroupRepository;
		this.statusRepository = statusRepository;
		this.taskRepository = taskRepository;
		this.violationRepository = violationRepository;
	}

	private <T extends AbstractEntity> T updateEntity(T oldEntity, T newEntity) {
		Date date = new Date();
		if (oldEntity != null) {
			newEntity.ID = oldEntity.ID;
			newEntity.CREATE_DATE = oldEntity.CREATE_DATE;
			newEntity.CHANGE_DATE = date;
		} else {
			newEntity.CREATE_DATE = date;
			newEntity.CHANGE_DATE = date;
			LOGGER.info(StringUtil.writeValueAsString(newEntity));
		}
		newEntity.IS_ACTIVE = true;
		return newEntity;
	}

	public Incident saveEntity(Incident entity) {
		Incident oldEntity = incidentRepository.findByParameters(entity.INCIDENT_NUMBER, entity.INCIDENT_ID);
		return incidentRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentAdditional saveEntity(IncidentAdditional entity) {
		IncidentAdditional oldEntity = additionalRepository.findByParameters(entity.INCIDENT_NUMBER, entity.INCIDENT_ID);
		return additionalRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentAttachment saveEntity(IncidentAttachment entity) {
		IncidentAttachment oldEntity = attachmentRepository.findByParameters(entity.ATTACHED_DOCUMENT_ID, entity.INCIDENT_ID);
		return attachmentRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentCode saveEntity(IncidentCode entity) {
		IncidentCode oldEntity = codeRepository.findByParameters(entity.INCIDENT_CODE);
		return codeRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentControl saveEntity(IncidentControl entity) {
		IncidentControl oldEntity = controlRepository.findByParameters(entity.INCIDENT_NUMBER, entity.INCIDENT_ID);
		return controlRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentLink saveEntity(IncidentLink entity) {
		IncidentLink oldEntity = linkRepository.findByParameters(entity.LINK_ID);
		return linkRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentLinkType saveEntity(IncidentLinkType entity) {
		IncidentLinkType oldEntity = linkTypeRepository.findByParameters(entity.TYPE_ID);
		return linkTypeRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentNote saveEntity(IncidentNote entity) {
		IncidentNote oldEntity = noteRepository.findByParameters(entity.INCIDENT_NUMBER, entity.INCIDENT_ID, entity.JTF_NOTE_ID);
		return noteRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentRepeatType saveEntity(IncidentRepeatType entity) {
		IncidentRepeatType oldEntity = repeatTypeRepository.findByParameters(entity.REPEATED_TYPE_ID);
		return repeatTypeRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentSeverity saveEntity(IncidentSeverity entity) {
		IncidentSeverity oldEntity = severityRepository.findByParameters(entity.INCIDENT_SEVERITY_ID);
		return severityRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentStatus saveEntity(IncidentStatus entity) {
		IncidentStatus oldEntity = statusRepository.findByParameters(entity.INCIDENT_STATUS_ID);
		return statusRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentStatusChange saveEntity(IncidentStatusChange entity) {
		IncidentStatusChange oldEntity = statusChangeRepository.findByParameters(entity.STATUS_TRANSITION_ID);
		return statusChangeRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentStatusGroup saveEntity(IncidentStatusGroup entity) {
		IncidentStatusGroup oldEntity = statusGroupRepository.findByParameters(entity.GROUP_ID);
		return statusGroupRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentTask saveEntity(IncidentTask entity) {
		IncidentTask oldEntity = taskRepository.findByParameters(entity.TASK_NUMBER, entity.TASK_ID);
		return taskRepository.save(updateEntity(oldEntity, entity));
	}

	public IncidentViolation saveEntity(IncidentViolation entity) {
		IncidentViolation oldEntity = violationRepository.findByParameters(entity.VIOLATION_ID);
		return violationRepository.save(updateEntity(oldEntity, entity));
	}

	public MessageImportAttachment saveEntity(MessageImportAttachment entity) {
		MessageImportAttachment oldEntity = importAttachmentRepository.findByParameters(entity.INCIDENT_ATTACHMENT_ID);
		if (oldEntity != null) entity.ID = oldEntity.ID;
		return importAttachmentRepository.save(entity);
	}

	public MessageImportIncident saveEntity(MessageImportIncident entity) {
		MessageImportIncident oldEntity = importIncidentRepository.findByParameters(entity.INCIDENT_ID);
		if (oldEntity != null) entity.ID = oldEntity.ID;
		return importIncidentRepository.save(entity);
	}

	public List<Incident> findIncidents() {
		return incidentRepository.findAll();
	}

	public Incident findIncidentById(String id) {
		return incidentRepository.findById(id);
	}

	public Incident findIncidentByNumber(String number) {
		return incidentRepository.findByNumber(number);
	}

	public String findAddressIdByNumber(String number) {
		Incident incident = findIncidentByNumber(number);
		return incident == null ? "" : incident.PRF_ID;
	}

	public List<MessageImportIncident> findImportIncidents() {
		return importIncidentRepository.findImportIncidents();
	}

	public MessageImportAttachment findImportAttachmentById(String INCIDENT_ATTACHMENT_ID) {
		return importAttachmentRepository.findByParameters(INCIDENT_ATTACHMENT_ID);
	}

	public void deleteOldIncidents(List<Incident> newEntityList) {
		List<Incident> oldEntityList = incidentRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (Incident entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			incidentRepository.save(entity);
		}
	}

	public void deleteOldAdditionals(List<IncidentAdditional> newEntityList) {
		List<IncidentAdditional> oldEntityList = additionalRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentAdditional entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			additionalRepository.save(entity);
		}
	}

	public void deleteOldAttachments(List<IncidentAttachment> newEntityList) {
		List<IncidentAttachment> oldEntityList = attachmentRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentAttachment entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			attachmentRepository.save(entity);
		}
	}

	public void deleteOldCodes(List<IncidentCode> newEntityList) {
		List<IncidentCode> oldEntityList = codeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentCode entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			codeRepository.save(entity);
		}
	}

	public void deleteOldControls(List<IncidentControl> newEntityList) {
		List<IncidentControl> oldEntityList = controlRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentControl entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			controlRepository.save(entity);
		}
	}

	public void deleteOldLinks(List<IncidentLink> newEntityList) {
		List<IncidentLink> oldEntityList = linkRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentLink entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			linkRepository.save(entity);
		}
	}

	public void deleteOldLinkTypes(List<IncidentLinkType> newEntityList) {
		List<IncidentLinkType> oldEntityList = linkTypeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentLinkType entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			linkTypeRepository.save(entity);
		}
	}

	public void deleteOldNotes(List<IncidentNote> newEntityList) {
		List<IncidentNote> oldEntityList = noteRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentNote entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			noteRepository.save(entity);
		}
	}

	public void deleteOldRepeatTypes(List<IncidentRepeatType> newEntityList) {
		List<IncidentRepeatType> oldEntityList = repeatTypeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentRepeatType entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			repeatTypeRepository.save(entity);
		}
	}

	public void deleteOldSeverities(List<IncidentSeverity> newEntityList) {
		List<IncidentSeverity> oldEntityList = severityRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentSeverity entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			severityRepository.save(entity);
		}
	}

	public void deleteOldStatusChanges(List<IncidentStatusChange> newEntityList) {
		List<IncidentStatusChange> oldEntityList = statusChangeRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentStatusChange entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			statusChangeRepository.save(entity);
		}
	}

	public void deleteOldStatusGroups(List<IncidentStatusGroup> newEntityList) {
		List<IncidentStatusGroup> oldEntityList = statusGroupRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentStatusGroup entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			statusGroupRepository.save(entity);
		}
	}

	public void deleteOldStatuses(List<IncidentStatus> newEntityList) {
		List<IncidentStatus> oldEntityList = statusRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentStatus entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			statusRepository.save(entity);
		}
	}

	public void deleteOldTasks(List<IncidentTask> newEntityList) {
		List<IncidentTask> oldEntityList = taskRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentTask entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			taskRepository.save(entity);
		}
	}

	public void deleteOldViolations(List<IncidentViolation> newEntityList) {
		List<IncidentViolation> oldEntityList = violationRepository.findActive();
		oldEntityList.removeAll(newEntityList);
		for (IncidentViolation entity : oldEntityList) {
			entity.IS_ACTIVE = false;
			violationRepository.save(entity);
		}
	}
}
