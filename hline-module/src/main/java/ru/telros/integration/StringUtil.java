package ru.telros.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StringUtil {
	public static Boolean isNull(String s) {
		return s == null || s.trim().isEmpty() || s.equalsIgnoreCase("null");
	}

	public static String writeValueAsString(Object value) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String returnString(String value) {
		//String.valueOf() returns "null", that's bad
		return value == null ? "" : value;
	}

	public static File decodeBase64ToFile(String value, String number, String filename) throws IOException {
		byte[] bytes = Base64.decodeBase64(value.getBytes(StandardCharsets.US_ASCII));
		Path directory = Paths.get("./files/demo", number);
		if (Files.notExists(directory)) {
			Files.createDirectory(directory);
		}
		Path destinationFile = Paths.get(directory.toString(), changeFileName(filename));
		Files.write(destinationFile, bytes);
		return destinationFile.toFile();
	}

	public static String encodeBase64FromFile(String url) throws IOException {
		File file = new File(url);
		byte[] bytes = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
		return new String(bytes, StandardCharsets.US_ASCII);
	}

	public static String changeFileName(String fileName) {
		String extension = fileName.substring(fileName.lastIndexOf("."));
		return System.currentTimeMillis() + extension;
	}
}
