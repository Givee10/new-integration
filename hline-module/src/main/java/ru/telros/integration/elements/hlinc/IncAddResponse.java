package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncAddResponse", namespace="urn:xmethods-delayed-addes")
public class IncAddResponse extends AbstractResponse {
}
