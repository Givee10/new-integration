package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getRelevans", namespace="urn:xmethods-delayed-relevans")
public class RelevansRequest extends AbstractRequest {
}
