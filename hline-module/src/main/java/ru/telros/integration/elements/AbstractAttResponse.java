package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"ArrayINCATT"})
public abstract class AbstractAttResponse {
    @XmlElement(name = "Result", required = true)
    protected ArrayINCATT ArrayINCATT;

    public ArrayINCATT getArrayINCATT() {
        return ArrayINCATT;
    }
}
