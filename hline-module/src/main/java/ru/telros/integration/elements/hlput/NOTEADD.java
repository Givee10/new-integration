package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NOTEADD", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"pIncidentId",
		"pIncidentNumber",
		"pTaskId",
		"pOrderNum",
		"pNotesId",
		"pNotesText",
		"pNotesType",
		"pNotesVisible",
		"pUpdatedByTime",
		"pUpdatedByFio",
		"pUpdatedByUsr"
})
public class NOTEADD {
	@XmlElement(name = "p_incident_id")
	protected String pIncidentId;
	@XmlElement(name = "p_incident_number")
	protected String pIncidentNumber;
	@XmlElement(name = "p_task_id")
	protected String pTaskId;
	@XmlElement(name = "p_order_num")
	protected String pOrderNum;
	@XmlElement(name = "p_notes_id")
	protected String pNotesId;
	@XmlElement(name = "p_notes_text")
	protected String pNotesText;
	@XmlElement(name = "p_notes_type")
	protected String pNotesType;
	@XmlElement(name = "p_notes_visible")
	protected String pNotesVisible;
	@XmlElement(name = "p_updated_by_time")
	protected String pUpdatedByTime;
	@XmlElement(name = "p_updated_by_fio")
	protected String pUpdatedByFio;
	@XmlElement(name = "p_updated_by_usr")
	protected String pUpdatedByUsr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpTaskId() {
		return pTaskId;
	}

	public void setpTaskId(String pTaskId) {
		this.pTaskId = pTaskId;
	}

	public String getpOrderNum() {
		return pOrderNum;
	}

	public void setpOrderNum(String pOrderNum) {
		this.pOrderNum = pOrderNum;
	}

	public String getpNotesId() {
		return pNotesId;
	}

	public void setpNotesId(String pNotesId) {
		this.pNotesId = pNotesId;
	}

	public String getpNotesText() {
		return pNotesText;
	}

	public void setpNotesText(String pNotesText) {
		this.pNotesText = pNotesText;
	}

	public String getpNotesType() {
		return pNotesType;
	}

	public void setpNotesType(String pNotesType) {
		this.pNotesType = pNotesType;
	}

	public String getpNotesVisible() {
		return pNotesVisible;
	}

	public void setpNotesVisible(String pNotesVisible) {
		this.pNotesVisible = pNotesVisible;
	}

	public String getpUpdatedByTime() {
		return pUpdatedByTime;
	}

	public void setpUpdatedByTime(String pUpdatedByTime) {
		this.pUpdatedByTime = pUpdatedByTime;
	}

	public String getpUpdatedByFio() {
		return pUpdatedByFio;
	}

	public void setpUpdatedByFio(String pUpdatedByFio) {
		this.pUpdatedByFio = pUpdatedByFio;
	}

	public String getpUpdatedByUsr() {
		return pUpdatedByUsr;
	}

	public void setpUpdatedByUsr(String pUpdatedByUsr) {
		this.pUpdatedByUsr = pUpdatedByUsr;
	}
}
