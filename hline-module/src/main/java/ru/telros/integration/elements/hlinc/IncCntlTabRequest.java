package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncCntlTab", namespace="urn:xmethods-delayed-ictabs")
public class IncCntlTabRequest extends AbstractRequest {
}
