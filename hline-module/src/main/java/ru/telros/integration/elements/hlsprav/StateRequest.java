package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getState", namespace="urn:xmethods-delayed-states")
public class StateRequest extends AbstractRequest {
}
