package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCodesResponse", namespace="urn:xmethods-delayed-codes")
public class CodesResponse extends AbstractResponse {
}
