package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getRepeatTypeResponse", namespace="urn:xmethods-delayed-rttypes")
public class RepeatTypeResponse extends AbstractResponse {
}
