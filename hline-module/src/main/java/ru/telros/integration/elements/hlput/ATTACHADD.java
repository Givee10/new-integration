package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ATTACHADD", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"pIncidentId",
		"pIncidentNumber",
		"pFileName",
		"pMimeType",
		"pFileData",
		"pFileDescr"
})
public class ATTACHADD {
	@XmlElement(name = "p_incident_id")
	protected String pIncidentId;
	@XmlElement(name = "p_incident_number")
	protected String pIncidentNumber;
	@XmlElement(name = "p_file_name")
	protected String pFileName;
	@XmlElement(name = "p_mime_type")
	protected String pMimeType;
	@XmlElement(name = "p_file_data")
	protected String pFileData;
	@XmlElement(name = "p_file_descr")
	protected String pFileDescr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpFileName() {
		return pFileName;
	}

	public void setpFileName(String pFileName) {
		this.pFileName = pFileName;
	}

	public String getpMimeType() {
		return pMimeType;
	}

	public void setpMimeType(String pMimeType) {
		this.pMimeType = pMimeType;
	}

	public String getpFileData() {
		return pFileData;
	}

	public void setpFileData(String pFileData) {
		this.pFileData = pFileData;
	}

	public String getpFileDescr() {
		return pFileDescr;
	}

	public void setpFileDescr(String pFileDescr) {
		this.pFileDescr = pFileDescr;
	}
}
