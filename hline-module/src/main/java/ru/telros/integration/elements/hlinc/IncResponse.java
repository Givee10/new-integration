package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncResponse", namespace="urn:xmethods-delayed-inces")
public class IncResponse extends AbstractResponse {
}
