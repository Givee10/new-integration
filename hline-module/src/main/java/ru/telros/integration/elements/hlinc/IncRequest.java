package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractInputRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getInc", namespace="urn:xmethods-delayed-inces")
public class IncRequest extends AbstractInputRequest {
}
