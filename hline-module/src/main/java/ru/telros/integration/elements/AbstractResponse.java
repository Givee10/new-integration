package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"Result"})
public abstract class AbstractResponse {
    @XmlElement(required = true)
    protected Result Result;

    public Result getResult() {
        return Result;
    }
}
