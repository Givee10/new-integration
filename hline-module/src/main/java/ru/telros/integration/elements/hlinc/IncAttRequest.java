package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncAtt", namespace="urn:xmethods-delayed-attes")
public class IncAttRequest extends AbstractRequest {
}
