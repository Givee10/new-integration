package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncLink", namespace="urn:xmethods-delayed-inclinks")
public class IncLinkRequest extends AbstractRequest {
}
