package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStateChange", namespace="urn:xmethods-delayed-stchanges")
public class StateChangeRequest extends AbstractRequest {
}
