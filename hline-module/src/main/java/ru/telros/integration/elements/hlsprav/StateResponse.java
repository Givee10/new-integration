package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStateResponse", namespace="urn:xmethods-delayed-states")
public class StateResponse extends AbstractResponse {
}
