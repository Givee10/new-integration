package ru.telros.integration.elements;

import ru.telros.integration.elements.hlinc.*;
import ru.telros.integration.elements.hlput.*;
import ru.telros.integration.elements.hlsprav.*;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {
	public ObjectFactory() {
	}

	/* hlinc */

	public IncRequest createIncRequest() {
		return new IncRequest();
	}

	public IncResponse createIncResponse() {
		return new IncResponse();
	}

	public IncNotesRequest createIncNotesRequest() {
		return new IncNotesRequest();
	}

	public IncNotesResponse createIncNotesResponse() {
		return new IncNotesResponse();
	}

	public IncAddRequest createIncAddRequest() {
		return new IncAddRequest();
	}

	public IncAddResponse createIncAddResponse() {
		return new IncAddResponse();
	}

	public IncAttRequest createIncAttRequest() {
		return new IncAttRequest();
	}

	public IncAttResponse createIncAttResponse() {
		return new IncAttResponse();
	}

	public IncCntlTabRequest createIncCntlTabRequest() {
		return new IncCntlTabRequest();
	}

	public IncCntlTabResponse createIncCntlTabResponse() {
		return new IncCntlTabResponse();
	}

	public IncLinkRequest createIncLinkRequest() {
		return new IncLinkRequest();
	}

	public IncLinkResponse createIncLinkResponse() {
		return new IncLinkResponse();
	}

	public IncTaskRequest createIncTaskRequest() {
		return new IncTaskRequest();
	}

	public IncTaskResponse createIncTaskResponse() {
		return new IncTaskResponse();
	}

	/* hlsprav */

	public CodesRequest createCodesRequest() {
		return new CodesRequest();
	}

	public CodesResponse createCodesResponse() {
		return new CodesResponse();
	}

	public LinkTypesRequest createLinkTypesRequest() {
		return new LinkTypesRequest();
	}

	public LinkTypesResponse createLinkTypesResponse() {
		return new LinkTypesResponse();
	}

	public RelevansRequest createRelevansRequest() {
		return new RelevansRequest();
	}

	public RelevansResponse createRelevansResponse() {
		return new RelevansResponse();
	}

	public RepeatTypeRequest createRepeatTypeRequest() {
		return new RepeatTypeRequest();
	}

	public RepeatTypeResponse createRepeatTypeResponse() {
		return new RepeatTypeResponse();
	}

	public StateRequest createStateRequest() {
		return new StateRequest();
	}

	public StateResponse createStateResponse() {
		return new StateResponse();
	}

	public StateChangeRequest createStateChangeRequest() {
		return new StateChangeRequest();
	}

	public StateChangeResponse createStateChangeResponse() {
		return new StateChangeResponse();
	}

	public StateGroupsRequest createStateGroupsRequest() {
		return new StateGroupsRequest();
	}

	public StateGroupsResponse createStateGroupsResponse() {
		return new StateGroupsResponse();
	}

	public ViolReasRequest createViolReasRequest() {
		return new ViolReasRequest();
	}

	public ViolReasResponse createViolReasResponse() {
		return new ViolReasResponse();
	}

	/* hlput */

	public PutAttachAddRequest createPutAttachAddRequest() {
		return new PutAttachAddRequest();
	}

	public PutAttachAddResponse createPutAttachAddResponse() {
		return new PutAttachAddResponse();
	}

	public PutIncInfoRequest createPutIncInfoRequest() {
		return new PutIncInfoRequest();
	}

	public PutIncInfoResponse createPutIncInfoResponse() {
		return new PutIncInfoResponse();
	}

	public PutItemAddRequest createPutItemAddRequest() {
		return new PutItemAddRequest();
	}

	public PutItemAddResponse createPutItemAddResponse() {
		return new PutItemAddResponse();
	}

	public PutNoteAddRequest createPutNoteAddRequest() {
		return new PutNoteAddRequest();
	}

	public PutNoteAddResponse createPutNoteAddResponse() {
		return new PutNoteAddResponse();
	}
}
