package ru.telros.integration.elements.hlput;

import ru.telros.integration.elements.AbstractInputResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putIncInfoResponse", namespace="urn:xmethods-delayed-wincinf")
public class PutIncInfoResponse extends AbstractInputResponse {
}
