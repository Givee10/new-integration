package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "INCINFO", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"pIncidentId",
		"pIncidentNumber",
		"pStatusId",
		"pStatusName",
		"pAddAdrInfo",
		"pFilialNum",
		"pStartDate",
		"pClosedDate",
		"pUpdatedByTime",
		"pUpdatedByFio",
		"pUpdatedByUsr"
})
public class INCINFO {
	@XmlElement(name = "p_incident_id")
	protected String pIncidentId;
	@XmlElement(name = "p_incident_number")
	protected String pIncidentNumber;
	@XmlElement(name = "p_status_id")
	protected String pStatusId;
	@XmlElement(name = "p_status_name")
	protected String pStatusName;
	@XmlElement(name = "p_add_adr_info")
	protected String pAddAdrInfo;
	@XmlElement(name = "p_filial_num")
	protected String pFilialNum;
	@XmlElement(name = "p_start_date")
	protected String pStartDate;
	@XmlElement(name = "p_closed_date")
	protected String pClosedDate;
	@XmlElement(name = "p_updated_by_time")
	protected String pUpdatedByTime;
	@XmlElement(name = "p_updated_by_fio")
	protected String pUpdatedByFio;
	@XmlElement(name = "p_updated_by_usr")
	protected String pUpdatedByUsr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpStatusId() {
		return pStatusId;
	}

	public void setpStatusId(String pStatusId) {
		this.pStatusId = pStatusId;
	}

	public String getpStatusName() {
		return pStatusName;
	}

	public void setpStatusName(String pStatusName) {
		this.pStatusName = pStatusName;
	}

	public String getpAddAdrInfo() {
		return pAddAdrInfo;
	}

	public void setpAddAdrInfo(String pAddAdrInfo) {
		this.pAddAdrInfo = pAddAdrInfo;
	}

	public String getpFilialNum() {
		return pFilialNum;
	}

	public void setpFilialNum(String pFilialNum) {
		this.pFilialNum = pFilialNum;
	}

	public String getpStartDate() {
		return pStartDate;
	}

	public void setpStartDate(String pStartDate) {
		this.pStartDate = pStartDate;
	}

	public String getpClosedDate() {
		return pClosedDate;
	}

	public void setpClosedDate(String pClosedDate) {
		this.pClosedDate = pClosedDate;
	}

	public String getpUpdatedByTime() {
		return pUpdatedByTime;
	}

	public void setpUpdatedByTime(String pUpdatedByTime) {
		this.pUpdatedByTime = pUpdatedByTime;
	}

	public String getpUpdatedByFio() {
		return pUpdatedByFio;
	}

	public void setpUpdatedByFio(String pUpdatedByFio) {
		this.pUpdatedByFio = pUpdatedByFio;
	}

	public String getpUpdatedByUsr() {
		return pUpdatedByUsr;
	}

	public void setpUpdatedByUsr(String pUpdatedByUsr) {
		this.pUpdatedByUsr = pUpdatedByUsr;
	}
}
