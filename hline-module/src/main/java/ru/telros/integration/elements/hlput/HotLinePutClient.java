package ru.telros.integration.elements.hlput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class HotLinePutClient extends WebServiceGatewaySupport {
	private static final Logger log = LoggerFactory.getLogger(HotLinePutClient.class);

	public PutAttachAddResponse addAttach(ATTACHADD inputData) {
		PutAttachAddRequest request = new PutAttachAddRequest();
		request.setInputData(inputData);

		log.info("Saving New Attach");
		return (PutAttachAddResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutIncInfoResponse addInfo(INCINFO inputData) {
		PutIncInfoRequest request = new PutIncInfoRequest();
		request.setInputData(inputData);

		log.info("Saving New Info");
		return (PutIncInfoResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutItemAddResponse addItem(ITEMADD inputData) {
		PutItemAddRequest request = new PutItemAddRequest();
		request.setInputData(inputData);

		log.info("Saving New Item");
		return (PutItemAddResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public PutNoteAddResponse addNote(NOTEADD inputData) {
		PutNoteAddRequest request = new PutNoteAddRequest();
		request.setInputData(inputData);

		log.info("Saving New Note");
		return (PutNoteAddResponse) getWebServiceTemplate().marshalSendAndReceive(request);
	}
}
