package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
public abstract class AbstractInputRequest {
    @XmlElement(required = true)
    protected InputData InputData;

    public InputData getInputData() {
        return InputData;
    }

    public void setInputData(InputData inputData) {
        InputData = inputData;
    }
}
