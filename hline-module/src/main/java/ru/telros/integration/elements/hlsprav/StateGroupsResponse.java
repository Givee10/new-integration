package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStateGroupsResponse", namespace="urn:xmethods-delayed-stgroups")
public class StateGroupsResponse extends AbstractResponse {
}
