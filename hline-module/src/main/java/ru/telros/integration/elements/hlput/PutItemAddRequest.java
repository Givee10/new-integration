package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putItemAdd", namespace="urn:xmethods-delayed-witadd")
public class PutItemAddRequest {
	@XmlElement(name = "InputData", required = true)
	protected ITEMADD InputData;

	public ITEMADD getInputData() {
		return InputData;
	}

	public void setInputData(ITEMADD inputData) {
		InputData = inputData;
	}
}
