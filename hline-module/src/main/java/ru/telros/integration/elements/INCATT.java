package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "INCATT", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"incidentId",
		"attachedDocumentId",
		"seqNum",
		"fileName",
		"fileContentType",
		"fileData",
		"uploadDate",
		"incidentCreationDate",
		"incidentLastUpdateDate"
})
public class INCATT {
	@XmlElement(name = "INCIDENT_ID")
	protected String incidentId;
	@XmlElement(name = "ATTACHED_DOCUMENT_ID")
	protected String attachedDocumentId;
	@XmlElement(name = "SEQ_NUM")
	protected String seqNum;
	@XmlElement(name = "FILE_NAME")
	protected String fileName;
	@XmlElement(name = "FILE_CONTENT_TYPE")
	protected String fileContentType;
	@XmlElement(name = "FILE_DATA")
	protected String fileData;
	@XmlElement(name = "UPLOAD_DATE")
	protected String uploadDate;
	@XmlElement(name = "INCIDENT_CREATION_DATE")
	protected String incidentCreationDate;
	@XmlElement(name = "INCIDENT_LAST_UPDATE_DATE")
	protected String incidentLastUpdateDate;

	public String getIncidentId() {
		return incidentId;
	}

	public String getAttachedDocumentId() {
		return attachedDocumentId;
	}

	public String getSeqNum() {
		return seqNum;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public String getFileData() {
		return fileData;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public String getIncidentCreationDate() {
		return incidentCreationDate;
	}

	public String getIncidentLastUpdateDate() {
		return incidentLastUpdateDate;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("INCATT{");
		sb.append("incidentId='").append(incidentId).append('\'');
		sb.append(", attachedDocumentId='").append(attachedDocumentId).append('\'');
		sb.append(", seqNum='").append(seqNum).append('\'');
		sb.append(", fileName='").append(fileName).append('\'');
		sb.append(", fileContentType='").append(fileContentType).append('\'');
		sb.append(", fileData='").append(fileData).append('\'');
		sb.append(", uploadDate='").append(uploadDate).append('\'');
		sb.append(", incidentCreationDate='").append(incidentCreationDate).append('\'');
		sb.append(", incidentLastUpdateDate='").append(incidentLastUpdateDate).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
