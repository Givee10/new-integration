package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractInputRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncAdd", namespace="urn:xmethods-delayed-addes")
public class IncAddRequest extends AbstractInputRequest {
}
