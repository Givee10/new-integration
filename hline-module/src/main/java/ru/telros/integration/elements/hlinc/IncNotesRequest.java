package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractInputRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncNotes", namespace="urn:xmethods-delayed-incnotes")
public class IncNotesRequest extends AbstractInputRequest {
}
