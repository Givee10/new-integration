package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayINCATT", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"incatt"
})
public class ArrayINCATT {
	@XmlElement(name = "INCATT", nillable = true)
	protected List<INCATT> incatt;

	public List<INCATT> getIncatt() {
		return incatt;
	}
}
