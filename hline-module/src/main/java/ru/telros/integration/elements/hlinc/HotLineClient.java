package ru.telros.integration.elements.hlinc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import ru.telros.integration.elements.InputData;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HotLineClient extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(HotLineClient.class);
	private static final long YEAR_VALUE = 10;
	private static final long HOUR_VALUE = 2;
	private static final long ADD_VALUE = 1;

	private InputData createInputData(Long id) {
		LocalDateTime now = LocalDateTime.now();
		return createInputData(id, now.minusYears(YEAR_VALUE), now.plusHours(ADD_VALUE));
	}

	private InputData createInputData(Long id, LocalDateTime start, LocalDateTime finish) {
		InputData inputData = new InputData();
		inputData.setId(id == null ? "0" : id.toString());
		inputData.setMinIncidentCreationDate(dateToString(start));
		inputData.setMaxIncidentCreationDate(dateToString(finish));
		inputData.setMinIncidentLastUpdateDate(dateToString(start));
		inputData.setMaxIncidentLastUpdateDate(dateToString(finish));
		return inputData;
	}

	private String dateToString(LocalDateTime dateToConvert) {
		return dateToConvert == null ? "" : dateToConvert.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
	}

    /* основная информация по заявке */
    public IncResponse getIncs() {
        IncRequest request = new IncRequest();
		request.setInputData(createInputData(null));

        log.info("Requesting All Incidents");
		try {
			return (IncResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

	public IncResponse getNewIncs() {
		LocalDateTime now = LocalDateTime.now();
		IncRequest request = new IncRequest();
		request.setInputData(createInputData(null, now.minusHours(HOUR_VALUE), now.plusHours(ADD_VALUE)));

		log.info("Requesting New Incidents");
		try {
			return (IncResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
	}

    public IncResponse getInc(Long id) {
        IncRequest request = new IncRequest();
        request.setInputData(createInputData(id));

        log.info("Requesting Incident " + id);
		try {
			return (IncResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    /* дополнительная информация по заявке */
    public IncAddResponse getIncAdds() {
        IncAddRequest request = new IncAddRequest();
		request.setInputData(createInputData(null));

        log.info("Requesting All Adds");
		try {
			return (IncAddResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

	public IncAddResponse getNewIncAdds() {
		LocalDateTime now = LocalDateTime.now();
		IncAddRequest request = new IncAddRequest();
		request.setInputData(createInputData(null, now.minusHours(HOUR_VALUE), now.plusHours(ADD_VALUE)));

		log.info("Requesting New Adds");
		try {
			return (IncAddResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
	}

    public IncAddResponse getIncAdd(Long id) {
        IncAddRequest request = new IncAddRequest();
		request.setInputData(createInputData(id));

        log.info("Requesting Add " + id);
		try {
			return (IncAddResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    /* вложения заявок */
    public IncAttResponse getIncAtt(Long id) {
        IncAttRequest request = new IncAttRequest();
        request.setId(id.toString());

        log.info("Requesting Attachment " + id);
		try {
			return (IncAttResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    /* значения полей закладки "Контроль" в форме заявки ГЛ */
    public IncCntlTabResponse getIncCntlTab(Long id) {
        IncCntlTabRequest request = new IncCntlTabRequest();
        request.setId(id.toString());

        log.info("Requesting Control tab " + id);
        try {
			return (IncCntlTabResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    /* связанные заявки (дочки) */
    public IncLinkResponse getIncLinks() {
        IncLinkRequest request = new IncLinkRequest();
        request.setId("0");

        log.info("Requesting All Links");
		try {
			return (IncLinkResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    public IncLinkResponse getIncLink(Long id) {
        IncLinkRequest request = new IncLinkRequest();
        request.setId(id.toString());

        log.info("Requesting Link " + id);
		try {
			return (IncLinkResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    /* комментарии к заявкам и работам */
    public IncNotesResponse getIncNotes() {
        IncNotesRequest request = new IncNotesRequest();
		request.setInputData(createInputData(null));

        log.info("Requesting All Notes");
		try {
			return (IncNotesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

	public IncNotesResponse getNewIncNotes() {
		LocalDateTime now = LocalDateTime.now();
		IncNotesRequest request = new IncNotesRequest();
		request.setInputData(createInputData(null, now.minusHours(HOUR_VALUE), now.plusHours(ADD_VALUE)));

		log.info("Requesting New Notes");
		try {
			return (IncNotesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
	}

    public IncNotesResponse getIncNote(Long id) {
        IncNotesRequest request = new IncNotesRequest();
		request.setInputData(createInputData(id));

        log.info("Requesting Note " + id);
		try {
			return (IncNotesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

    /* закладка "Работы" */
    public IncTaskResponse getIncTasks() {
        IncTaskRequest request = new IncTaskRequest();
		request.setInputData(createInputData(null));

        log.info("Requesting All Tasks");
		try {
			return (IncTaskResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }

	public IncTaskResponse getNewIncTasks() {
		LocalDateTime now = LocalDateTime.now();
		IncTaskRequest request = new IncTaskRequest();
		request.setInputData(createInputData(null, now.minusHours(HOUR_VALUE), now.plusHours(ADD_VALUE)));

		log.info("Requesting New Tasks");
		try {
			return (IncTaskResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
	}

    public IncTaskResponse getIncTask(Long id) {
        IncTaskRequest request = new IncTaskRequest();
		request.setInputData(createInputData(id));

        log.info("Requesting Task " + id);
		try {
			return (IncTaskResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		} catch (Exception e) {
			log.error("Request error: {}", e.getMessage());
			return null;
		}
    }
}
