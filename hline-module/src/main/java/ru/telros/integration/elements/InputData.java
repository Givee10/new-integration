package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "INP", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"id",
		"minIncidentCreationDate",
		"maxIncidentCreationDate",
		"minIncidentLastUpdateDate",
		"maxIncidentLastUpdateDate"
})
public class InputData {
	@XmlElement(name = "id")
	protected String id;
	@XmlElement(name = "min_incident_creation_date")
	protected String minIncidentCreationDate;
	@XmlElement(name = "max_incident_creation_date")
	protected String maxIncidentCreationDate;
	@XmlElement(name = "min_incident_last_update_date")
	protected String minIncidentLastUpdateDate;
	@XmlElement(name = "max_incident_last_update_date")
	protected String maxIncidentLastUpdateDate;

	public String getId() {
		return id;
	}

	public String getMinIncidentCreationDate() {
		return minIncidentCreationDate;
	}

	public String getMaxIncidentCreationDate() {
		return maxIncidentCreationDate;
	}

	public String getMinIncidentLastUpdateDate() {
		return minIncidentLastUpdateDate;
	}

	public String getMaxIncidentLastUpdateDate() {
		return maxIncidentLastUpdateDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMinIncidentCreationDate(String minIncidentCreationDate) {
		this.minIncidentCreationDate = minIncidentCreationDate;
	}

	public void setMaxIncidentCreationDate(String maxIncidentCreationDate) {
		this.maxIncidentCreationDate = maxIncidentCreationDate;
	}

	public void setMinIncidentLastUpdateDate(String minIncidentLastUpdateDate) {
		this.minIncidentLastUpdateDate = minIncidentLastUpdateDate;
	}

	public void setMaxIncidentLastUpdateDate(String maxIncidentLastUpdateDate) {
		this.maxIncidentLastUpdateDate = maxIncidentLastUpdateDate;
	}
}
