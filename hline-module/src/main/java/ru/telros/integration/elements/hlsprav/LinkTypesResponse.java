package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getLinkTypesResponse", namespace="urn:xmethods-delayed-lktypes")
public class LinkTypesResponse extends AbstractResponse {
}
