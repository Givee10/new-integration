package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStateChangeResponse", namespace="urn:xmethods-delayed-stchanges")
public class StateChangeResponse extends AbstractResponse {
}
