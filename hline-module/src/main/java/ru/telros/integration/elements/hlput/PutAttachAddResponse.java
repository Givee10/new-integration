package ru.telros.integration.elements.hlput;

import ru.telros.integration.elements.AbstractInputResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putAttachAddResponse", namespace="urn:xmethods-delayed-wattadd")
public class PutAttachAddResponse extends AbstractInputResponse {
}
