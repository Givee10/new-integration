package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncTaskResponse", namespace="urn:xmethods-delayed-inctasks")
public class IncTaskResponse extends AbstractResponse {
}
