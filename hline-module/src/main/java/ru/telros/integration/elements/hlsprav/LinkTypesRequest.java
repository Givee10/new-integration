package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getLinkTypes", namespace="urn:xmethods-delayed-lktypes")
public class LinkTypesRequest extends AbstractRequest {
}
