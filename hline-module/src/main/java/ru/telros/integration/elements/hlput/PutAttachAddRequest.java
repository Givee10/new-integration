package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putAttachAdd", namespace="urn:xmethods-delayed-wattadd")
public class PutAttachAddRequest {
	@XmlElement(name = "InputData", required = true)
	protected ATTACHADD InputData;

	public ATTACHADD getInputData() {
		return InputData;
	}

	public void setInputData(ATTACHADD inputData) {
		InputData = inputData;
	}
}
