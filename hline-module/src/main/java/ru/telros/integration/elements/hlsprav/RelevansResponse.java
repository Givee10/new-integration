package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getRelevansResponse", namespace="urn:xmethods-delayed-relevans")
public class RelevansResponse extends AbstractResponse {
}
