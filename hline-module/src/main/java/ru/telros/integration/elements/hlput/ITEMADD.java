package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ITEMADD", namespace = "http://schemas.xmlsoap.org/soap/encoding/", propOrder = {
		"pIncidentId",
		"pIncidentNumber",
		"pTaskNumber",
		"pTaskStatusId",
		"pTaskType",
		"pTaskDescr",
		"pTaskNotes",
		"pResponsibleName",
		"pResponsibleFio",
		"pTaskStartTime",
		"pTaskEndTime",
		"pUpdatedByTime"
//		"pUpdatedByFio",
//		"pUpdatedByUsr"
})
public class ITEMADD {
	@XmlElement(name = "p_incident_id")
	protected String pIncidentId;
	@XmlElement(name = "p_incident_number")
	protected String pIncidentNumber;
	@XmlElement(name = "p_task_number")
	protected String pTaskNumber;
	@XmlElement(name = "p_task_status_id")
	protected String pTaskStatusId;
	@XmlElement(name = "p_task_type")
	protected String pTaskType;
	@XmlElement(name = "p_task_descr")
	protected String pTaskDescr;
	@XmlElement(name = "p_task_notes")
	protected String pTaskNotes;
	@XmlElement(name = "p_responsible_name")
	protected String pResponsibleName;
	@XmlElement(name = "p_responsible_fio")
	protected String pResponsibleFio;
	@XmlElement(name = "p_task_start_time")
	protected String pTaskStartTime;
	@XmlElement(name = "p_task_end_time")
	protected String pTaskEndTime;
	@XmlElement(name = "p_updated_by_time")
	protected String pUpdatedByTime;
//	@XmlElement(name = "p_updated_by_fio")
//	protected String pUpdatedByFio;
//	@XmlElement(name = "p_updated_by_usr")
//	protected String pUpdatedByUsr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpTaskNumber() {
		return pTaskNumber;
	}

	public void setpTaskNumber(String pTaskNumber) {
		this.pTaskNumber = pTaskNumber;
	}

	public String getpTaskStatusId() {
		return pTaskStatusId;
	}

	public void setpTaskStatusId(String pTaskStatusId) {
		this.pTaskStatusId = pTaskStatusId;
	}

	public String getpTaskType() {
		return pTaskType;
	}

	public void setpTaskType(String pTaskType) {
		this.pTaskType = pTaskType;
	}

	public String getpTaskDescr() {
		return pTaskDescr;
	}

	public void setpTaskDescr(String pTaskDescr) {
		this.pTaskDescr = pTaskDescr;
	}

	public String getpTaskNotes() {
		return pTaskNotes;
	}

	public void setpTaskNotes(String pTaskNotes) {
		this.pTaskNotes = pTaskNotes;
	}

	public String getpResponsibleName() {
		return pResponsibleFio;
	}

	public void setpResponsibleName(String pResponsibleName) {
		this.pResponsibleFio = pResponsibleName;
	}

	public String getpResponsibleFio() {
		return pResponsibleFio;
	}

	public void setpResponsibleFio(String pResponsibleFio) {
		this.pResponsibleFio = pResponsibleFio;
	}

	public String getpTaskStartTime() {
		return pTaskStartTime;
	}

	public void setpTaskStartTime(String pTaskStartTime) {
		this.pTaskStartTime = pTaskStartTime;
	}

	public String getpTaskEndTime() {
		return pTaskEndTime;
	}

	public void setpTaskEndTime(String pTaskEndTime) {
		this.pTaskEndTime = pTaskEndTime;
	}

	public String getpUpdatedByTime() {
		return pUpdatedByTime;
	}

	public void setpUpdatedByTime(String pUpdatedByTime) {
		this.pUpdatedByTime = pUpdatedByTime;
	}

//	public String getpUpdatedByFio() {
//		return pUpdatedByFio;
//	}
//
//	public void setpUpdatedByFio(String pUpdatedByFio) {
//		this.pUpdatedByFio = pUpdatedByFio;
//	}
//
//	public String getpUpdatedByUsr() {
//		return pUpdatedByUsr;
//	}
//
//	public void setpUpdatedByUsr(String pUpdatedByUsr) {
//		this.pUpdatedByUsr = pUpdatedByUsr;
//	}
}
