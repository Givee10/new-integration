package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractAttResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncAttResponse", namespace="urn:xmethods-delayed-attes")
public class IncAttResponse extends AbstractAttResponse {
}
