package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getStateGroups", namespace="urn:xmethods-delayed-stgroups")
public class StateGroupsRequest extends AbstractRequest {
}
