package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getViolReas", namespace="urn:xmethods-delayed-vreas")
public class ViolReasRequest extends AbstractRequest {
}
