package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Result", propOrder = {"item"})
public class Result {
    @XmlElement(required = true)
    protected List<ResultItem> item;

    public List<ResultItem> getItem() {
        return item;
    }
}
