package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getCodes", namespace="urn:xmethods-delayed-codes")
public class CodesRequest extends AbstractRequest {
}
