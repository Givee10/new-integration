package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getViolReasResponse", namespace="urn:xmethods-delayed-vreas")
public class ViolReasResponse extends AbstractResponse {
}
