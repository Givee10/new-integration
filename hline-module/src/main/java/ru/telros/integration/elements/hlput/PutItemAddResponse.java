package ru.telros.integration.elements.hlput;

import ru.telros.integration.elements.AbstractInputResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putItemAddResponse", namespace="urn:xmethods-delayed-witadd")
public class PutItemAddResponse extends AbstractInputResponse {
}
