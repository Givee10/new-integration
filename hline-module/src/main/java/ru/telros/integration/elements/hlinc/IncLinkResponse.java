package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncLinkResponse", namespace="urn:xmethods-delayed-inclinks")
public class IncLinkResponse extends AbstractResponse {
}
