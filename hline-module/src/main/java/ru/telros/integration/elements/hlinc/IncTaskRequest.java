package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractInputRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncTask", namespace="urn:xmethods-delayed-inctasks")
public class IncTaskRequest extends AbstractInputRequest {
}
