package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputResult", propOrder = {"item"})
public class InputResult {
    @XmlElement(required = true)
    protected List<ResponseItem> item;

    public List<ResponseItem> getItem() {
        return item;
    }
}
