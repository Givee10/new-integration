package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putIncInfo", namespace="urn:xmethods-delayed-wincinf")
public class PutIncInfoRequest {
	@XmlElement(name = "InputData", required = true)
	protected INCINFO InputData;

	public INCINFO getInputData() {
		return InputData;
	}

	public void setInputData(INCINFO inputData) {
		InputData = inputData;
	}
}
