package ru.telros.integration.elements.hlput;

import ru.telros.integration.elements.AbstractInputResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "putNoteAddResponse", namespace="urn:xmethods-delayed-wnotadd")
public class PutNoteAddResponse extends AbstractInputResponse {
}
