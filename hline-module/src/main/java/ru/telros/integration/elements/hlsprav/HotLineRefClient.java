package ru.telros.integration.elements.hlsprav;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class HotLineRefClient extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(HotLineRefClient.class);

    /* коды проблем */
    public CodesResponse getCodes() {
        CodesRequest request = new CodesRequest();
        request.setId("0");

        log.info("Requesting All Codes");
        return (CodesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public CodesResponse getCode(Long id) {
        CodesRequest request = new CodesRequest();
        request.setId(id.toString());

        log.info("Requesting Code with id = " + id);
        return (CodesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* типы связей заявок */
    public LinkTypesResponse getLinkTypes() {
        LinkTypesRequest request = new LinkTypesRequest();
        request.setId("0");

        log.info("Requesting All Link Types");
        return (LinkTypesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public LinkTypesResponse getLinkType(Long id) {
        LinkTypesRequest request = new LinkTypesRequest();
        request.setId(id.toString());

        log.info("Requesting Link Type with id = " + id);
        return (LinkTypesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* важность заявки */
    public RelevansResponse getRelevans() {
        RelevansRequest request = new RelevansRequest();
        request.setId("0");

        log.info("Requesting All Relevans");
        return (RelevansResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public RelevansResponse getRelevan(Long id) {
        RelevansRequest request = new RelevansRequest();
        request.setId(id.toString());

        log.info("Requesting Relevan with id = " + id);
        return (RelevansResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* тип повторности заявки */
    public RepeatTypeResponse getRepeatTypes() {
        RepeatTypeRequest request = new RepeatTypeRequest();
        request.setId("0");

        log.info("Requesting All Repeat Types");
        return (RepeatTypeResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public RepeatTypeResponse getRepeatType(Long id) {
        RepeatTypeRequest request = new RepeatTypeRequest();
        request.setId(id.toString());

        log.info("Requesting Repeat Type with id = " + id);
        return (RepeatTypeResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* справочник статусов */
    public StateResponse getStates() {
        StateRequest request = new StateRequest();
        request.setId("0");

        log.info("Requesting All States");
        return (StateResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StateResponse getState(Long id) {
        StateRequest request = new StateRequest();
        request.setId(id.toString());

        log.info("Requesting State with id = " + id);
        return (StateResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* переходы статусов по группам */
    public StateChangeResponse getStateChanges() {
        StateChangeRequest request = new StateChangeRequest();
        request.setId("0");

        log.info("Requesting All State Changes");
        return (StateChangeResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StateChangeResponse getStateChange(Long id) {
        StateChangeRequest request = new StateChangeRequest();
        request.setId(id.toString());

        log.info("Requesting State Change with id = " + id);
        return (StateChangeResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* группы статусов для полномочий */
    public StateGroupsResponse getStateGroups() {
        StateGroupsRequest request = new StateGroupsRequest();
        request.setId("0");

        log.info("Requesting All State Groups");
        return (StateGroupsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StateGroupsResponse getStateGroup(Long id) {
        StateGroupsRequest request = new StateGroupsRequest();
        request.setId(id.toString());

        log.info("Requesting State Group with id = " + id);
        return (StateGroupsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /* причина отклонения сроков выполнения заявки */
    public ViolReasResponse getViolReas() {
        ViolReasRequest request = new ViolReasRequest();
        request.setId("0");

        log.info("Requesting All Violation Reasons");
        return (ViolReasResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public ViolReasResponse getViolReas(Long id) {
        ViolReasRequest request = new ViolReasRequest();
        request.setId(id.toString());

        log.info("Requesting Violation Reason with id = " + id);
        return (ViolReasResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
