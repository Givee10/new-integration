package ru.telros.integration.elements.hlsprav;

import ru.telros.integration.elements.AbstractRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getRepeatType", namespace="urn:xmethods-delayed-rttypes")
public class RepeatTypeRequest extends AbstractRequest {
}
