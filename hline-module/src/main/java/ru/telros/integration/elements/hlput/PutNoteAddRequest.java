package ru.telros.integration.elements.hlput;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"InputData"})
@XmlRootElement(name = "putNoteAdd", namespace="urn:xmethods-delayed-wnotadd")
public class PutNoteAddRequest {
	@XmlElement(name = "InputData", required = true)
	protected NOTEADD InputData;

	public NOTEADD getInputData() {
		return InputData;
	}

	public void setInputData(NOTEADD inputData) {
		InputData = inputData;
	}
}
