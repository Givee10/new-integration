package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncNotesResponse", namespace="urn:xmethods-delayed-incnotes")
public class IncNotesResponse extends AbstractResponse {
}
