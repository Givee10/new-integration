package ru.telros.integration.elements.hlinc;

import ru.telros.integration.elements.AbstractResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getIncCntlTabResponse", namespace="urn:xmethods-delayed-ictabs")
public class IncCntlTabResponse extends AbstractResponse {
}
