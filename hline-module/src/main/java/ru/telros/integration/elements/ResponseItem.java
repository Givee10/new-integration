package ru.telros.integration.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "item", propOrder = {"key", "value"})
public class ResponseItem {
    @XmlElement(required = true)
    protected String key, value;

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
