package ru.telros.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import ru.telros.integration.elements.hlinc.HotLineClient;
import ru.telros.integration.elements.hlput.HotLinePutClient;
import ru.telros.integration.elements.hlsprav.HotLineRefClient;

@Configuration
public class HLineConfiguration {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in pom.xml
        marshaller.setContextPath("ru.telros.integration.elements");
		return marshaller;
	}

	@Bean
	public HotLineRefClient hlRefClient(Jaxb2Marshaller marshaller) {
        HotLineRefClient client = new HotLineRefClient();
		client.setDefaultUri("http://172.16.152.134/xxbr/hlsprav.php?wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

    @Bean
    public HotLineClient hlMainClient(Jaxb2Marshaller marshaller) {
        HotLineClient client = new HotLineClient();
        client.setDefaultUri("http://172.16.152.134/xxbr/hlinc.php?wsdl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

	@Bean
	public HotLinePutClient hlPutClient(Jaxb2Marshaller marshaller) {
		HotLinePutClient client = new HotLinePutClient();
		client.setDefaultUri("http://172.16.152.134/xxbr/hlput.php?wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public TaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.setPoolSize(10);
		return threadPoolTaskScheduler;
	}
}
