package ru.telros.integration.domain;

import javax.persistence.*;

@Entity
@Table(name = "INCIDENT_ATTACHMENT")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_ATTACHMENT_SEQ", allocationSize = 1)
public class IncidentAttachment extends AbstractEntity {
    @Column(name = "INCIDENT_ID")
    public String INCIDENT_ID;

    @Column(name = "ATTACHED_DOCUMENT_ID")
    public String ATTACHED_DOCUMENT_ID;

    @Column(name = "SEQ_NUM")
    public String SEQ_NUM;

    @Column(name = "FILE_NAME")
    public String FILE_NAME;

    @Column(name = "FILE_CONTENT_TYPE")
    public String FILE_CONTENT_TYPE;

    @Column(name = "FILE_DATA", columnDefinition = "CLOB")
    @Lob
    public String FILE_DATA;

    @Column(name = "UPLOAD_DATE")
    public String UPLOAD_DATE;

    @Column(name = "INCIDENT_CREATION_DATE")
    public String INCIDENT_CREATION_DATE;

    @Column(name = "INCIDENT_LAST_UPDATE_DATE")
    public String INCIDENT_LAST_UPDATE_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentAttachment that = (IncidentAttachment) o;

        if (INCIDENT_ID != null ? !INCIDENT_ID.equals(that.INCIDENT_ID) : that.INCIDENT_ID != null) return false;
        return ATTACHED_DOCUMENT_ID != null ? ATTACHED_DOCUMENT_ID.equals(that.ATTACHED_DOCUMENT_ID) : that.ATTACHED_DOCUMENT_ID == null;
    }

    @Override
    public int hashCode() {
        int result = INCIDENT_ID != null ? INCIDENT_ID.hashCode() : 0;
        result = 31 * result + (ATTACHED_DOCUMENT_ID != null ? ATTACHED_DOCUMENT_ID.hashCode() : 0);
        return result;
    }
}
