package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_CONTROL")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_CONTROL_SEQ", allocationSize = 1)
public class IncidentControl extends AbstractEntity {
    @Column(name = "INCIDENT_ID")
    public String INCIDENT_ID;

    @Column(name = "INCIDENT_NUMBER")
    public String INCIDENT_NUMBER;

    @Column(name = "FILIAL_ID")
    public String FILIAL_ID;

    @Column(name = "FILIAL_NAME")
    public String FILIAL_NAME;

    @Column(name = "PEU_ID")
    public String PEU_ID;

    @Column(name = "PEU_NAME")
    public String PEU_NAME;

    @Column(name = "SURVEYED_ON_TIME")
    public String SURVEYED_ON_TIME;

    @Column(name = "DONE_ON_TIME")
    public String DONE_ON_TIME;

    @Column(name = "VIOLATION_REASON_CODE")
    public String VIOLATION_REASON_CODE;

    @Column(name = "VIOLATION_REASON_NAME")
    public String VIOLATION_REASON_NAME;

    @Column(name = "FACT_PROBLEM_CODE")
    public String FACT_PROBLEM_CODE;

    @Column(name = "FACT_PROBLEM_DESC")
    public String FACT_PROBLEM_DESC;

    @Column(name = "REZERV_1")
    public String REZERV_1;

    @Column(name = "COMPLETED_WORK_ID")
    public String COMPLETED_WORK_ID;

    @Column(name = "COMPLETED_WORK_DESC")
    public String COMPLETED_WORK_DESC;

    @Column(name = "DONE_IN_GU")
    public String DONE_IN_GU;

    @Column(name = "SENT2SOU")
    public String SENT2SOU;

    @Column(name = "CONTROL_DATE")
    public String CONTROL_DATE;

    @Column(name = "CLOSE_IN_GU")
    public String CLOSE_IN_GU;

    @Column(name = "SURVEY_DATE")
    public String SURVEY_DATE;

    @Column(name = "NUMBER_OF_VISITS")
    public String NUMBER_OF_VISITS;

    @Column(name = "INCIDENT_CREATION_DATE")
    public String INCIDENT_CREATION_DATE;

    @Column(name = "INCIDENT_LAST_UPDATE_DATE")
    public String INCIDENT_LAST_UPDATE_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentControl that = (IncidentControl) o;

        if (INCIDENT_NUMBER != null ? !INCIDENT_NUMBER.equals(that.INCIDENT_NUMBER) : that.INCIDENT_NUMBER != null)
            return false;
        return INCIDENT_ID != null ? INCIDENT_ID.equals(that.INCIDENT_ID) : that.INCIDENT_ID == null;
    }

    @Override
    public int hashCode() {
        int result = INCIDENT_NUMBER != null ? INCIDENT_NUMBER.hashCode() : 0;
        result = 31 * result + (INCIDENT_ID != null ? INCIDENT_ID.hashCode() : 0);
        return result;
    }
}
