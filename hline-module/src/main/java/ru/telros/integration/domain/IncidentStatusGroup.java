package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_STATUS_GROUP")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_STATUS_GROUP_SEQ", allocationSize = 1)
public class IncidentStatusGroup extends AbstractEntity {
    @Column(name = "GROUP_ID")
    public String GROUP_ID;

    @Column(name = "GROUP_NAME")
    public String GROUP_NAME;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentStatusGroup that = (IncidentStatusGroup) o;

        return GROUP_ID != null ? GROUP_ID.equals(that.GROUP_ID) : that.GROUP_ID == null;
    }

    @Override
    public int hashCode() {
        return GROUP_ID != null ? GROUP_ID.hashCode() : 0;
    }
}
