package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_SEQ", allocationSize = 1)
public class Incident extends AbstractEntity {
    @Column(name = "INCIDENT_NUMBER")
    public String INCIDENT_NUMBER;

    @Column(name = "INCIDENT_ID")
    public String INCIDENT_ID;

    @Column(name = "CALLER_TYPE_NAME")
    public String CALLER_TYPE_NAME;

    @Column(name = "CONTACT_NAME")
    public String CONTACT_NAME;

    @Column(name = "ID_AS")
    public String ID_AS;

    @Column(name = "PRF_ID")
    public String PRF_ID;

    @Column(name = "ADDRESS")
    public String ADDRESS;

    @Column(name = "CITY")
    public String CITY;

    @Column(name = "STATE")
    public String STATE;

    @Column(name = "COMPANY_NAME")
    public String COMPANY_NAME;

    @Column(name = "FULL_ADDRESS")
    public String FULL_ADDRESS;

    @Column(name = "ADD_ADDRESS_INFO", length = 2000)
    public String ADD_ADDRESS_INFO;

    @Column(name = "INCIDENT_SEVERITY_ID")
    public String INCIDENT_SEVERITY_ID;

    @Column(name = "PHONE_NUMBER")
    public String PHONE_NUMBER;

    @Column(name = "FLAT_NUMBER")
    public String FLAT_NUMBER;

    @Column(name = "INCIDENT_TYPE_NAME")
    public String INCIDENT_TYPE_NAME;

    @Column(name = "INCIDENT_STATUS_ID")
    public String INCIDENT_STATUS_ID;

    @Column(name = "INCIDENT_DATE")
    public String INCIDENT_DATE;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Column(name = "CREATED_BY_FULL_NAME")
    public String CREATED_BY_FULL_NAME;

    @Column(name = "CLOSE_DATE")
    public String CLOSE_DATE;

    @Column(name = "OWNER_GROUP_ID")
    public String OWNER_GROUP_ID;

    @Column(name = "RESOURCE_GROUP_NAME")
    public String RESOURCE_GROUP_NAME;

    @Column(name = "DEST_FILIAL_CODE")
    public String DEST_FILIAL_CODE;

    @Column(name = "DEST_FILIAL_NAME")
    public String DEST_FILIAL_NAME;

    @Column(name = "DEST_PEU_CODE")
    public String DEST_PEU_CODE;

    @Column(name = "DEST_PEU_NAME")
    public String DEST_PEU_NAME;

    @Column(name = "PROBLEM_CODE")
    public String PROBLEM_CODE;

    @Column(name = "NUM_IN_FILIAL")
    public String NUM_IN_FILIAL;

    @Column(name = "GU_FIELD")
    public String GU_FIELD;

    @Column(name = "FACT_BEGIN_DATE")
    public String FACT_BEGIN_DATE;

    @Column(name = "FACT_END_DATE")
    public String FACT_END_DATE;

    @Column(name = "PLAN_BEGIN_DATE")
    public String PLAN_BEGIN_DATE;

    @Column(name = "PLAN_END_DATE")
    public String PLAN_END_DATE;

    @Column(name = "RESOLUTION_CODE")
    public String RESOLUTION_CODE;

    @Column(name = "RESOLUTION_CODE_MEANING")
    public String RESOLUTION_CODE_MEANING;

    @Column(name = "WORK_QUEUE_FLAG")
    public String WORK_QUEUE_FLAG;

    @Column(name = "INCIDENT_CREATION_DATE")
    public String INCIDENT_CREATION_DATE;

    @Column(name = "INCIDENT_LAST_UPDATE_DATE")
    public String INCIDENT_LAST_UPDATE_DATE;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Incident incident = (Incident) o;

		if (INCIDENT_NUMBER != null ? !INCIDENT_NUMBER.equals(incident.INCIDENT_NUMBER) : incident.INCIDENT_NUMBER != null)
			return false;
		return INCIDENT_ID != null ? INCIDENT_ID.equals(incident.INCIDENT_ID) : incident.INCIDENT_ID == null;
	}

	@Override
	public int hashCode() {
		int result = INCIDENT_NUMBER != null ? INCIDENT_NUMBER.hashCode() : 0;
		result = 31 * result + (INCIDENT_ID != null ? INCIDENT_ID.hashCode() : 0);
		return result;
	}
}
