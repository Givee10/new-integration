package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_NOTE")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_NOTE_SEQ", allocationSize = 1)
public class IncidentNote extends AbstractEntity {
    @Column(name = "JTF_NOTE_ID")
    public String JTF_NOTE_ID;

    @Column(name = "OBJECT_CODE")
    public String OBJECT_CODE;

    @Column(name = "INCIDENT_ID")
    public String INCIDENT_ID;

    @Column(name = "INCIDENT_NUMBER")
    public String INCIDENT_NUMBER;

    @Column(name = "TASK_ID")
    public String TASK_ID;

    @Column(name = "TASK_NUMBER")
    public String TASK_NUMBER;

    @Column(name = "LAST_UPDATE_DATE")
    public String LAST_UPDATE_DATE;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Column(name = "NOTES", length = 15000)
    public String NOTES;

    @Column(name = "CREATED_BY")
    public String CREATED_BY;

    @Column(name = "CREATED_BY_NAME")
    public String CREATED_BY_NAME;

    @Column(name = "INCIDENT_CREATION_DATE")
    public String INCIDENT_CREATION_DATE;

    @Column(name = "INCIDENT_LAST_UPDATE_DATE")
    public String INCIDENT_LAST_UPDATE_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentNote that = (IncidentNote) o;

        if (JTF_NOTE_ID != null ? !JTF_NOTE_ID.equals(that.JTF_NOTE_ID) : that.JTF_NOTE_ID != null) return false;
        if (INCIDENT_ID != null ? !INCIDENT_ID.equals(that.INCIDENT_ID) : that.INCIDENT_ID != null) return false;
        return INCIDENT_NUMBER != null ? INCIDENT_NUMBER.equals(that.INCIDENT_NUMBER) : that.INCIDENT_NUMBER == null;
    }

    @Override
    public int hashCode() {
        int result = JTF_NOTE_ID != null ? JTF_NOTE_ID.hashCode() : 0;
        result = 31 * result + (INCIDENT_ID != null ? INCIDENT_ID.hashCode() : 0);
        result = 31 * result + (INCIDENT_NUMBER != null ? INCIDENT_NUMBER.hashCode() : 0);
        return result;
    }
}
