package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_ADDITIONAL")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_ADDITIONAL_SEQ", allocationSize = 1)
public class IncidentAdditional extends AbstractEntity {
    @Column(name = "INCIDENT_NUMBER")
    public String INCIDENT_NUMBER;

    @Column(name = "INCIDENT_ID")
    public String INCIDENT_ID;

    @Column(name = "PROBLEM_CODE")
    public String PROBLEM_CODE;

    @Column(name = "EXTERNAL_ATTRIBUTE_1")
    public String EXTERNAL_ATTRIBUTE_1;

    @Column(name = "EXTERNAL_ATTRIBUTE_1_PROMPT")
    public String EXTERNAL_ATTRIBUTE_1_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_1_VALUE")
    public String EXTERNAL_ATTRIBUTE_1_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_2")
    public String EXTERNAL_ATTRIBUTE_2;

    @Column(name = "EXTERNAL_ATTRIBUTE_2_PROMPT")
    public String EXTERNAL_ATTRIBUTE_2_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_2_VALUE")
    public String EXTERNAL_ATTRIBUTE_2_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_3")
    public String EXTERNAL_ATTRIBUTE_3;

    @Column(name = "EXTERNAL_ATTRIBUTE_3_PROMPT")
    public String EXTERNAL_ATTRIBUTE_3_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_3_VALUE")
    public String EXTERNAL_ATTRIBUTE_3_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_4")
    public String EXTERNAL_ATTRIBUTE_4;

    @Column(name = "EXTERNAL_ATTRIBUTE_4_PROMPT")
    public String EXTERNAL_ATTRIBUTE_4_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_4_VALUE")
    public String EXTERNAL_ATTRIBUTE_4_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_5")
    public String EXTERNAL_ATTRIBUTE_5;

    @Column(name = "EXTERNAL_ATTRIBUTE_5_PROMPT")
    public String EXTERNAL_ATTRIBUTE_5_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_5_VALUE")
    public String EXTERNAL_ATTRIBUTE_5_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_6")
    public String EXTERNAL_ATTRIBUTE_6;

    @Column(name = "EXTERNAL_ATTRIBUTE_6_PROMPT")
    public String EXTERNAL_ATTRIBUTE_6_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_6_VALUE")
    public String EXTERNAL_ATTRIBUTE_6_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_7")
    public String EXTERNAL_ATTRIBUTE_7;

    @Column(name = "EXTERNAL_ATTRIBUTE_7_PROMPT")
    public String EXTERNAL_ATTRIBUTE_7_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_7_VALUE")
    public String EXTERNAL_ATTRIBUTE_7_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_8")
    public String EXTERNAL_ATTRIBUTE_8;

    @Column(name = "EXTERNAL_ATTRIBUTE_8_PROMPT")
    public String EXTERNAL_ATTRIBUTE_8_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_8_VALUE")
    public String EXTERNAL_ATTRIBUTE_8_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_9")
    public String EXTERNAL_ATTRIBUTE_9;

    @Column(name = "EXTERNAL_ATTRIBUTE_9_PROMPT")
    public String EXTERNAL_ATTRIBUTE_9_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_9_VALUE")
    public String EXTERNAL_ATTRIBUTE_9_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_10")
    public String EXTERNAL_ATTRIBUTE_10;

    @Column(name = "EXTERNAL_ATTRIBUTE_10_PROMPT")
    public String EXTERNAL_ATTRIBUTE_10_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_10_VALUE")
    public String EXTERNAL_ATTRIBUTE_10_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_11")
    public String EXTERNAL_ATTRIBUTE_11;

    @Column(name = "EXTERNAL_ATTRIBUTE_11_PROMPT")
    public String EXTERNAL_ATTRIBUTE_11_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_11_VALUE")
    public String EXTERNAL_ATTRIBUTE_11_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_12")
    public String EXTERNAL_ATTRIBUTE_12;

    @Column(name = "EXTERNAL_ATTRIBUTE_12_PROMPT")
    public String EXTERNAL_ATTRIBUTE_12_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_12_VALUE")
    public String EXTERNAL_ATTRIBUTE_12_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_13")
    public String EXTERNAL_ATTRIBUTE_13;

    @Column(name = "EXTERNAL_ATTRIBUTE_13_PROMPT")
    public String EXTERNAL_ATTRIBUTE_13_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_13_VALUE")
    public String EXTERNAL_ATTRIBUTE_13_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_14")
    public String EXTERNAL_ATTRIBUTE_14;

    @Column(name = "EXTERNAL_ATTRIBUTE_14_PROMPT")
    public String EXTERNAL_ATTRIBUTE_14_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_14_VALUE")
    public String EXTERNAL_ATTRIBUTE_14_VALUE;

    @Column(name = "EXTERNAL_ATTRIBUTE_15")
    public String EXTERNAL_ATTRIBUTE_15;

    @Column(name = "EXTERNAL_ATTRIBUTE_15_PROMPT")
    public String EXTERNAL_ATTRIBUTE_15_PROMPT;

    @Column(name = "EXTERNAL_ATTRIBUTE_15_VALUE")
    public String EXTERNAL_ATTRIBUTE_15_VALUE;

    @Column(name = "INCIDENT_CREATION_DATE")
    public String INCIDENT_CREATION_DATE;

    @Column(name = "INCIDENT_LAST_UPDATE_DATE")
    public String INCIDENT_LAST_UPDATE_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentAdditional that = (IncidentAdditional) o;

        if (INCIDENT_NUMBER != null ? !INCIDENT_NUMBER.equals(that.INCIDENT_NUMBER) : that.INCIDENT_NUMBER != null)
            return false;
        return INCIDENT_ID != null ? INCIDENT_ID.equals(that.INCIDENT_ID) : that.INCIDENT_ID == null;
    }

    @Override
    public int hashCode() {
        int result = INCIDENT_NUMBER != null ? INCIDENT_NUMBER.hashCode() : 0;
        result = 31 * result + (INCIDENT_ID != null ? INCIDENT_ID.hashCode() : 0);
        return result;
    }
}
