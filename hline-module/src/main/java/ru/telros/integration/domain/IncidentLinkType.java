package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_LINK_TYPE")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_LINK_TYPE_SEQ", allocationSize = 1)
public class IncidentLinkType extends AbstractEntity {
    @Column(name = "TYPE_ID")
    public String TYPE_ID;

    @Column(name = "LINK_NAME")
    public String LINK_NAME;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentLinkType that = (IncidentLinkType) o;

        return TYPE_ID != null ? TYPE_ID.equals(that.TYPE_ID) : that.TYPE_ID == null;
    }

    @Override
    public int hashCode() {
        return TYPE_ID != null ? TYPE_ID.hashCode() : 0;
    }
}
