package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_STATUS_CHANGE")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_STATUS_CHANGE_SEQ", allocationSize = 1)
public class IncidentStatusChange extends AbstractEntity {
    @Column(name = "STATUS_TRANSITION_ID")
    public String STATUS_TRANSITION_ID;

    @Column(name = "FROM_INCIDENT_STATUS_ID")
    public String FROM_INCIDENT_STATUS_ID;

    @Column(name = "FROM_INCIDENT_STATUS_NAME")
    public String FROM_INCIDENT_STATUS_NAME;

    @Column(name = "TO_INCIDENT_STATUS_ID")
    public String TO_INCIDENT_STATUS_ID;

    @Column(name = "TO_INCIDENT_STATUS_NAME")
    public String TO_INCIDENT_STATUS_NAME;

    @Column(name = "STATUS_GROUP_ID")
    public String STATUS_GROUP_ID;

    @Column(name = "GROUP_NAME")
    public String GROUP_NAME;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentStatusChange that = (IncidentStatusChange) o;

        return STATUS_TRANSITION_ID != null ? STATUS_TRANSITION_ID.equals(that.STATUS_TRANSITION_ID) : that.STATUS_TRANSITION_ID == null;
    }

    @Override
    public int hashCode() {
        return STATUS_TRANSITION_ID != null ? STATUS_TRANSITION_ID.hashCode() : 0;
    }
}
