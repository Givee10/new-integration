package ru.telros.integration.domain;

public class IncItem {
	private String pIncidentId;
	private String pIncidentNumber;
	private String pTaskNumber;
	private String pTaskStatusId;
	private String pTaskType;
	private String pTaskDescr;
	private String pTaskNotes;
	private String pResponsibleName;
	private String pResponsibleFio;
	private String pTaskStartTime;
	private String pTaskEndTime;
	private String pUpdatedByTime;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpTaskNumber() {
		return pTaskNumber;
	}

	public void setpTaskNumber(String pTaskNumber) {
		this.pTaskNumber = pTaskNumber;
	}

	public String getpTaskStatusId() {
		return pTaskStatusId;
	}

	public void setpTaskStatusId(String pTaskStatusId) {
		this.pTaskStatusId = pTaskStatusId;
	}

	public String getpTaskType() {
		return pTaskType;
	}

	public void setpTaskType(String pTaskType) {
		this.pTaskType = pTaskType;
	}

	public String getpTaskDescr() {
		return pTaskDescr;
	}

	public void setpTaskDescr(String pTaskDescr) {
		this.pTaskDescr = pTaskDescr;
	}

	public String getpTaskNotes() {
		return pTaskNotes;
	}

	public void setpTaskNotes(String pTaskNotes) {
		this.pTaskNotes = pTaskNotes;
	}

	public String getpResponsibleName() {
		return pResponsibleName;
	}

	public void setpResponsibleName(String pResponsibleName) {
		this.pResponsibleName = pResponsibleName;
	}

	public String getpResponsibleFio() {
		return pResponsibleFio;
	}

	public void setpResponsibleFio(String pResponsibleFio) {
		this.pResponsibleFio = pResponsibleFio;
	}

	public String getpTaskStartTime() {
		return pTaskStartTime;
	}

	public void setpTaskStartTime(String pTaskStartTime) {
		this.pTaskStartTime = pTaskStartTime;
	}

	public String getpTaskEndTime() {
		return pTaskEndTime;
	}

	public void setpTaskEndTime(String pTaskEndTime) {
		this.pTaskEndTime = pTaskEndTime;
	}

	public String getpUpdatedByTime() {
		return pUpdatedByTime;
	}

	public void setpUpdatedByTime(String pUpdatedByTime) {
		this.pUpdatedByTime = pUpdatedByTime;
	}
}
