package ru.telros.integration.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MESSAGE_IMPORT_INCIDENT")
@SequenceGenerator(name = "idGenerator", sequenceName = "MESSAGE_IMPORT_INCIDENT_SEQ", allocationSize = 1)
public class MessageImportIncident {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenerator")
	@Column(name = "ID")
	public Long ID;

	@Column(name = "GUID")
	public String GUID;

	@Column(name = "TYPE_MESSAGE")
	public String TYPE_MESSAGE;

	@Column(name = "INCIDENT_ID")
	public String INCIDENT_ID;

	@Column(name = "REQUEST_ID")
	public String REQUEST_ID;

	@Column(name = "RESULT", length = 4000)
	public String RESULT;

	@Column(name = "ERROR_CODE")
	public String ERROR_CODE;

	@CreatedDate
	@Column(name = "CREATE_DATE")
	public Date CREATE_DATE;

	@LastModifiedDate
	@Column(name = "CHANGE_DATE")
	public Date CHANGE_DATE;
}
