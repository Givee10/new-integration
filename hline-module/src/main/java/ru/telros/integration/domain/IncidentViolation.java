package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_VIOLATION")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_VIOLATION_SEQ", allocationSize = 1)
public class IncidentViolation extends AbstractEntity {
    @Column(name = "VIOLATION_ID")
    public String VIOLATION_ID;

    @Column(name = "VIOLATION_CODE")
    public String VIOLATION_CODE;

    @Column(name = "VIOLATION_NAME")
    public String VIOLATION_NAME;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentViolation that = (IncidentViolation) o;

        return VIOLATION_ID != null ? VIOLATION_ID.equals(that.VIOLATION_ID) : that.VIOLATION_ID == null;
    }

    @Override
    public int hashCode() {
        return VIOLATION_ID != null ? VIOLATION_ID.hashCode() : 0;
    }
}
