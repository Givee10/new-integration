package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_LINK")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_LINK_SEQ", allocationSize = 1)
public class IncidentLink extends AbstractEntity {
    @Column(name = "LINK_ID")
    public String LINK_ID;

    @Column(name = "SUBJECT_INCIDENT_ID")
    public String SUBJECT_INCIDENT_ID;

    @Column(name = "TO_INCIDENT_ID")
    public String TO_INCIDENT_ID;

    @Column(name = "TO_INCIDENT_NUMBER")
    public String TO_INCIDENT_NUMBER;

    @Column(name = "TO_INCIDENT_SUMMARY")
    public String TO_INCIDENT_SUMMARY;

    @Column(name = "TO_INCIDENT_SEVERITY")
    public String TO_INCIDENT_SEVERITY;

    @Column(name = "TO_INCIDENT_STATUS")
    public String TO_INCIDENT_STATUS;

    @Column(name = "FROM_INCIDENT_ID")
    public String FROM_INCIDENT_ID;

    @Column(name = "LINK_TYPE_ID")
    public String LINK_TYPE_ID;

    @Column(name = "LINK_TYPE_NAME")
    public String LINK_TYPE_NAME;

    @Column(name = "LINK_TYPE")
    public String LINK_TYPE;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentLink that = (IncidentLink) o;

        return LINK_ID != null ? LINK_ID.equals(that.LINK_ID) : that.LINK_ID == null;
    }

    @Override
    public int hashCode() {
        return LINK_ID != null ? LINK_ID.hashCode() : 0;
    }
}
