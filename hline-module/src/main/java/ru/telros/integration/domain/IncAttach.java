package ru.telros.integration.domain;

public class IncAttach {
	private String pIncidentId;
	private String pIncidentNumber;
	private String pFileName;
	private String pMimeType;
	private String pFileData;
	private String pFileDescr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpFileName() {
		return pFileName;
	}

	public void setpFileName(String pFileName) {
		this.pFileName = pFileName;
	}

	public String getpMimeType() {
		return pMimeType;
	}

	public void setpMimeType(String pMimeType) {
		this.pMimeType = pMimeType;
	}

	public String getpFileData() {
		return pFileData;
	}

	public void setpFileData(String pFileData) {
		this.pFileData = pFileData;
	}

	public String getpFileDescr() {
		return pFileDescr;
	}

	public void setpFileDescr(String pFileDescr) {
		this.pFileDescr = pFileDescr;
	}
}
