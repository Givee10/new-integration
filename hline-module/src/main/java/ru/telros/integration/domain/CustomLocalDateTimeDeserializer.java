package ru.telros.integration.domain;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import ru.telros.integration.StringUtil;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CustomLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
	@Override
	public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
		if (StringUtil.isNull(jsonParser.getText())) return null;
		return LocalDateTime.parse(jsonParser.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
	}
}
