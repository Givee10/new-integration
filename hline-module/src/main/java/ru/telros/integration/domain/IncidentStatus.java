package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_STATUS")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_STATUS_SEQ", allocationSize = 1)
public class IncidentStatus extends AbstractEntity {
    @Column(name = "INCIDENT_STATUS_ID")
    public String INCIDENT_STATUS_ID;

    @Column(name = "INCIDENT_STATUS_NAME")
    public String INCIDENT_STATUS_NAME;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentStatus that = (IncidentStatus) o;

        return INCIDENT_STATUS_ID != null ? INCIDENT_STATUS_ID.equals(that.INCIDENT_STATUS_ID) : that.INCIDENT_STATUS_ID == null;
    }

    @Override
    public int hashCode() {
        return INCIDENT_STATUS_ID != null ? INCIDENT_STATUS_ID.hashCode() : 0;
    }
}
