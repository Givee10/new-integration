package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_REPEAT_TYPE")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_REPEAT_TYPE_SEQ", allocationSize = 1)
public class IncidentRepeatType extends AbstractEntity {
    @Column(name = "REPEATED_TYPE_ID")
    public String REPEATED_TYPE_ID;

    @Column(name = "MEANING")
    public String MEANING;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentRepeatType that = (IncidentRepeatType) o;

        return REPEATED_TYPE_ID != null ? REPEATED_TYPE_ID.equals(that.REPEATED_TYPE_ID) : that.REPEATED_TYPE_ID == null;
    }

    @Override
    public int hashCode() {
        return REPEATED_TYPE_ID != null ? REPEATED_TYPE_ID.hashCode() : 0;
    }
}
