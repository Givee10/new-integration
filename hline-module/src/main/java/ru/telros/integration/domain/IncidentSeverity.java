package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_SEVERITY")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_SEVERITY_SEQ", allocationSize = 1)
public class IncidentSeverity extends AbstractEntity {
    @Column(name = "INCIDENT_SEVERITY_ID")
    public String INCIDENT_SEVERITY_ID;

    @Column(name = "NAME")
    public String NAME;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentSeverity that = (IncidentSeverity) o;

        return INCIDENT_SEVERITY_ID != null ? INCIDENT_SEVERITY_ID.equals(that.INCIDENT_SEVERITY_ID) : that.INCIDENT_SEVERITY_ID == null;
    }

    @Override
    public int hashCode() {
        return INCIDENT_SEVERITY_ID != null ? INCIDENT_SEVERITY_ID.hashCode() : 0;
    }
}
