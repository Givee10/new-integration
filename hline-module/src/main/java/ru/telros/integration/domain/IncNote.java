package ru.telros.integration.domain;

public class IncNote {
	private String pIncidentId;
	private String pIncidentNumber;
	private String pTaskId;
	private String pOrderNum;
	private String pNotesId;
	private String pNotesText;
	private String pNotesType;
	private String pNotesVisible;
	private String pUpdatedByTime;
	private String pUpdatedByFio;
	private String pUpdatedByUsr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpTaskId() {
		return pTaskId;
	}

	public void setpTaskId(String pTaskId) {
		this.pTaskId = pTaskId;
	}

	public String getpOrderNum() {
		return pOrderNum;
	}

	public void setpOrderNum(String pOrderNum) {
		this.pOrderNum = pOrderNum;
	}

	public String getpNotesId() {
		return pNotesId;
	}

	public void setpNotesId(String pNotesId) {
		this.pNotesId = pNotesId;
	}

	public String getpNotesText() {
		return pNotesText;
	}

	public void setpNotesText(String pNotesText) {
		this.pNotesText = pNotesText;
	}

	public String getpNotesType() {
		return pNotesType;
	}

	public void setpNotesType(String pNotesType) {
		this.pNotesType = pNotesType;
	}

	public String getpNotesVisible() {
		return pNotesVisible;
	}

	public void setpNotesVisible(String pNotesVisible) {
		this.pNotesVisible = pNotesVisible;
	}

	public String getpUpdatedByTime() {
		return pUpdatedByTime;
	}

	public void setpUpdatedByTime(String pUpdatedByTime) {
		this.pUpdatedByTime = pUpdatedByTime;
	}

	public String getpUpdatedByFio() {
		return pUpdatedByFio;
	}

	public void setpUpdatedByFio(String pUpdatedByFio) {
		this.pUpdatedByFio = pUpdatedByFio;
	}

	public String getpUpdatedByUsr() {
		return pUpdatedByUsr;
	}

	public void setpUpdatedByUsr(String pUpdatedByUsr) {
		this.pUpdatedByUsr = pUpdatedByUsr;
	}
}
