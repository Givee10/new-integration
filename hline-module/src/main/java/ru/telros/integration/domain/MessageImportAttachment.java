package ru.telros.integration.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MESSAGE_IMPORT_ATTACHMENT")
@SequenceGenerator(name = "idGenerator", sequenceName = "MESSAGE_IMPORT_ATTACHMENT_SEQ", allocationSize = 1)
public class MessageImportAttachment {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenerator")
	@Column(name = "ID")
	public Long ID;

	@Column(name = "INCIDENT_ID")
	public String INCIDENT_ID;

	@Column(name = "INCIDENT_NUMBER")
	public String INCIDENT_NUMBER;

	@Column(name = "REQUEST_ID")
	public String REQUEST_ID;

	@Column(name = "INCIDENT_ATTACHMENT_ID")
	public String INCIDENT_ATTACHMENT_ID;

	@Column(name = "REQUEST_ATTACHMENT_ID")
	public String REQUEST_ATTACHMENT_ID;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE")
	public Date CREATE_DATE;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHANGE_DATE")
	public Date CHANGE_DATE;
}
