package ru.telros.integration.domain;

public class IncInfo {
	private String pIncidentId;
	private String pIncidentNumber;
	private String pStatusId;
	private String pStatusName;
	private String pAddAdrInfo;
	private String pFilialNum;
	private String pStartDate;
	private String pClosedDate;
	private String pUpdatedByTime;
	private String pUpdatedByFio;
	private String pUpdatedByUsr;

	public String getpIncidentId() {
		return pIncidentId;
	}

	public void setpIncidentId(String pIncidentId) {
		this.pIncidentId = pIncidentId;
	}

	public String getpIncidentNumber() {
		return pIncidentNumber;
	}

	public void setpIncidentNumber(String pIncidentNumber) {
		this.pIncidentNumber = pIncidentNumber;
	}

	public String getpStatusId() {
		return pStatusId;
	}

	public void setpStatusId(String pStatusId) {
		this.pStatusId = pStatusId;
	}

	public String getpStatusName() {
		return pStatusName;
	}

	public void setpStatusName(String pStatusName) {
		this.pStatusName = pStatusName;
	}

	public String getpAddAdrInfo() {
		return pAddAdrInfo;
	}

	public void setpAddAdrInfo(String pAddAdrInfo) {
		this.pAddAdrInfo = pAddAdrInfo;
	}

	public String getpFilialNum() {
		return pFilialNum;
	}

	public void setpFilialNum(String pFilialNum) {
		this.pFilialNum = pFilialNum;
	}

	public String getpStartDate() {
		return pStartDate;
	}

	public void setpStartDate(String pStartDate) {
		this.pStartDate = pStartDate;
	}

	public String getpClosedDate() {
		return pClosedDate;
	}

	public void setpClosedDate(String pClosedDate) {
		this.pClosedDate = pClosedDate;
	}

	public String getpUpdatedByTime() {
		return pUpdatedByTime;
	}

	public void setpUpdatedByTime(String pUpdatedByTime) {
		this.pUpdatedByTime = pUpdatedByTime;
	}

	public String getpUpdatedByFio() {
		return pUpdatedByFio;
	}

	public void setpUpdatedByFio(String pUpdatedByFio) {
		this.pUpdatedByFio = pUpdatedByFio;
	}

	public String getpUpdatedByUsr() {
		return pUpdatedByUsr;
	}

	public void setpUpdatedByUsr(String pUpdatedByUsr) {
		this.pUpdatedByUsr = pUpdatedByUsr;
	}
}
