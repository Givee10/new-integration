package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_TASK")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_TASK_SEQ", allocationSize = 1)
public class IncidentTask extends AbstractEntity {
    @Column(name = "TASK_ID")
    public String TASK_ID;

    @Column(name = "TASK_NAME")
    public String TASK_NAME;

    @Column(name = "DESCRIPTION")
    public String DESCRIPTION;

    @Column(name = "INCIDENT_ID")
    public String INCIDENT_ID;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Column(name = "TASK_NUMBER")
    public String TASK_NUMBER;

    @Column(name = "TASK_PRIORITY_ID")
    public String TASK_PRIORITY_ID;

    @Column(name = "ACTUAL_START_DATE")
    public String ACTUAL_START_DATE;

    @Column(name = "ACTUAL_END_DATE")
    public String ACTUAL_END_DATE;

    @Column(name = "ACTUAL_EFFORT")
    public String ACTUAL_EFFORT;

    @Column(name = "ACTUAL_EFFORT_UOM")
    public String ACTUAL_EFFORT_UOM;

    @Column(name = "PLANNED_START_DATE")
    public String PLANNED_START_DATE;

    @Column(name = "PLANNED_END_DATE")
    public String PLANNED_END_DATE;

    @Column(name = "SCHEDULED_START_DATE")
    public String SCHEDULED_START_DATE;

    @Column(name = "SCHEDULED_END_DATE")
    public String SCHEDULED_END_DATE;

    @Column(name = "OPEN_FLAG")
    public String OPEN_FLAG;

    @Column(name = "INCIDENT_CREATION_DATE")
    public String INCIDENT_CREATION_DATE;

    @Column(name = "INCIDENT_LAST_UPDATE_DATE")
    public String INCIDENT_LAST_UPDATE_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentTask that = (IncidentTask) o;

        if (TASK_ID != null ? !TASK_ID.equals(that.TASK_ID) : that.TASK_ID != null) return false;
        return TASK_NUMBER != null ? TASK_NUMBER.equals(that.TASK_NUMBER) : that.TASK_NUMBER == null;
    }

    @Override
    public int hashCode() {
        int result = TASK_ID != null ? TASK_ID.hashCode() : 0;
        result = 31 * result + (TASK_NUMBER != null ? TASK_NUMBER.hashCode() : 0);
        return result;
    }
}
