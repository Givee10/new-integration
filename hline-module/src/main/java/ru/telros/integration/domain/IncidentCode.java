package ru.telros.integration.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INCIDENT_CODE")
@SequenceGenerator(name = "idGenerator", sequenceName = "INCIDENT_CODE_SEQ", allocationSize = 1)
public class IncidentCode extends AbstractEntity {
    @Column(name = "INCIDENT_CODE")
    public String INCIDENT_CODE;

    @Column(name = "INCIDENT_CODE_DESCR")
    public String INCIDENT_CODE_DESCR;

    @Column(name = "CREATION_DATE")
    public String CREATION_DATE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncidentCode that = (IncidentCode) o;

        return INCIDENT_CODE != null ? INCIDENT_CODE.equals(that.INCIDENT_CODE) : that.INCIDENT_CODE == null;
    }

    @Override
    public int hashCode() {
        return INCIDENT_CODE != null ? INCIDENT_CODE.hashCode() : 0;
    }
}
